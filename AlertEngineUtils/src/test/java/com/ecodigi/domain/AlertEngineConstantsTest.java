/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

public class AlertEngineConstantsTest
{
    private final Log logger = LogFactory.getLog( this.getClass() );

    private final String[] alertFileNames      = new String[] { "alert-id7.alert", "alert-id23.alert", "badalert-id15.alert", "alert-id9.other" };
    private final String[] sourceFileNames     = new String[] { "eventsource-id4.source", "eventsource-id197.source" };
    private final String[] feedFileNames       = new String[] { "eplfeed-Meu Feed.feed", "eplfeed-feed18734.feed", "eplfeed-Consulta de ordens.feed" };
    private final String[] validationFileNames = new String[] { "__validate__1.feed", "__validate__23.feed", "__validate__987654.feed" };

    public AlertEngineConstantsTest()
    {
    }

    private void testExpressions( String[] values, String pattern )
    {
        Pattern filePattern = Pattern.compile( pattern );
        for( String value: values )
        {
            Matcher matcher = filePattern.matcher( value );
            if ( matcher.matches() )
            {
                logger.info( String.format( "Match %s Groups %d", value, matcher.groupCount() ) );
                for( int group = 0; group <= matcher.groupCount(); group++ )
                    logger.info( String.format( "    Group %d '%s'", group, matcher.group( group ) ) );
                logger.info( String.format( "    ID Group '%s'",matcher.group( AlertEngineConstants.CONFIG_REGEX_ID_GROUP_NAME ) ) );
            }
            else
            {
                logger.info( String.format( "No match for %s", value ) );
            }
        }
    }

    @Test
    public void testConfigRegex()
    {
        testExpressions( alertFileNames, AlertEngineConstants.ALERT_CONFIG_REGEX );
        testExpressions( sourceFileNames, AlertEngineConstants.EVENT_SOURCE_CONFIG_REGEX );
        testExpressions( feedFileNames, AlertEngineConstants.FEED_CONFIG_REGEX );
        testExpressions( validationFileNames, AlertEngineConstants.VALIDATION_CONFIG_REGEX );
    }
}
