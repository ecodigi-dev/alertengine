/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecodigi.io;

import java.io.File;
import java.util.Comparator;

/**
 *
 * @author flaviogago
 */
public class FileTimeComparator implements Comparator<File>
{
    @Override
    public int compare( File file1, File file2 )
    {
        long timestamp1 = file1.lastModified();
        long timestamp2 = file2.lastModified();

        if ( timestamp1 == timestamp2 )
            return( 0 );

        if ( timestamp1 > timestamp2 )
            return( 1 );
        else
            return( -1 );
    }
}
