/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.stream;

import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.nio.file.SensitivityWatchEventModifier;

public class DirectoryFileListener implements FileProcessor, Runnable
{
    protected Log logger = LogFactory.getLog( this.getClass() );

    public static final boolean AUTO_DELETE   = true;
    public static final boolean MANUAL_DELETE = false;

    public static final String XML_MIME_TYPE = "application/xml";

    public static final String REJECT_PATH = "reject";
    public static final int RESTART_DELAY = 1000;

    private final String description;
    private final Path watchPath;
    private final Path rejectPath;
    private final WatchEvent.Kind<?> kinds[];
    private final boolean autoDelete;
    private final AtomicBoolean running = new AtomicBoolean( false );
    private boolean hasCreateKind;
    private WatchService watcher = null;
    private Thread watcherThread = null;

    public DirectoryFileListener( String filesPath, boolean autoDelete, WatchEvent.Kind<?>... kinds ) throws IOException
    {
        this.watchPath  = Paths.get( filesPath );
        this.rejectPath = Paths.get( filesPath,  REJECT_PATH );
        this.kinds      = kinds;
        this.autoDelete = autoDelete;
        Files.createDirectories( watchPath );
        Files.createDirectories( rejectPath );
        for ( WatchEvent.Kind<?> kind : kinds )
            hasCreateKind |= StandardWatchEventKinds.ENTRY_CREATE.equals(kind);
        this.description = String.format( "Directory watcher service at '%s' for files.", filesPath );
    }

    public boolean isAutoDelete()
    {
        return autoDelete;
    }

    protected void beforeStart() throws IOException
    {
        if ( hasCreateKind )
        {
            // Process files already in dir on start
            logger.info( String.format( "Will process files already on directory %s", watchPath.toString() ) );
            try ( DirectoryStream<Path> ds = Files.newDirectoryStream( watchPath ) )
            {
                for ( Path path : ds )
                    processFileInternal( path, StandardWatchEventKinds.ENTRY_CREATE );
            }
        }
    }

    protected boolean acceptFile( Path file )
    {
        return true;
    }

    public void start()
    {
        if ( watcherThread == null )
        {
            watcherThread = new Thread( this );
            watcherThread.start();
        }
    }

    public void stop()
    {
        if ( watcherThread != null )
        {
            try
            {
                watcher.close();
            }
            catch( IOException e )
            {
                logger.error( "Unable to close directory watcher", e );
            }
            watcher = null;
            running.set( false );
            watcherThread.interrupt();
            watcherThread = null;
        }
    }

    // TODO: esse metodo nao funciona se a extensao nao corresponder ao conteudo do arquivo
    // removi as chamadas
    protected boolean checkMimeType( Path file, String expectedMimeType )
    {
        boolean result = false;
        try
        {
            String mime = Files.probeContentType( file );
            if ( mime == null )
                logger.error( String.format ( "Unable to determine Mime type of File '%s'", file ) );
            else if ( mime.equals( expectedMimeType ) )
                result = true;
            else
                logger.error( String.format ( "File '%s' is not a %s file, it is %s", file, expectedMimeType, mime ) );
        }
        catch ( IOException  e )
        {
            logger.error( String.format( "Unable to check if %s is a %s file", file.toString(), expectedMimeType ), e );
        }
        return( result );
    }

    @Override
    public boolean process(Path file, WatchEvent.Kind<?> eventKind)
    {
        throw new UnsupportedOperationException("Not implemented.");
    }

    protected void processFileInternal( Path file, WatchEvent.Kind<?> eventKind )
    {
        // File must have been already processed or is a directory (for startup)
        if ( !Files.exists( file ) || Files.isDirectory( file ) )
                return;

        if ( acceptFile(file) )
        {
            try
            {
                if ( this.process( file, eventKind ) )
                {
                    if ( autoDelete )
                    {
                        logger.debug( String.format( "File '%s' processed successfully, will delete it.", file ) );
                        Files.delete( file );
                    }
                }
                else
                {
                    logger.error( String.format( "File '%s' did not process successfully, will move it to reject directory.", file ) );
                    Files.move( file, rejectPath.resolve( file.getFileName() ), StandardCopyOption.ATOMIC_MOVE );
                }
            }
            catch (IOException ex)
            {
                logger.error( String.format( "File '%s' could not be moved / deleted.", file ), ex );
            }
        }
    }

    @Override
    public void run()
    {
        logger.info( String.format( "Started %s", description ) );
        try
        {
            this.watcher = watchPath.getFileSystem().newWatchService();
            this.watchPath.register( this.watcher, kinds, SensitivityWatchEventModifier.HIGH );
            beforeStart();
        }
        catch (IOException ex)
        {
            logger.error( "Could not start directory watcher.", ex );
            return;
        }
        running.set( true );
        while ( running.get() )
        {
            // wait for key to be signaled
            WatchKey key;
            try
            {
                key = watcher.take();
            }
            catch( ClosedWatchServiceException e )
            {
                watcher = null;
                running.set( false );
                break;
            }
            catch ( InterruptedException e )
            {
                continue;
            }

            for ( WatchEvent<?> event: key.pollEvents() )
            {
                WatchEvent.Kind<?> kind = event.kind();

                // This key is registered only for ENTRY_CREATE events, but an OVERFLOW event can
                // occur regardless if events are lost or discarded.
                if ( StandardWatchEventKinds.OVERFLOW.equals( kind ) )
                {
                    logger.info( "Overflow event, don't know what to do..." );
                    continue;
                }

                // The filename is the context of the event.
                if ( event.context() instanceof Path path )
                    processFileInternal( watchPath.resolve( path ), kind );
            }

            // Reset the key -- this step is critical if you want to
            // receive further watch events.  If the key is no longer valid,
            // the directory is inaccessible so exit the loop.
            if ( !key.reset() )
            {
                logger.error("Unaccessible directory, exiting directory watcher loop.");
                break;
            }
        }
        if ( !running.get() )
            logger.info( String.format( "%s stopped successfully", description ) );
        else
            logger.info( String.format( "%s stopped abnormally", description ) );
    }
}
