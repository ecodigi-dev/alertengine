/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.stream;

import com.ecodigi.io.FileTimeComparator;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PoolingDirectoryFileListener
{
    protected final Log logger = LogFactory.getLog( this.getClass() );

    public static final String DEFAULT_LISTEN_EXT = ".xml";
    public static final int DEFAULT_POOLING_INTERVAL = 100;
    public static final int FILE_PROCESS_POOLING_INTERVAL = 100;
    public static final int DEFAULT_HISTORY_FILE_COUNT = 0;
    public static final long FILE_COUNT_INTERVAL_MILLIS = 1000L;
    public static final long QUEUE_PROCESSING_TIMEOUT_MILLIS = 15000L;

    public static final boolean AUTO_DELETE              = true;
    public static final boolean MANUAL_DELETE            = false;
    public static final boolean PROCESS_FILES_AT_STARTUP = true;
    public static final boolean IGNORE_FILES_AT_STARTUP  = false;
    public static final String REJECT_PATH  = "reject";
    public static final String HISTORY_PATH = "history";

    public static final Set<FileListenerOperation> ALL_OPERATIONS= EnumSet.allOf( FileListenerOperation.class);
    public static final Set<FileListenerOperation> CREATED_OPERATIONS= EnumSet.of( FileListenerOperation.Created );
    private static final Comparator<File> fileTimeComparator = new FileTimeComparator();

    private final File rejectPath;
    private final boolean autoDelete;
    private final Set<FileListenerOperation> operations;
    private final String listenFolder;
    private final FileAlterationObserver observer;
    private final FileAlterationMonitor monitor;
    private final FileListener listener;
    private final File historyPath;
    private final int historyFileCount;
    private long lastFileCountMillis = 0;
    private Thread processThread = null;

    public PoolingDirectoryFileListener( String listenFolder, IOFileFilter fileFilter, boolean autoDelete, boolean processFilesAtStartup, int historyFileCount, int pollingInterval, Set<FileListenerOperation> operations ) throws IOException
    {
        this.autoDelete   = autoDelete;
        this.operations   = operations;
        this.listenFolder = listenFolder;
        this.rejectPath = new File( listenFolder, REJECT_PATH );
        this.rejectPath.mkdirs();
        this.historyPath = new File( listenFolder, HISTORY_PATH );
        this.historyFileCount = historyFileCount;
        if ( historyFileCount > 0 )
        {
            this.historyPath.mkdirs();
            if ( !autoDelete )
                logger.error( String.format( "Invalid historyFileCount %d parameter when autoDelete not enabled", historyFileCount ) );
        }
        observer = FileAlterationObserver.builder()
            .setPath( listenFolder )
            .setFileFilter( fileFilter )
            .setIOCase( IOCase.SYSTEM )
            .get();
        monitor = new FileAlterationMonitor( pollingInterval );
        listener = new FileListener( processFilesAtStartup );
        observer.addListener( listener );
        monitor.addObserver( observer );
    }

    public PoolingDirectoryFileListener( String listenFolder, String extension, boolean autoDelete, boolean processFilesAtStartup, int historyFileCount, int pollingInterval, Set<FileListenerOperation> operations ) throws IOException
    {
        this( listenFolder, FileFilterUtils.suffixFileFilter( StringUtils.isNotBlank(extension) ? extension : DEFAULT_LISTEN_EXT ), autoDelete, processFilesAtStartup, historyFileCount, pollingInterval, operations );
    }

    public static final IOFileFilter getFileFilter( String[] extensions )
    {
        IOFileFilter[] fileFilters = new IOFileFilter[ extensions.length ];
        for ( int i=0; i < extensions.length; i++ )
            fileFilters[i] = FileFilterUtils.suffixFileFilter( extensions[i] );
        return FileFilterUtils.or( fileFilters );
    }

    public PoolingDirectoryFileListener( String listenFolder, String[] extensions, boolean autoDelete, boolean processFilesAtStartup, int historyFileCount, int pollingInterval, Set<FileListenerOperation> operations ) throws IOException
    {
        this( listenFolder, PoolingDirectoryFileListener.getFileFilter(extensions), autoDelete, processFilesAtStartup, historyFileCount, pollingInterval, operations );
    }

    public PoolingDirectoryFileListener( String listenFolder, String extension, int historyFileCount ) throws IOException
    {
        this( listenFolder, FileFilterUtils.suffixFileFilter( StringUtils.isNotBlank(extension) ? extension : DEFAULT_LISTEN_EXT ), AUTO_DELETE, PROCESS_FILES_AT_STARTUP, historyFileCount, DEFAULT_POOLING_INTERVAL, CREATED_OPERATIONS );
    }

    public PoolingDirectoryFileListener( String listenFolder, String extension ) throws IOException
    {
        this( listenFolder, FileFilterUtils.suffixFileFilter( StringUtils.isNotBlank(extension) ? extension : DEFAULT_LISTEN_EXT ), AUTO_DELETE, PROCESS_FILES_AT_STARTUP, DEFAULT_HISTORY_FILE_COUNT, DEFAULT_POOLING_INTERVAL, CREATED_OPERATIONS );
    }

    protected boolean process( File file, FileListenerOperation operation )
    {
        throw new RuntimeException("Not implemented.");
    }

    public void start()
    {
        try
        {
            monitor.start();
            startProcessThread();
        }
        catch (Exception ex)
        {
            logger.error( "Cannot start monitor for directory '" + observer.getDirectory().getName() + "'.", ex);
        }
    }

    public void stop()
    {
        try
        {
            if ( ( processThread != null ) && processThread.isAlive() )
                processThread.interrupt();
            monitor.stop();
        }
        catch (Exception ex)
        {
            logger.error( "Error stopping monitor for directory '" + observer.getDirectory().getName() + "'.", ex);
        }
    }

    protected void startProcessThread()
    {
        processThread = new Thread( new Runnable() {

            @Override
            public void run()
            {
                while ( true )
                {
                    try
                    {
                        processEntries();
                        Thread.sleep( FILE_PROCESS_POOLING_INTERVAL );
                    }
                    catch (InterruptedException ex)
                    {
                        break;
                    }
                }
            }

        });
        logger.info( "Starting internal process Thread" );
        processThread.start();
    }

    public boolean processEntries()
    {
        return( listener.internalProcessFiles() );
    }

    // Reject file while still in listen folder or if it was already moved to history folder
    public boolean rejectFile( String filename )
    {
        boolean success = false;
        try
        {
            File originalFile = new File( listenFolder, filename );
            if  ( originalFile.exists() )
            {
                logger.info( String.format( "Will reject file '%s' from %s", filename, listenFolder ) );
                FileUtils.moveFileToDirectory( originalFile, rejectPath, false );
                success = true;
            }
            else
            {
                File historyFile = new File( historyPath, filename );
                if  ( historyFile.exists() )
                {
                    logger.info( String.format( "Will reject file '%s' from %s", filename, historyPath.toString() ) );
                    FileUtils.moveFileToDirectory( historyFile, rejectPath, false );
                    success = true;
                }
                else
                    logger.error( String.format( "Unable to reject file '%s': not found", filename ) );
            }
        }
        catch( IOException e )
        {
            logger.error( String.format( "Unable to reject file '%s'", filename ), e );
        }
        return( success );
    }

    class FileProcessOperation
    {
        private final File file;
        private final FileListenerOperation operation;

        public FileProcessOperation( File file, FileListenerOperation operation )
        {
            this.file = file;
            this.operation = operation;
        }
    }

    class FileListener extends FileAlterationListenerAdaptor
    {
        private boolean firstStart = true;
        private final ConcurrentLinkedQueue<FileProcessOperation> processingQueue = new ConcurrentLinkedQueue<>();

        public FileListener( boolean processFilesAtStartup )
        {
            firstStart = processFilesAtStartup;
        }

        @Override
        public void onStart( FileAlterationObserver observer )
        {
            if ( firstStart )
            {
                File listenDir = observer.getDirectory();
                if ( !listenDir.exists() || !listenDir.isDirectory() )
                    throw new RuntimeException( "FileListener::onStart - The path '" + listenDir.getPath() + "' does not exists or is not a directory." );
                logger.info( "Starting FileListener on: '" + listenDir.getPath() + "'.");
                File[] files = listenDir.listFiles( observer.getFileFilter() );
                logger.info( String.format( "There are %d files to process", files.length ) );
                if ( files.length > 0 )
                {
                    // Sort files, from older to newer, before processing
                    Arrays.sort( files, ( File f1, File f2 ) -> ( Long.compare( f1.lastModified(), f2.lastModified() ) ) );

                    for ( File file : files )
                    {
                        logger.info( "FileListener::onStart - Will process file: '" + file.getPath() + "'." );
                        processingQueue.add( new FileProcessOperation( file, FileListenerOperation.Created ) );
                    }
                }
                firstStart = false;
            }
        }

        public boolean internalProcessFiles()
        {
            if ( processingQueue.isEmpty() )
                return false;

            FileProcessOperation op;
            long startMillis = System.currentTimeMillis();
            while ( ( op = processingQueue.poll() ) != null )
            {
                try
                {
                    if ( process( op.file, op.operation ) )
                    {
                        if ( autoDelete )
                        {
                            if ( historyFileCount > 0 )
                            {
                                if ( logger.isDebugEnabled() )
                                    logger.debug( String.format( "File '%s' will be moved to history directory.", op.file.toString() ) );
                                Files.move( op.file.toPath(), historyPath.toPath().resolve( op.file.getName() ), StandardCopyOption.ATOMIC_MOVE );
                                manageHistory();
                            }
                            else
                            {
                                if ( logger.isDebugEnabled() )
                                    logger.debug( String.format( "File '%s' processed successfully, will delete it.", op.file.toString() ) );
                                Files.delete( op.file.toPath() );
                            }
                        }
                    }
                    else
                    {
                        logger.error( String.format( "File '%s' did not process successfully, will move it to reject directory.", op.file.toString() ) );
                        Files.move( op.file.toPath(), rejectPath.toPath().resolve( op.file.getName() ), StandardCopyOption.ATOMIC_MOVE );
                    }
                }
                catch (IOException ex)
                {
                    logger.error( String.format( "File '%s' could not be moved / deleted.", op.file.toString() ), ex );
                }
                catch (Throwable ex)
                {
                    logger.error( String.format( "File '%s' could not be processed.", op.file.toString() ), ex );
                }

                // Yield for thread heartbeat
                long elapsedMillis = System.currentTimeMillis() - startMillis;
                if ( elapsedMillis > QUEUE_PROCESSING_TIMEOUT_MILLIS )
                    return( false );

            }
            return true;
        }

        private void manageHistory()
        {
            long now = System.currentTimeMillis();
            if ( ( now < lastFileCountMillis ) || ( ( now - lastFileCountMillis ) > FILE_COUNT_INTERVAL_MILLIS ) )
            {
                File files[] = historyPath.listFiles();
                if ( files.length > historyFileCount )
                {
                    int filesToDelete = files.length - historyFileCount;
                    Arrays.sort( files, fileTimeComparator );
                    for( int i = 0; i < filesToDelete; i++ )
                    {
                        File file = files[ i ];
                        try
                        {
                            file.delete();
                        }
                        catch( Throwable e )
                        {
                            logger.error( String.format( "Unable to delete file %s", file.getName() ), e );
                        }
                    }
                }
                lastFileCountMillis = now;
            }
        }

        @Override
        public void onFileCreate( File file )
        {
            logger.debug( "File created: '" + file.getPath() + "'." );
            if ( operations.contains( FileListenerOperation.Created ) )
                processingQueue.add( new FileProcessOperation(file, FileListenerOperation.Created) );
        }

        @Override
        public void onFileDelete(File file)
        {
            logger.debug( "File deleted: '" + file.getPath() + "'." );
            if ( operations.contains( FileListenerOperation.Deleted ) )
                processingQueue.add( new FileProcessOperation(file, FileListenerOperation.Deleted) );
        }

        @Override
        public void onFileChange(File file)
        {
            logger.debug( "File changed: '" + file.getPath() + "'." );
            if ( operations.contains( FileListenerOperation.Modified ) )
                processingQueue.add( new FileProcessOperation(file, FileListenerOperation.Modified) );
        }
    }
}
