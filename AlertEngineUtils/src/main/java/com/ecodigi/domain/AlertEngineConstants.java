/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain;

public class AlertEngineConstants
{
    // Configurations
    public static final String EVENT_SOURCE_CONFIG_EXTENSION   = ".source";
    public static final String ALERT_CONFIG_EXTENSION          = ".alert";
    public static final String FEED_CONFIG_EXTENSION           = ".feed";
    public static final String DOMAIN_CONFIG_EXTENSION         = ".domain";
    public static final String NAMED_VALUES_CONFIG_EXTENSION   = ".namedval";
    public static final String HEALTH_REPORT_FILENAME          = "health-report";
    public static final String HEALTH_REPORT_EXTENSION         = ".health";

    // Events
    public static final String EVENTS_FILENAME_EXTENSION     = ".events";
    public static final String ALERT_FILE_EXTENSION          = ".alert";
    public static final String MESSAGE_FILE_EXTENSION        = ".message";

    // Status
    public static final String CEP_ACTION_SUCCESS            = "success";
    public static final String EVENTS_FILE_ROOT_NODE         = "events";

    // Other
    public static final String VALIDATE_EXPRESSION_PREFIX    = "__validate__";

    public static final String ALERT_CONFIG_REGEX            = "^(alert\\-id)(?<ID>[0-9]+)(\\.alert)$";
    public static final String EVENT_SOURCE_CONFIG_REGEX     = "^(eventsource\\-id)(?<ID>[0-9]+)(\\.source)$";
    public static final String FEED_CONFIG_REGEX             = "^(eplfeed\\-)(?<ID>[\\p{Print}]+)(\\.feed)$";
    public static final String VALIDATION_CONFIG_REGEX       = "^(__validate__)(?<ID>[0-9]+)(\\.feed)$";
    public static final String CONFIG_REGEX_ID_GROUP_NAME    = "ID";

    public static final String EVENT_NAME_TIMESTAMP          = "Message";
    public static final String EVENT_TIMESTAMP_ATTRIBUTE     = "tstamp";

    public static final String INTERNAL_ATTR_TIMESTAMP      = "timestamp";
    public static final String INTERNAL_ATTR_TIMESTAMPMS    = "timestampms";
    public static final String INTERNAL_ATTR_TSTAMP         = "tstamp";
    public static final String INTERNAL_ATTR_SESSION_ID     = "sessionId";
    public static final String INTERNAL_ATTR_MESSAGE_ID     = "messageId";
    public static final String INTERNAL_ATTR_COMP_ID        = "compID";
}
