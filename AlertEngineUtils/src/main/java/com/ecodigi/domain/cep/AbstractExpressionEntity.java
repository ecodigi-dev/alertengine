/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.cep;

import java.io.Serializable;

public abstract class AbstractExpressionEntity implements Serializable
{
    private static final long serialVersionUID = 100L;

    private Integer createOrder;
    private Long id;
    private String name;
    private String expression;
    private boolean enabled = true;

    public AbstractExpressionEntity()
    {
    }

    public AbstractExpressionEntity( Long id, Integer createOrder, String name, String expression, Boolean enabled )
    {
        this.id            = id;
        this.createOrder   = createOrder;
        this.name          = name;
        this.expression    = expression;
        this.enabled       = enabled;
    }

    public Integer getCreateOrder()
    {
        return createOrder;
    }

    public void setCreateOrder( Integer createOrder )
    {
        this.createOrder = createOrder;
    }

    public Long getId()
    {
        return id;
    }

    public void setId( Long id )
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public String getExpression()
    {
        return expression;
    }

    public void setExpression( String expression )
    {
        this.expression = expression;
    }
    
    public boolean isEnabled() 
    {
        return enabled;
    }

    public void setEnabled( boolean enabled )
    {
        this.enabled = enabled;
    }

    public boolean equals( AbstractExpressionEntity other )
    {
        return( ( id != null )         && id.equals( other.id ) &&
                ( name != null )       && name.equals( ( other.name ) ) &&
                ( expression != null ) && expression.equals( other.expression ) &&
                ( enabled == other.enabled ) );
    }
}
