/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.cep;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;

@XStreamAlias( "HealthReport" )
public class HealthReport implements Serializable
{
    private static final long serialVersionUID = 100L;
    
    private String version;
    private long uptime;
    private long timestamp;
    private long streamTime;
    private long heartbeat;
    private long eventsIn;
    private long alertsOut;
    private long feedsOut;
    private long freeMemory;
    private long maxMemory;
    private long totalMemory;
    private int filesystemQueueCount;
    private long filesystemQueueSize;
    private int requestQueueSize;
    private int inputQueueSize;
    private int outputQueueSize;
    private int eventSources;
    private int alerts;
    private int feeds;

    public HealthReport()
    {
    }

    public HealthReport( String version, long uptime, long timestamp, long streamTime, long heartbeat,
                         long eventsIn, long alertsOut, long feedsOut,
                         int filesystemQueueCount, long filesystemQueueSize,
                         int requestQueueSize, int inputQueueSize, int outputQueueSize )
    {
        this.version              = version;
        this.uptime               = uptime;
        this.timestamp            = timestamp;
        this.streamTime           = streamTime;
        this.heartbeat            = heartbeat;
        this.eventsIn             = eventsIn;
        this.alertsOut            = alertsOut;
        this.feedsOut             = feedsOut;
        this.freeMemory           = Runtime.getRuntime().freeMemory();
        this.maxMemory            = Runtime.getRuntime().maxMemory();
        this.totalMemory          = Runtime.getRuntime().totalMemory();
        this.filesystemQueueCount = filesystemQueueCount;
        this.filesystemQueueSize  = filesystemQueueSize;
        this.requestQueueSize     = requestQueueSize;
        this.inputQueueSize       = inputQueueSize;
        this.outputQueueSize      = outputQueueSize;
    }

    public void setExpressionCount( int eventSources, int alerts, int feeds )
    {
        this.eventSources = eventSources;
        this.alerts       = alerts;
        this.feeds        = feeds;
    }

    public String getVersion()
    {
        return version;
    }

    public long getUptime()
    {
        return uptime;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public long getStreamTime()
    {
        return streamTime;
    }

    public long getHeartbeat()
    {
        return heartbeat;
    }

    public long getEventsIn()
    {
        return eventsIn;
    }

    public long getAlertsOut()
    {
        return alertsOut;
    }

    public long getFeedsOut()
    {
        return feedsOut;
    }

    public long getFreeMemory()
    {
        return freeMemory;
    }

    public long getMaxMemory()
    {
        return maxMemory;
    }

    public long getTotalMemory()
    {
        return totalMemory;
    }
    
    public int getFilesystemQueueCount()
    {
        return filesystemQueueCount;
    }

    public long getFilesystemQueueSize()
    {
        return filesystemQueueSize;
    }

    public int getRequestQueueSize()
    {
        return requestQueueSize;
    }

    public int getInputQueueSize()
    {
        return inputQueueSize;
    }

    public int getOutputQueueSize()
    {
        return outputQueueSize;
    }

    public int getEventSources()
    {
        return eventSources;
    }

    public int getAlerts()
    {
        return alerts;
    }

    public int getFeeds()
    {
        return feeds;
    }    
}
