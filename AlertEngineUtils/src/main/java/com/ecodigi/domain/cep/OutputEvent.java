/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.cep;

import com.ecodigi.domain.utils.EventSeverity;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@XStreamAlias( "OutputEvent" )
public class OutputEvent implements Serializable
{
    private static final long serialVersionUID = 100L;

    private OutputEventType eventType;
    private Long            sourceId;
    private String          sourceName;

    private Long            timestamp;
    private EventSeverity   eventSeverity;
    private String          message;

    private final List<OutputEventEntry> entries = new ArrayList<OutputEventEntry>();

    public OutputEvent()
    {
    }
    
    public OutputEvent( OutputEventType eventType, Long timestamp )
    {
        this.eventType = eventType;
        this.timestamp = timestamp;
    }

    public OutputEvent( OutputEventType eventType, Long sourceId, Long timestamp )
    {
        this.eventType = eventType;
        this.sourceId  = sourceId;
        this.timestamp = timestamp;
    }

    //TODO: Alterar o construtor para receber os entries
    public OutputEvent( OutputEventType eventType, String sourceName, Long timestamp )
    {
        this.eventType  = eventType;
        this.sourceName = sourceName;
        this.timestamp  = timestamp;
    }

    public OutputEvent( AbstractExpressionEntity entity, EventSeverity eventSeverity, String message, Long timestamp )
    {
        this.eventType     = OutputEventType.getEventType( entity );
        this.sourceId      = entity.getId();
        this.sourceName    = entity.getName();
        this.timestamp     = timestamp;
        this.eventSeverity = eventSeverity;
        this.message       = message;
    }

    public OutputEventType getEventType()
    {
        return eventType;
    }

    public void setEventType( OutputEventType eventType )
    {
        this.eventType = eventType;
    }

    public void setSourceId( Long sourceId )
    {
        this.sourceId = sourceId;
    }

    public Long getSourceId()
    {
        return sourceId;
    }

    public void setSourceName( String sourceName )
    {
        this.sourceName = sourceName;
    }

    public String getSourceName()
    {
        return sourceName;
    }

    public void setTimestamp( Long timestamp )
    {
        this.timestamp = timestamp;
    }

    public Long getTimestamp()
    {
        return timestamp;
    }
    
    public EventSeverity getEventSeverity()
    {
        return eventSeverity;
    }

    public void setEventSeverity( EventSeverity eventSeverity )
    {
        this.eventSeverity = eventSeverity;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage( String message )
    {
        this.message = message;
    }

    public List<OutputEventEntry> getEntries()
    {
        return entries;
    }
    
    public void addEntry( String name, String value )
    {
        entries.add( new OutputEventEntry( name, value ) );
    }

    public Map<String,String> getEntryMap()
    {
        Map<String,String> contentMap = new LinkedHashMap<String, String>();
        for( OutputEventEntry entry: entries )
            contentMap.put( entry.getName(), entry.getValue() );
        return( contentMap );
    }
    
    public String getEntriesAsString()
    {
        StringBuilder str = new StringBuilder();
        for( OutputEventEntry entry: entries )
            str.append( entry.getName() ).append( "='" ).append( entry.getValue() ).append( "', " );
        return( str.toString() );
    }

    @Override
    public String toString()
    {
        StringBuilder str = new StringBuilder();
        if ( eventType != null )
            str.append( "Type " ).append( eventType.name() ).append( " " );
        if ( sourceName != null )
            str.append( "Name '" ).append( sourceName ).append( "' " );
        if ( sourceId != null )
            str.append( "Id " ).append( sourceId ).append( " " );
        if ( eventSeverity != null )
            str.append( "Severity " ).append( eventSeverity.name() ).append( " " );
        if ( message != null )
            str.append( "Message '" ).append( message ).append( "' " );
        str.append( "Entries: " ).append( entries.size() );
        return( str.toString() );
    }
}
