/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.cep;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.Serializable;
import java.util.Map;

@XStreamAlias( "NamedValues" )
public class NamedValuesConfiguration implements Serializable
{
    private static final long serialVersionUID = 100L;
    
    private Long id;
    private Long timestamp;
    private String name;
    private Map<String,Long> valueMap;
    
    public NamedValuesConfiguration()
    {
    }

    public NamedValuesConfiguration( long id, long timestamp, String name )
    {
        this.id        = id;
        this.timestamp = timestamp;
        this.name      = name;
    }

    public Long getId()
    {
        return id;
    }

    public void setId( Long id )
    {
        this.id = id;
    }

    public Long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp( Long timestamp )
    {
        this.timestamp = timestamp;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public Map<String, Long> getValueMap()
    {
        return valueMap;
    }

    public void setValueMap( Map<String, Long> valueMap )
    {
        this.valueMap = valueMap;
    }
}
