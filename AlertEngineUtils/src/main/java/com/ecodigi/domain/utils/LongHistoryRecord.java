/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LongHistoryRecord implements Serializable
{
    private static final long serialVersionUID = 100L;
    private static final long NO_VALUE         = -1;

    private final Long[] values;
    private int    index;

    private final AtomicLong    minValue   = new AtomicLong( NO_VALUE );
    private final AtomicBoolean minUpdated = new AtomicBoolean( false );
    private final AtomicLong    maxValue   = new AtomicLong( NO_VALUE );
    private final AtomicBoolean maxUpdated = new AtomicBoolean( false );
    private final AtomicBoolean filled     = new AtomicBoolean( false );

    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    private final NumberFormat nf = new DecimalFormat( "###,###,###,##0" );

    public LongHistoryRecord( int historySize )
    {
        values   = new Long[ historySize ];
        index    = 0;
    }

    public void reset()
    {
        lock.writeLock().lock();
        try
        {
            index = 0;
            for( int i = 0; i < values.length; i++ )
                values[ i ] = null;
            minValue.set( NO_VALUE );
            maxValue.set( NO_VALUE );
            minUpdated.set( false );
            maxUpdated.set( false );
            filled.set( false );
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public void put( Long value )
    {
        lock.writeLock().lock();
        try
        {
            values[ index ] = value;
            if ( ++index >= values.length )
            {
                index = 0;
                filled.set( true );
            }
            minUpdated.set( true );
            maxUpdated.set( true );
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    public boolean isEmpty()
    {
        lock.readLock().lock();
        try
        {
            for ( Long value : values )
            {
                if ( value != null )
                    return( false );
            }
            return( true );
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    public boolean isFilled()
    {
        return( filled.get() );
    }

    //TODO: implementar getLast()

    public int getQty()
    {
        lock.readLock().lock();
        try
        {
            int  count = 0;
            for ( Long value : values )
            {
                if ( value != null )
                    count++;
            }
            return( count );
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    public Long getMin()
    {
        // Do not compute min again, if no value was added
        if ( !minUpdated.get() )
        {
            // Workaround: AtomicLong doesn't accept null values
            long minVal = minValue.get();
            if ( minVal == NO_VALUE )
                return( null );
            else
                return( minVal );
        }
        
        lock.readLock().lock();
        try
        {
            Long minVal = null;
            for ( Long value : values )
            {
                if ( value != null )
                {
                    if ( ( minVal == null ) || ( minVal > value ) )
                        minVal = value;
                }
            }

            if ( minVal != null )
            {
                minValue.set( minVal );
                minUpdated.set( false );
            }

            return( minVal );
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    public Long getMax()
    {
        // Do not compute max again, if no value was added
        if ( !maxUpdated.get() )
        {
            // Workaround: AtomicLong doesn't accept null values
            long maxVal = maxValue.get();
            if ( maxVal == NO_VALUE )
                return( null );
            else
                return( maxVal );
        }

        lock.readLock().lock();
        try
        {
            Long maxVal = null;
            for ( Long value : values )
            {
                if ( value != null )
                {
                    if ( ( maxVal == null ) || ( maxVal < value ) )
                        maxVal = value;
                }
            }

            if ( maxVal != null )
            {
                maxValue.set( maxVal );
                maxUpdated.set( false );
            }

            return( maxVal );
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    public Long getAvg()
    {
        lock.readLock().lock();
        try
        {
            long sum   = 0;
            int  count = 0;
            for ( Long value : values ) 
            {
                if ( value != null )
                {
                    sum += value;
                    count++;
                }
            }
            return( count > 0 ? sum / count : null );
        }
        finally
        {
            lock.readLock().unlock();
        }
    }
    
    public String getStatistics()
    {
        lock.readLock().lock();
        try
        {
            StringBuilder stat = new StringBuilder();

            stat.append( "Qty " ).append( nf.format( getQty() ) );

            Long min = getMin();
            if ( min != null )
                stat.append( " Min " ).append( nf.format( min ) );

            Long max = getMax();
            if ( max != null )
                stat.append( " Max " ).append( nf.format( max ) );

            Long avg = getAvg();
            if ( avg != null )
                stat.append( " Avg " ).append( nf.format( avg ) );

            return( stat.toString() );
        }
        finally
        {
            lock.readLock().unlock();
        }
    }
}
