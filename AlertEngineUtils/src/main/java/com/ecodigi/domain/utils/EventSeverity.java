/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.utils;

import java.util.Arrays;
import java.util.List;

public enum EventSeverity
{
    Info, Warning, Error, Severe;

    private static final List<EventSeverity> allValues = Arrays.asList( EventSeverity.values() );

    public static List<EventSeverity> getAllValues()
    {
        return allValues;
    }

    public static EventSeverity fromString( String name )
    {
        try
        {
            return EventSeverity.valueOf( name );
        }
        catch ( Exception ex )
        {
        }
        return null;
    }
    
    public static String[] getNames()
    {
        EventSeverity[] modes = EventSeverity.values();
        String[] names = new String[ modes.length ];
        for( int i = 0; i < modes.length; i++ )
            names[ i ] = modes[ i ].name();
        return( names );
    }
    
    public int getCodeForSNMP()
    {
        return( ordinal() + 1 );
    }
}
