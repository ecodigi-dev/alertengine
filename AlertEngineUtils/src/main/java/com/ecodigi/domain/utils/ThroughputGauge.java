/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.utils;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.atomic.AtomicLong;

public class ThroughputGauge implements Serializable
{
    private static final long serialVersionUID = 100L;

    private static final long MILLIS_IN_A_SECOND = 1000L;
    private static final long MILLIS_IN_A_MINUTE = MILLIS_IN_A_SECOND * 60L;

    private final NumberFormat nf = new DecimalFormat( "###,###,###,##0" );

    private String unitStr = "m";

    private boolean firstTime = true;
    private final AtomicLong forwardCount = new AtomicLong( 0 );
    private final AtomicLong reverseCount = new AtomicLong( 0 );

    private final LongHistoryRecord processedHistory = new LongHistoryRecord( 60 );
    private final LongHistoryRecord millisHistory    = new LongHistoryRecord( 60 );

    private long prevTimestampMillisSec = 0;
    private long prevProcessedSec       = 0;
    private final AtomicLong rateSec            = new AtomicLong( 0 );

    public ThroughputGauge()
    {
    }
    
    public ThroughputGauge( String unitStr )
    {
        this.unitStr = unitStr;
    }
    
    public long getForwardCount()
    {
        return( forwardCount.get() );
    }

    public void incForwardCount()
    {
        forwardCount.incrementAndGet();
    }

    public long getReverseCount()
    {
        return( reverseCount.get() );
    }

    public void incReverseCount()
    {
        reverseCount.incrementAndGet();
    }

    public long getTotalCount()
    {
        return( forwardCount.get() + reverseCount.get() );
    }

    public String getTotalCountStr()
    {
        StringBuilder countStr = new StringBuilder();
        countStr.append( nf.format( forwardCount.get() ) ).append( " F + " );
        countStr.append( nf.format( reverseCount.get() ) ).append( " R = " );
        countStr.append( nf.format( forwardCount.get() + reverseCount.get() ) );
        return( countStr.toString() );
    }
    
    private long computeRate( long deltaProcessed, long deltaMillis, long millisInPeriod )
    {
        return( Math.round( ( ( ( double )deltaProcessed * ( double )millisInPeriod ) ) / ( double )deltaMillis ) );
    }
    
    // This method should be called every second
    public long computeRate()
    {
        long now = System.currentTimeMillis();
        long processed = getTotalCount();
        
        millisHistory.put( now );
        processedHistory.put( processed );
        
        if ( firstTime )
        {
            // Initialize timestamps
            prevTimestampMillisSec = now;
            prevProcessedSec       = processed;
            firstTime              = false;
        }
        else
        {
            // Compute second rates
            long deltaMillis    = now - prevTimestampMillisSec;
            long deltaProcessed = processed - prevProcessedSec;
            rateSec.set( computeRate( deltaProcessed, deltaMillis, MILLIS_IN_A_SECOND ) );
            
            prevTimestampMillisSec = now;
            prevProcessedSec       = processed;
        }
        
        return( rateSec.get() );
    }

    public long getRateSec()
    {
        return( rateSec.get() );
    }

    public long getRateMin()
    {
        if ( millisHistory.isEmpty() )
            return( 0 );

        long deltaMillis    = millisHistory.getMax() - millisHistory.getMin();
        long deltaProcessed = processedHistory.getMax() - processedHistory.getMin();
        if ( deltaMillis > 0 )
            return( computeRate(deltaProcessed, deltaMillis, MILLIS_IN_A_MINUTE ) );
        else
            return( 0 );
    }
    
    public String getRateStr()
    {
        StringBuilder rate = new StringBuilder();
        rate.append( nf.format( getRateSec() ) ).append( " " ).append( unitStr ).append( "/sec - " );
        rate.append( nf.format( getRateMin() ) ).append( " " ).append( unitStr ).append( "/min" );
        return( rate.toString() );
    }
}
