/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils
{
    public static final String ISO_TIMESTAMP_FORMAT_STR    = "yyyy-MM-dd_HH_mm_ss_SSS";
    public static final String ISO_TIME_FORMAT_STR         = "HH_mm_ss_SSS";
    public static final String ISO_HR_TIMESTAMP_FORMAT_STR = "yyyy/MM/dd HH:mm:ss.SSS";
    public static final String ISO_HR_DATETIME_FORMAT_STR  = "yyyy/MM/dd HH:mm:ss";
    public static final String ISO_HR_DATE_FORMAT_STR      = "yyyy/MM/dd";
    public static final String ISO_HR_TIME_FORMAT_STR      = "HH:mm:ss";
    public static final String FIX_TIMESTAMP_FORMAT_STR    = "yyyyMMdd-HH:mm:ss.SSS";
    public static final String FIX_DATE_TIME_FORMAT_STR    = "yyyyMMdd-HH:mm:ss";
    public static final String DAY_FORMAT_STR              = "dd";
    public static final String HOUR_FORMAT_STR             = "HH";
    public static final String MINUTE_FORMAT_STR           = "mm";
    public static final long   MILLISECONDS_IN_A_DAY       = 1000L * 60L * 60L * 24L;

    private static final SimpleDateFormat gmtIsoTimestampFormat   = new SimpleDateFormat( ISO_TIMESTAMP_FORMAT_STR );
    private static final SimpleDateFormat gmtIsoHRTimestampFormat = new SimpleDateFormat( ISO_HR_TIMESTAMP_FORMAT_STR );
    private static final SimpleDateFormat gmtIsoHRDateTimeFormat  = new SimpleDateFormat( ISO_HR_DATETIME_FORMAT_STR );
    private static final SimpleDateFormat gmtIsoHRDateFormat      = new SimpleDateFormat( ISO_HR_DATE_FORMAT_STR );
    private static final SimpleDateFormat gmtIsoHRTimeFormat      = new SimpleDateFormat( ISO_HR_TIME_FORMAT_STR );
    private static final SimpleDateFormat gmtFixTimestampFormat   = new SimpleDateFormat( FIX_TIMESTAMP_FORMAT_STR );
    private static final SimpleDateFormat gmtFixDateTimeFormat    = new SimpleDateFormat( FIX_DATE_TIME_FORMAT_STR );
    private static final SimpleDateFormat gmtDayFormat            = new SimpleDateFormat( DAY_FORMAT_STR );
    private static final SimpleDateFormat gmtHourFormat           = new SimpleDateFormat( HOUR_FORMAT_STR );
    private static final SimpleDateFormat gmtMinuteFormat         = new SimpleDateFormat( MINUTE_FORMAT_STR );

    static
    {
        gmtIsoTimestampFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtIsoHRTimestampFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtIsoHRDateTimeFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtIsoHRDateFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtIsoHRTimeFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtFixTimestampFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtFixDateTimeFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtDayFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtHourFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
        gmtMinuteFormat.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
    }
    
    public static String getDateTimeZoneString( Date date, TimeZone timeZone )
    {
        DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        df.setTimeZone( timeZone );
        return( df.format( date ) + " " + timeZone.getDisplayName( false, TimeZone.SHORT ) );
    }

    public static String getIsoHRTimeString( long millis )
    {
        return( getIsoHRTimeString( new Date( millis ) ) );
    }

    public static String getIsoHRTimeString( Date date )
    {
        if ( date == null )
            return( null );

        synchronized( gmtIsoHRTimeFormat )
        {
            return( gmtIsoHRTimeFormat.format( date ) );
        }
    }

    public static String getIsoTimestampString( long millis )
    {
        return( getIsoTimestampString( new Date( millis ) ) );
    }
    
    public static String getIsoTimestampString( Date date )
    {
        if ( date == null )
            return( null );
        
        synchronized( gmtIsoTimestampFormat )
        {
           return( gmtIsoTimestampFormat.format( date ) );
        }
    }
    
    public static String getDurationStr( long durationMillis )
    {
        if ( durationMillis < 1500 )
            return( String.valueOf( durationMillis ) + " ms" );
        else
        {
            if ( durationMillis < MILLISECONDS_IN_A_DAY )
                return( getIsoHRTimeString( durationMillis + 500 ) );
            else
            {
                long days = durationMillis / MILLISECONDS_IN_A_DAY;
                return( String.format( "%d days %s", days, getIsoHRTimeString( durationMillis % MILLISECONDS_IN_A_DAY ) ) );
            }
        }
    }
    
    public static String getIsoHRDateTimeString( long millis )
    {
        return( getIsoHRDateTimeString( new Date( millis ) ) );
    }
    
    public static String getIsoHRDateTimeString( Date date )
    {
        if ( date == null )
            return( null );
        
        synchronized( gmtIsoHRDateTimeFormat )
        {
            return( gmtIsoHRDateTimeFormat.format( date ) );
        }
    }
    
}
