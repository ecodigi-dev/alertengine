/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.util.xml;

import com.ecodigi.domain.OnEventNode;
import com.ecodigi.service.XMLParser;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class XMLParserTest
{
    private static final String XML_DOC  = "src/test/resources/events-sample.xml";
    private static final String XML_DOC2 = "src/test/resources/events-sample2.xml";

    private final XMLParser xmlParser;
    protected Log logger = LogFactory.getLog( this.getClass() );
    
    public XMLParserTest()
    {
        xmlParser = new XMLParser();
        xmlParser.initialize( true );
    }

    @Test
    public void basicTest() throws ParserConfigurationException, SAXException, IOException
    {
        Node n = xmlParser.parse( XML_DOC );
        logger.info( "Root on document: " + n.getNodeName() );
        String name = n.getChildNodes().item(0).getNodeName();
        Assert.assertEquals( "events", name );
        logger.info( "  " + name );
    }
    
    //@Test
    public void testSAXParser()
    {
        logger.info( "*** SAX Parser ***" );
        xmlParser.parse( new File( XML_DOC2 ), "events", new OnEventNode()
        {
            @Override
            public boolean onEvent( Node node   )
            {
                logger.info( String.format( "----- New node '%s' ----- ", node.getNodeName() ) );
                xmlParser.printNode( node );
                return( true );
            }
        });
    }
}
