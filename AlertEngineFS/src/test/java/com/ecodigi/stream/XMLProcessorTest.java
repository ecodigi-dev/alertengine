/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.stream;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;

import com.ecodigi.service.XMLParser;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

public class XMLProcessorTest
{
    protected Log logger = LogFactory.getLog( this.getClass() );
    private final XMLParser xmlParser;
    private final String XML_FILE_EXTENSION = ".xml";
    private final Path SRC_XML_FILE = Paths.get("src/test/resources/events-sample.xml");
    private final String BASE_DIR = "target/test/xml";
    private final String XML_FILE = "/events-sample";

    public XMLProcessorTest()
    {
        xmlParser = new XMLParser();
        xmlParser.initialize( true );
    }

    private void copyAndFixSchema( Path src, Path dest ) throws IOException
    {
        Charset charset = StandardCharsets.UTF_8;
        String content = new String(Files.readAllBytes(src), charset);
        content = content.replaceAll("events-schema.xsd", "../../../src/test/resources/events-schema.xsd");
        Files.write( dest, content.getBytes(charset), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING );
    }

    @Test
    public void dummyTest()
    {

    }

    public void testProcessor() throws InterruptedException
    {
        try
        {
            DirectoryFileListener dirListener = new DirectoryFileListener( BASE_DIR, DirectoryFileListener.AUTO_DELETE, StandardWatchEventKinds.ENTRY_CREATE )
            {
                @Override
                protected boolean acceptFile(Path file)
                {
                    return ( file.toString().endsWith( ".xml" ) && checkMimeType( file, XML_MIME_TYPE ) );
                }

                @Override
                public boolean process( Path file, WatchEvent.Kind<?> eventKind )
                {
                    return( xmlParser.parseAsNode( file, "events" ) != null );
                }
            };
            Path dest = Paths.get( BASE_DIR + XML_FILE + XML_FILE_EXTENSION );
            Path reject = Paths.get( BASE_DIR + "/reject" + XML_FILE + XML_FILE_EXTENSION );
            Files.deleteIfExists( reject );
            copyAndFixSchema( SRC_XML_FILE, dest );
            dirListener.start();
            Thread.sleep( 1000 );
            Assert.assertFalse( Files.exists( dest ) );
            Assert.assertFalse( Files.exists( reject ) );
            Files.deleteIfExists( reject );
            copyAndFixSchema( SRC_XML_FILE, dest );
            Thread.sleep( 1000 );
            Assert.assertFalse( Files.exists( dest ) );
            Assert.assertFalse( Files.exists( reject ) );
            dirListener.stop();
        }
        catch ( IOException ex )
        {
            logger.error( ex );
        }
    }
}
