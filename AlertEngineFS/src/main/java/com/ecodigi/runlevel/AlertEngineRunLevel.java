/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.runlevel;

import java.io.IOException;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ecodigi.cep.CEPManager;
import com.ecodigi.cep.CEPSettings;
import com.ecodigi.esper.EsperEngine;
import com.ecodigi.service.CEPConfigService;
import com.ecodigi.service.EventConsumer;
import com.ecodigi.service.XMLParser;
import com.ecodigi.util.DateUtils;

@Component( "alertEngineRunLevel" )
public class AlertEngineRunLevel extends AbstractManagedRunLevelHandler
{
    public static final String SERVICE_NAME             = "Alert Engine FS";
    public static final String ALERT_ENGINE_VERSION_STR = "Alert Engine FS " + AlertEngineRunLevel.class.getPackage().getImplementationVersion();
    public static final String ALERT_ENGINE_REVISION    = "1";

    private static final long SERVICE_HEALTH_INTERVAL_MILLIS = 60 * 1000;

    @Autowired
    private CEPSettings cepSettings;

    @Autowired
    private EsperEngine esperEngine;

    @Autowired
    private CEPManager cepManager;

    @Autowired
    private CEPConfigService cepConfigService;

    @Autowired
    private EventConsumer eventConsumer;

    @Autowired
    private XMLParser xmlParser;

    @Value( "${cep.enabled}" )
    private Boolean cepEnabled;

    private boolean enabled;

    private final AtomicBoolean serviceThreadsHealthy = new AtomicBoolean( true );

    @Override
    public boolean handleServiceState( String actionName )
    {
        if ( enabled )
            return( super.handleServiceState( actionName ) );
        else
            return( false );
    }

    @Override
    public boolean handleServiceState( String actionName, ServiceRunLevelType serviceRunLevelType, Long actionIntervalMillis )
    {
        if ( enabled )
            return super.handleServiceState( actionName, serviceRunLevelType, actionIntervalMillis );
        else
            return( false );
    }

    @Override
    protected String getServiceName()
    {
        return( SERVICE_NAME );
    }

    @Override
    protected TimeZone getEngineTimeZone()
    {
        return( cepSettings.getCEPEngineTimeZone() );
    }

    @Override
    protected String getVersionStr()
    {
        return( ALERT_ENGINE_VERSION_STR );
    }

    @Override
    protected String getRevision()
    {
        return( ALERT_ENGINE_REVISION );
    }

    @Override
    public boolean onInitialization()
    {
        enabled = ( ( cepEnabled != null ) && cepEnabled );
        return( true );
    }

    @Override
    public boolean onStartProcessing()
    {
        if ( !enabled )
        {
            logger.error( "Alert Engine Services are not enabled" );
            return( false );
        }

        logger.info( "Will initialize CEP services" );
        long startMillis = System.currentTimeMillis();

        cepConfigService.initializeDomains();
        xmlParser.initialize();
        esperEngine.start();
        cepConfigService.initializeExpressions();
        try
        {
            cepConfigService.initializeFileListener();
            eventConsumer.initialize();
        }
        catch (IOException e)
        {
            logger.error( "Alert Engine core failed to initialize", e );
        }

        logger.info( "Alert Engine core services initialized in " + DateUtils.getDurationStr( System.currentTimeMillis() - startMillis ) );
        return( true );
    }

    @Override
    public boolean onStopProcessing()
    {
        logger.info( "Will shut down expressions" );
        eventConsumer.close();
        cepConfigService.close();
        cepManager.close();

        logger.info( "Will shut down Esper Engine" );
        esperEngine.stop();
        xmlParser.close();

        return( true );
    }

    @Override
    public boolean onShutdown()
    {
        logger.info( "Alert Engine process was whut down" );
        return( true );
    }

    // TODO: chamar esse m�todo da camada de apresenta��o para mostrar o estado de falha
    public boolean isServiceThreadsHealthy()
    {
        return( serviceThreadsHealthy.get() );
    }

    @Scheduled( initialDelay = ServiceRunLevelType.DEFAULT_APPLICATION_LEVEL_INITIAL_WAIT, fixedDelay = SERVICE_HEALTH_INTERVAL_MILLIS )
    public void checkServicesHealth()
    {
        if ( !handleServiceState( "Alert Engine Services Health", ServiceRunLevelType.Basic, SERVICE_HEALTH_INTERVAL_MILLIS ) )
            return;

        serviceThreadsHealthy.set( isServiceHealthy() );
        if ( !serviceThreadsHealthy.get() )
        {
            logger.error( "Services below may be unavailable or unstable" );
            reportNotHealthyActions( getEngineTimeZone() );
        }

        // Esper service thread is managed outside of the RunLevel framework
        esperEngine.checkThreadHealth();
    }

    @Scheduled( cron = "0 0 1 * * ?", zone = "${cep.engineTimezone}" )  // Every day at 01:00
    public void dailyCleanup()
    {
        pause();
        long startMillis = System.currentTimeMillis();
        System.gc();
        logger.info( "System GC took " + DateUtils.getDurationStr( System.currentTimeMillis() - startMillis ) );
        resume();
    }
}
