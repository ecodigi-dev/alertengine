/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.runlevel;

import com.ecodigi.lang.EcodigiException;

public enum RunLevelType
{
    Halt, Alive, Starting, Running, Stopping;

    public boolean isActive( ServiceRunLevelType serviceRunLevel )
    {
        return( RunLevelType.isActive( this, serviceRunLevel ) );
    }

    public static boolean isActive( RunLevelType currentRunLevel, ServiceRunLevelType serviceRunLevel )
    {
        switch( currentRunLevel )
        {
            case Halt:
                return( ServiceRunLevelType.Fulltime.equals( serviceRunLevel ) );
            case Alive:
            case Starting:
            case Stopping:
                switch( serviceRunLevel )
                {
                    case Fulltime:
                    case Basic:
                        return( true );
                    default:
                        return( false );
                }
            case Running:
                return( true );
            default:
                throw new EcodigiException( "Invalid current run level '" + String.valueOf( currentRunLevel ) + "'" );
        }
    }
}
