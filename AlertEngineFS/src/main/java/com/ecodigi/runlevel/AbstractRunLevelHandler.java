/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.runlevel;

import com.ecodigi.domain.utils.EventSeverity;
import com.ecodigi.service.NotificationPublisher;
import com.ecodigi.util.LoggableContextAwareBean;
import com.ecodigi.util.DateUtils;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractRunLevelHandler extends LoggableContextAwareBean implements RunLevelHandler, RunLevelTransitionHandler
{
    private final AtomicReference<RunLevelType> currentRunLevel = new AtomicReference<>();

    protected abstract String getServiceName();
    protected abstract TimeZone getEngineTimeZone();
    protected abstract String getVersionStr();
    protected abstract String getRevision();

    @Autowired
    private NotificationPublisher notificationPublisher;

    private Date    upTime   = null;
    private Date    downTime = null;

    public AbstractRunLevelHandler()
    {
        currentRunLevel.set( RunLevelType.Halt );
    }

    @Override
    public RunLevelType getRunLevel()
    {
        return( currentRunLevel.get() );
    }

    @Override
    public Date getUpTime()
    {
        return upTime;
    }

    @PostConstruct
    public void init()
    {
        TimeZone.setDefault( TimeZone.getTimeZone( "GMT" ) );
        logger.debug( "JVM TimeZone initialized to: " + TimeZone.getDefault().getID() );
        logger.info( String.format( "%s rev %s initialization", getVersionStr(), getRevision() ) );

        try
        {
            onInitialization();
            currentRunLevel.set( RunLevelType.Alive );
            logger.debug( "RunLevel initialized to Alive" );
        }
        catch( Throwable e )
        {
            logger.error( "Unable to initialize RunLevelState", e );
        }
    }

    @PreDestroy
    public void close()
    {
        if ( RunLevelType.Running.equals( currentRunLevel.get() ) )
            stop();

        notificationPublisher.sendNotification( EventSeverity.Info, String.format( "%s rev %s stopped", getVersionStr(), getRevision() ) );
        try
        {
            onShutdown();
            currentRunLevel.set( RunLevelType.Halt );
            logger.debug( "RunLevel finalized and set to Halt" );
        }
        catch( Throwable e )
        {
            logger.error( "Unable to finalize RunLevelState", e );
        }
    }

    @Override
    public void start()
    {
        RunLevelType previousRunLevel = currentRunLevel.get();
        try
        {
            logger.debug( "RunLevel set to Starting" );
            currentRunLevel.set( RunLevelType.Starting );
            if ( onStartProcessing() )
            {
                currentRunLevel.set( RunLevelType.Running );
                logger.debug( "RunLevel set to Running" );
                upTime = new Date();
                downTime = null;
                notificationPublisher.sendNotification( EventSeverity.Info, String.format( "%s rev %s started at %s", getVersionStr(), getRevision(), DateUtils.getDateTimeZoneString( upTime, getEngineTimeZone() ) ) );
            }
            else
            {
                currentRunLevel.set( previousRunLevel );
                logger.info( "RunLevel set back to " + previousRunLevel.name() );
            }
        }
        catch( Throwable e )
        {
            logger.error( "Unable to set RunLevel to Running", e );
            currentRunLevel.set( previousRunLevel );
            logger.info( "RunLevel set back to " + previousRunLevel.name() );
        }
    }

    @Override
    public void stop()
    {
        RunLevelType previousRunLevel = currentRunLevel.get();
        try
        {
            downTime = new Date();
            notificationPublisher.sendNotification(EventSeverity.Info, getServiceName() + " requested to stop at " + DateUtils.getDateTimeZoneString( downTime, getEngineTimeZone() ) );
            logger.debug( "RunLevel set to Stopping" );
            currentRunLevel.set( RunLevelType.Stopping );
            onStopProcessing();
            currentRunLevel.set( RunLevelType.Alive );
            logger.debug( "RunLevel set to Alive" );
            if ( upTime != null )
            {
                long elapsed = System.currentTimeMillis() - upTime.getTime();
                logger.info( String.format( "%s running time was %s", getServiceName(), DateUtils.getDurationStr( elapsed ) ) );
            }
            upTime   = null;
            downTime = null;
        }
        catch( Throwable e )
        {
            logger.error( "Unable to set RunLevel to Alive", e );
            currentRunLevel.set( previousRunLevel );
            logger.info( "RunLevel set back to " + previousRunLevel.name() );
        }
    }

    @Override
    public void resume()
    {
        RunLevelType previousRunLevel = currentRunLevel.get();
        try
        {
            logger.info( "Will resume processing" );
            logger.debug( "RunLevel set to Starting" );
            currentRunLevel.set( RunLevelType.Starting );
            if ( onResumeProcessing() )
            {
                currentRunLevel.set( RunLevelType.Running );
                logger.debug( "RunLevel set to Running" );
            }
            else
            {
                currentRunLevel.set( previousRunLevel );
                logger.info( "RunLevel set back to " + previousRunLevel.name() );
            }
        }
        catch( Throwable e )
        {
            logger.error( "Unable to set RunLevel to Running", e );
            currentRunLevel.set( previousRunLevel );
            logger.info( "RunLevel set back to " + previousRunLevel.name() );
        }
    }

    @Override
    public void pause()
    {
        RunLevelType previousRunLevel = currentRunLevel.get();
        try
        {
            logger.info( "Will pause processing" );
            logger.debug( "RunLevel set to Stopping" );
            currentRunLevel.set( RunLevelType.Stopping );
            onPauseProcessing();
            currentRunLevel.set( RunLevelType.Alive );
            logger.debug( "RunLevel set to Alive" );
        }
        catch( Throwable e )
        {
            logger.error( "Unable to set RunLevel to Alive", e );
            currentRunLevel.set( previousRunLevel );
            logger.info( "RunLevel set back to " + previousRunLevel.name() );
        }
    }

    @Override
    public boolean onResumeProcessing()
    {
        return( onStartProcessing() );
    }

    @Override
    public boolean onPauseProcessing()
    {
        return( onStopProcessing() );
    }
}
