/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.runlevel;

import com.ecodigi.service.TimeService;
import com.ecodigi.util.DateUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

public abstract class AbstractManagedRunLevelHandler extends AbstractRunLevelHandler
{
    private static final long HEALTH_INFO_PROCESS_INTERVAL = 500L;
    private static final long HEALTH_INFO_PROCESS_TIMEOUT  = HEALTH_INFO_PROCESS_INTERVAL - 100L;

    private static final long ACTION_SHORT_INTERVAL_MILLIS    = 1000L;
    private static final long RECURRING_ACTION_TIMEOUT_MILLIS = 30 * 1000L;

    protected static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private final Map<String,RecurringActionMetadata> recurringActionsMap = new TreeMap<String, RecurringActionMetadata>();
    private final ReentrantLock recurringActionsMapLock = new ReentrantLock();

    private final BlockingQueue<RecurringActionHealthInfo> healthInfoQueue = new LinkedBlockingQueue<RecurringActionHealthInfo>();

    @Autowired
    private TimeService timeService;

    @Override
    public boolean handleServiceState( String actionName )
    {
        return( handleServiceState( actionName, ServiceRunLevelType.Basic, ACTION_SHORT_INTERVAL_MILLIS ) );
    }

    @Override
    public boolean handleServiceState( String actionName, ServiceRunLevelType serviceRunLevelType, Long actionIntervalMillis )
    {
        boolean active = getRunLevel().isActive( serviceRunLevelType );
        long actionTimeoutMillis = actionIntervalMillis + RECURRING_ACTION_TIMEOUT_MILLIS;
        healthInfoQueue.offer( new RecurringActionHealthInfo( actionName, timeService.getMillis(), actionTimeoutMillis, active ) );
        return( active );
    }

    private void updateHealthInfo( RecurringActionHealthInfo healthInfo )
    {
        recurringActionsMapLock.lock();
        try
        {
            RecurringActionMetadata actionMetadata = recurringActionsMap.get( healthInfo.getActionName() );
            if ( actionMetadata == null )
            {
                actionMetadata = new RecurringActionMetadata( healthInfo );
                recurringActionsMap.put( healthInfo.getActionName(), actionMetadata );
                logger.info( String.format( "Recurring action [%d] %s registered", actionMetadata.getOrdinal(), healthInfo.getActionName() ) );
            }
            else
            {
                actionMetadata.setTimestamp( healthInfo.getTimestamp() );
                actionMetadata.setActive( healthInfo.isActive() );
                actionMetadata.incCount();
            }
        }
        finally
        {
            recurringActionsMapLock.unlock();
        }
    }

    @Scheduled( fixedDelay = HEALTH_INFO_PROCESS_INTERVAL )
    public void processHealthInfo()
    {
        if ( !handleServiceState( getServiceName() + " Health Check", ServiceRunLevelType.Fulltime, HEALTH_INFO_PROCESS_INTERVAL ) )
            return;

        long startMillis = System.currentTimeMillis();
        do
        {
            RecurringActionHealthInfo healthInfo = healthInfoQueue.poll();
            if ( healthInfo != null )
            {
                updateHealthInfo( healthInfo );
            }
            else
                break;
        } while( ( System.currentTimeMillis() - startMillis ) < HEALTH_INFO_PROCESS_TIMEOUT );
    }

    public List<RecurringActionMetadata> getRecurringActions( boolean notHealthyOnly )
    {
        long now = timeService.getMillis();
        List<RecurringActionMetadata> actionMetadataList = new ArrayList<RecurringActionMetadata>();

        recurringActionsMapLock.lock();
        try
        {        
            for( RecurringActionMetadata actionMetadata: recurringActionsMap.values() )
            {
                if ( !actionMetadata.isHealthy( now ) || !notHealthyOnly )
                    actionMetadataList.add( actionMetadata );
            }
        }
        finally
        {
            recurringActionsMapLock.unlock();
        }

        return( actionMetadataList );
    }

    public boolean isServiceHealthy()
    {
        long now = timeService.getMillis();
        recurringActionsMapLock.lock();
        try
        {        
            for( RecurringActionMetadata actionMetadata: recurringActionsMap.values() )
            {
                if ( !actionMetadata.isHealthy( now ) )
                    return( false );
            }
        }
        finally
        {
            recurringActionsMapLock.unlock();
        }
        return( true );
    }

    public void reportNotHealthyActions( TimeZone userTimeZone )
    {
        DateFormat userDateFormat = new SimpleDateFormat( DateUtils.ISO_HR_DATETIME_FORMAT_STR );
        userDateFormat.setTimeZone( userTimeZone );
        List<RecurringActionMetadata> actionMetadataList = getRecurringActions( true );
        long now = timeService.getMillis();
        for( RecurringActionMetadata actionMetadata: actionMetadataList )
            logger.error( actionMetadata.getActionName() + ": " + actionMetadata.getStateString( userDateFormat, now ) );
    }
}
