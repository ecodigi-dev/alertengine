/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.runlevel;

public enum ServiceRunLevelType
{
    Fulltime, Basic, Application;

    public static final long DISABLED_INITIAL_WAIT                  = -1L;
    public static final long DEFAULT_BASIC_LEVEL_INITIAL_WAIT       = 5000L;
    public static final long DEFAULT_APPLICATION_LEVEL_INITIAL_WAIT = 10000L;

//    public static long getDefaultInitialWait( ServiceRunLevelType runLevel )
//    {
//        switch( runLevel )
//        {
//            case Fulltime:
//                return( DISABLED_INITIAL_WAIT );
//            case Basic:
//                return( DEFAULT_BASIC_LEVEL_INITIAL_WAIT );
//            case Application:
//                return( DEFAULT_APPLICATION_LEVEL_INITIAL_WAIT );
//            default:
//                return( DISABLED_INITIAL_WAIT );
//        }
//    }
}
