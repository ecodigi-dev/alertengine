/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.runlevel;

import com.ecodigi.util.DateUtils;
import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class RecurringActionMetadata
{
    private final String actionName;
    private final int    ordinal;
    private long         timestamp;
    private long         actionTimeout;
    private boolean      active;
    private long         count;

    private static final AtomicInteger ordinalFactory = new AtomicInteger( 0 );

    public RecurringActionMetadata( RecurringActionHealthInfo healthInfo )
    {
        this( healthInfo.getActionName(), healthInfo.getTimestamp(), healthInfo.getActionTimeout(), healthInfo.isActive() );
    }

    public RecurringActionMetadata( String actionName, long timestamp, long actionTimeout, boolean active )
    {
        this.actionName    = actionName;
        this.ordinal       = ordinalFactory.incrementAndGet();
        this.timestamp     = timestamp;
        this.actionTimeout = actionTimeout;
        this.active        = active;
        this.count         = 1;
    }

    public String getActionName()
    {
        return actionName;
    }

    public int getOrdinal()
    {
        return ordinal;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public void setTimestamp( long timestamp )
    {
        this.timestamp = timestamp;
    }

    public long getActionTimeout()
    {
        return actionTimeout;
    }

    public void setActionTimeout( long actionTimeout )
    {
        this.actionTimeout = actionTimeout;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive( boolean active )
    {
        this.active = active;
    }

    public boolean isHealthy( long nowMillis )
    {
        long actionAge = nowMillis - timestamp;
        return( actionAge < actionTimeout );
    }

    public long getCount()
    {
        return count;
    }

    public void setCount( long count )
    {
        this.count = count;
    }

    public void incCount()
    {
        count++;
    }

    public String getStateString( DateFormat dateFormat, long nowMillis )
    {
        StringBuilder state = new StringBuilder();
        if ( active )
            state.append( "Active " );
        else
            state.append( "Idle   " );

        state.append( dateFormat.format( new Date( timestamp ) ) );
        long actionAge = nowMillis - timestamp;
        if ( actionAge > actionTimeout )
            state.append( " Delayed " ).append(DateUtils.getDurationStr( actionAge ) ).append( ' ' );
        state.append( " Ord " ).append( ordinal ).append( ' ' );
        state.append( " [" ).append( count ).append( "] " );

        return( state.toString() );
    }
}
