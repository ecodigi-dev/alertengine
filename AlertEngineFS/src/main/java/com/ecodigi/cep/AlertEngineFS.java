/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.cep;

import com.ecodigi.config.AlertEngineConfig;
import com.ecodigi.runlevel.AlertEngineRunLevel;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.TimeZone;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public class AlertEngineFS
{
    private static final Log logger = LogFactory.getLog( AlertEngineFS.class );
    private static volatile boolean running = false;
    private static final String JVM_TIMEZONE = "GMT";

    private static void usage( String[] args )
    {
        show_header_announcement();
        if ( args.length > 0 )
            System.out.println( "Invalid arguments: " + StringUtils.join( args, " " )  + "\n\n" );
        System.out.println( "\nUsage:\n\n    java -jar [-Dconfig.path=/path/alertEngine.properties] AlertEngineFS.jar [-P pid-filename]\n" );
    }

    private static void show_warranty_announcement()
    {
        System.out.println( "\n" + AlertEngineRunLevel.ALERT_ENGINE_VERSION_STR + " - Copyright (C) 2022 Ecodigi Tecnologia e Servi�os Ltda.\n" );
        System.out.println("BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE PROGRAM,\n"
                + " TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING\n"
                + " THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM \"AS IS\" WITHOUT WARRANTY\n"
                + " OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,\n"
                + " THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.\n"
                + " THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.\n"
                + " SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,\n"
                + " REPAIR OR CORRECTION.\n");
        System.out.println( "IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT\n"
                + " HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE,\n"
                + " BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES\n"
                + " ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA\n"
                + " OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE\n"
                + " PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF\n"
                + " THE POSSIBILITY OF SUCH DAMAGES.");
        System.exit(0);
    }

    private static void show_license_announcement() throws IOException
    {
        BufferedReader br = new BufferedReader( new FileReader( new ClassPathResource( "com/ecodigi/config/gpl-2.0.txt" ).getFile() ) );
        String line;
        System.out.println( "\n" + AlertEngineRunLevel.ALERT_ENGINE_VERSION_STR + " - Copyright (C) 2022 Ecodigi Tecnologia e Servi�os Ltda.\n" );
        while ((line = br.readLine()) != null)
            System.out.println( line );
        System.exit(0);
    }

    private static void show_header_announcement()
    {
        System.out.println( "\n" + AlertEngineRunLevel.ALERT_ENGINE_VERSION_STR + " - Copyright (C) 2022 Ecodigi Tecnologia e Servi�os Ltda." );
        System.out.println( "AlertEngineFS comes with ABSOLUTELY NO WARRANTY; for details type 'java -jar AlertEngineFS.jar -show warranty'" );
        System.out.println( "This is free software, and you are welcome to redistribute it under certain conditions; type 'java -jar AlertEngineFS.jar -show license' for details.\n" );
    }

    private static void writePIDFile( String filename ) throws IOException
    {
        String jvmName = ManagementFactory.getRuntimeMXBean().getName();
        int index = jvmName.indexOf( '@' );
        if ( index > 0 )
        {
            String pid = jvmName.substring( 0, index );
            FileWriter fileWriter = new FileWriter( filename );
            fileWriter.write( pid );
            fileWriter.close();
        }
    }

    private static void deletePIDFile( String filename )
    {
        File file = new File( filename );
        if ( file.exists() )
            file.delete();
    }

    private static void waitForKeypress()
    {
        try
        {
            while ( true )
            {
                while ( System.in.available() == 0 )
                {
                    Thread.sleep( 100 );
                }

                int key = System.in.read();
                while ( System.in.available() > 0 )
                    System.in.read();

                if ( ( key == 'q' ) || ( key == 'Q' ) )
                    break;
            }
        }
        catch ( IOException e )
        {
            System.out.println( e );
        }
        catch( InterruptedException e )
        {
            System.out.println( "Sleep interrupted" );
        }
    }

    private static void initializeTimeZone()
    {
        TimeZone.setDefault( TimeZone.getTimeZone( JVM_TIMEZONE ) );
        logger.debug( "JVM TimeZone initialized to: " + TimeZone.getDefault().getID() );
    }

    public static void startProcess( final AlertEngineRunLevel runLevel, boolean daemonize, final String filename )
    {
        if ( daemonize )
        {
            running = true;
            try
            {
                AlertEngineFS.writePIDFile( filename );
            }
            catch( IOException e )
            {
                logger.error( "Unable to write PID file", e );
            }
            logger.info( "Send kill signal to stop Alert Engine" );
            Runtime.getRuntime().addShutdownHook( new Thread() {
                @Override
                public void run()
                {
                    logger.info( "Shutdown hook invoked. Stopping Alert Engine..." );
                    runLevel.stop();
                    
                    AlertEngineFS.deletePIDFile( filename );
                    running = false;
                }
            } );
            while ( running )
            {
                try {
                    Thread.sleep( 100 );
                }
                catch( InterruptedException e ) {
                }
            }
            logger.debug( "Probe daemon stop requested" );
        }
        else
        {
            logger.info( "Press q to quit" );
            AlertEngineFS.waitForKeypress();
            runLevel.stop();
        }
    }

    public static void dispatch( boolean daemonize, final String filename )
    {
        show_header_announcement();
        initializeTimeZone();

        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext( AlertEngineConfig.class );
        AlertEngineRunLevel runLevel = applicationContext.getBean( AlertEngineRunLevel.class );
        ThreadPoolTaskScheduler threadPoolTaskScheduler = applicationContext.getBean( ThreadPoolTaskScheduler.class );
        runLevel.start();
        startProcess( runLevel, daemonize, filename );
        threadPoolTaskScheduler.shutdown();

        logger.info( "Will close application context" );
        applicationContext.close();
    }

    public static void main( String[] args ) throws IOException
    {
        if ( args.length == 0  )
            dispatch( false, null );
        else if ( ( args.length == 2 ) && ( "-P".equalsIgnoreCase( args[ 0 ] ) ) )
            dispatch( true, args[ 1 ] );
        else if ( ( args.length == 2 ) && ( "-show".equalsIgnoreCase( args[ 0 ] ) ) )
            if ( "warranty".equalsIgnoreCase( args[ 1 ] ) )
                show_warranty_announcement();
            else if ( "license".equalsIgnoreCase( args[ 1 ] ) )
                show_license_announcement();
        else
            usage( args );
    }
}
