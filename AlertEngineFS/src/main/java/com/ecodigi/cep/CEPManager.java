/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.cep;

import com.ecodigi.domain.cep.AbstractStatementBean;
import com.ecodigi.domain.cep.AlertConfiguration;
import com.ecodigi.domain.cep.CEPServiceRequest;
import com.ecodigi.domain.cep.CEPServiceRequestCompleted;
import com.ecodigi.domain.cep.EcodigiAlert;
import com.ecodigi.domain.cep.EcodigiEPLFeed;
import com.ecodigi.domain.cep.EcodigiSourceFeed;
import com.ecodigi.domain.cep.EventSourceConfiguration;
import com.ecodigi.esper.EsperEngine;
import com.ecodigi.lang.EcodigiException;
import com.ecodigi.service.EventOutputPublisher;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EPStatementException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CEPManager
{
    private static final long CEP_SERVICE_REQUEST_TIMEOUT_MILLIS = 5000L;
    
    protected Log logger = LogFactory.getLog( this.getClass() );

    @Autowired
    private EsperEngine esperEngine;

    @Autowired
    private EventOutputPublisher outputPublisher;

    private final ReentrantLock managerLock = new ReentrantLock();

    private final Map<Long, EcodigiSourceFeed> feedMap    = new HashMap<>();
    private final Map<Long, EcodigiAlert>      alertMap   = new HashMap<>();
    private final Map<String, EcodigiEPLFeed>  eplFeedMap = new HashMap<>();
    
    public void close()
    {
        managerLock.lock();
        try
        {
            removeAllAlerts();
            removeAllSourceFeeds();
            removeAllEPLOutputFeeds();
        }
        finally
        {
            managerLock.unlock();
        }
    }

    public void acquire()
    {
        managerLock.lock();
    }

    public void release()
    {
        managerLock.unlock();
    }

    public int getActiveAlertCount()
    {
        managerLock.lock();
        try
        {
            return( alertMap.size() );
        }
        finally
        {
            managerLock.unlock();
        }
    }

    public boolean addAlert( AlertConfiguration alertConfig )
    {
        assert managerLock.isHeldByCurrentThread();

        if ( alertConfig.getId() == null )
            throw new EcodigiException( "AlertConfig must be persisted before started" );
        
        EcodigiAlert alert = alertMap.remove( alertConfig.getId() );
        if ( alert != null )
        {
            destroyStatementBean( alert );
            logger.info( String.format( "Dropped alert[%d] '%s'", alert.getAlertConfig().getId(), alert.getAlertConfig().getExpression() ) );
        }

        if ( !alertConfig.isEnabled() )
            return( true );

        alert = new EcodigiAlert( outputPublisher, alertConfig );
        try
        {
            alert.setEpStatement( createEPStatement( alertConfig.getExpression() ) );
            alert.setListener();
            alertMap.put( alertConfig.getId(), alert );
            logger.info( String.format( "Added alert[%d] '%s'", alertConfig.getId(), alertConfig.getExpression() ) );
        }
        catch( EPStatementException | EcodigiException e )
        {
            throw e;
        }
        catch( Throwable e )
        {
            logger.error( String.format( "Unable to create EP Stament '%s'", alertConfig.getExpression() ), e );
        }
        return( true );
    }

    public boolean removeAlert( Long alertConfigId )
    {
        assert managerLock.isHeldByCurrentThread();

        if ( alertConfigId == null )
            return( false );
        
        EcodigiAlert alert = alertMap.remove( alertConfigId );
        if ( alert == null )
        {
            logger.error( String.format( "Unable to drop alert id %d: not found", alertConfigId ) );
            return( false );
        }

        destroyStatementBean( alert );
        logger.info( String.format( "Dropped alert[%d] '%s'", alertConfigId, alert.getAlertConfig().getExpression() ) );
        return( true );
    }
    
    private void removeAllAlerts()
    {
        assert managerLock.isHeldByCurrentThread();

        Iterator<EcodigiAlert> i = alertMap.values().iterator();
        while ( i.hasNext() )
        {
            EcodigiAlert alert = i.next();
            try
            {
                destroyStatementBean( alert );
                logger.info( String.format( "Dropped alert[%d] '%s'", alert.getAlertConfig().getId(), alert.getAlertConfig().getExpression() ) );
            }
            catch( Throwable e )
            {
                logger.error( "Unable to remove alert " + alert.getAlertConfig().getName(), e );
            }
            i.remove();
        }
    }

    public int getActiveSourceFeedCount()
    {
        managerLock.lock();
        try
        {
            return( feedMap.size() );
        }
        finally
        {
            managerLock.unlock();
        }
    }

    public boolean addSourceFeed( EventSourceConfiguration eventSourceConfig )
    {
        assert managerLock.isHeldByCurrentThread();

        if ( eventSourceConfig.getId() == null )
            throw new EcodigiException( "EventSourceConfig must be persisted before started" );
        
        EcodigiSourceFeed feed = feedMap.remove( eventSourceConfig.getId() );
        if ( feed != null )
        {
            destroyStatementBean( feed );
            logger.info( String.format( "Dropped event source[%d] '%s'", feed.getEventFeedConfig().getId(), feed.getEventFeedConfig().getExpression() ) );
        }
        
        if ( !eventSourceConfig.isEnabled() )
            return( true );

        feed = new EcodigiSourceFeed( eventSourceConfig );
        try
        {
            feed.setEpStatement( createEPStatement( eventSourceConfig.getExpression() ) );
            feedMap.put( eventSourceConfig.getId(), feed );
            logger.info( String.format( "Added event source[%d] '%s'", eventSourceConfig.getId(), eventSourceConfig.getExpression() ) );
        }
        catch( EPStatementException | EcodigiException e )
        {
            throw e;
        }
        catch( Throwable e )
        {
            logger.error( String.format( "Unable to create EP Stament '%s'", eventSourceConfig.getExpression() ), e );
            return( false );
        }
        return( true );
    }
    
    public boolean removeSourceFeed( Long eventFeedConfigId )
    {
        assert managerLock.isHeldByCurrentThread();

        if ( eventFeedConfigId == null )
            return( false );

        EcodigiSourceFeed feed = feedMap.remove( eventFeedConfigId );
        if ( feed == null )
        {
            logger.error( String.format( "Unable to drop event source id %d: not found", eventFeedConfigId ) );
            return( false );
        }

        destroyStatementBean( feed );
        logger.info( String.format( "Dropped event source[%d] '%s'", eventFeedConfigId, feed.getEventFeedConfig().getExpression() ) );
        return( true );
    }

    private void removeAllSourceFeeds()
    {
        assert managerLock.isHeldByCurrentThread();

        Iterator<EcodigiSourceFeed> i = feedMap.values().iterator();
        while ( i.hasNext() )
        {
            EcodigiSourceFeed feed = i.next();
            try
            {
                destroyStatementBean( feed );
                logger.info( String.format( "Dropped event source[%d] '%s'", feed.getEventFeedConfig().getId(), feed.getEventFeedConfig().getExpression() ) );
            }
            catch( Throwable e )
            {
                logger.error( "Unable to remove source feed " + feed.getEventFeedConfig().getName(), e );
            }
            i.remove();
        }
    }

    public int getActiveEPLOutputFeedCount()
    {
        managerLock.lock();
        try
        {
            return( eplFeedMap.size() );
        }
        finally
        {
            managerLock.unlock();
        }
    }

    public boolean addEPLOutputFeed( String name, String expression )
    {
        assert managerLock.isHeldByCurrentThread();

        if ( StringUtils.isBlank( expression ) )
            return( false );

        if ( StringUtils.isBlank( name ) )
            throw new EcodigiException( "EPLOutputFeed need a valid non-empty name" );

        EcodigiEPLFeed feed = eplFeedMap.remove( name );
        if ( feed != null )
        {
            destroyStatementBean( feed );
            logger.info( String.format( "Dropped EPL Feed[%s]", feed.getName() ) );
        }

        feed = new EcodigiEPLFeed( outputPublisher, name );
        try
        {
            feed.setEpStatement( createEPStatement( expression ) );
            feed.setListener();
            eplFeedMap.put( name, feed );
            logger.info( String.format( "Added EPL Feed[%s] '%s'", name, expression ) );
        }
        catch( EPStatementException | EcodigiException e )
        {
            throw e;
        }
        catch( Throwable e )
        {
            logger.error( String.format( "Unable to create EP Stament '%s' for EPLOutputFeed '%s'", expression, name ), e );
            return( false );
        }
        return( true );
    }

    public boolean removeEPLOutputFeed( String name )
    {
        assert managerLock.isHeldByCurrentThread();

        if ( StringUtils.isBlank( name ) )
            return( false );

        logger.debug( "Will remove feed " + name );
        EcodigiEPLFeed feed = eplFeedMap.remove( name );
        if ( feed == null )
        {
            logger.info( String.format( "Unable to drop EPL Feed[%s]", name ) );
            return( false );
        }

        logger.debug( "Will destroy feed " + name );
        destroyStatementBean( feed );
        logger.info( String.format( "Dropped EPL Feed[%s]", name ) );
        return( true );
    }

    private void removeAllEPLOutputFeeds()
    {
        assert managerLock.isHeldByCurrentThread();

        Iterator<EcodigiEPLFeed> i = eplFeedMap.values().iterator();
        while ( i.hasNext() )
        {
            EcodigiEPLFeed feed = i.next();
            try
            {
                destroyStatementBean( feed );
                logger.info( String.format( "Dropped EPL Feed[%s]", feed.getName() ) );
            }
            catch( Throwable e )
            {
                logger.error( "Unable to remove EPLOutput feed " + feed.getEpStatement().getText(), e );
            }
            i.remove();
        }
    }

    public boolean validateExpression( String expression )
    {
        EPStatement statement = createEPStatement( expression );
        statement.destroy();
        logger.info( String.format( "Expression '%s' validated" , expression ) );
        return( true );
    }

    private EPStatement createEPStatement( String epl )
    {
//        logger.info( "Will execute service request " + epl );
        CEPServiceRequest serviceRequest = executeCEPServiceRequest( new CEPServiceRequest( epl ) );
        return( serviceRequest.getStatement() );
    }

    private void destroyStatementBean( AbstractStatementBean statementBean )
    {
        executeCEPServiceRequest( new CEPServiceRequest( statementBean ) );
    }

    private CEPServiceRequest executeCEPServiceRequest( CEPServiceRequest serviceRequest )
    {
        final AtomicBoolean done = new AtomicBoolean( false );
        serviceRequest.setCompletionHandler( new CEPServiceRequestCompleted()
        {
            @Override
            public void completed( CEPServiceRequest request )
            {
                done.set( true );
            }
        } );

        // Request service and wait for completion to force this call to be synchronous
        esperEngine.requestService( serviceRequest );
        long startMillis = System.currentTimeMillis();
        while ( !done.get() )
        {
            long elapsed = System.currentTimeMillis() - startMillis;
            if ( elapsed > CEP_SERVICE_REQUEST_TIMEOUT_MILLIS )
                throw new EcodigiException( "Timeout in attempt to execute CEP Service: " + serviceRequest.toString() );
            try
            {
                Thread.sleep( 1 );
            }
            catch ( InterruptedException ex )
            {
            }
        }

        boolean success = ( serviceRequest.getException() == null );
        if ( logger.isDebugEnabled() )
        {
            long elapsed = System.currentTimeMillis() - startMillis;
            logger.debug( "CEP Service request [" + serviceRequest.toString() + "] executed " + ( success ? "successfully" : "with error" ) + " in " + elapsed + " ms" );
        }

        if ( !success )
            throw new EcodigiException( String.format( "Unable to execute CEP Service request '%s'", serviceRequest.toString() ), serviceRequest.getException() );

        return( serviceRequest );
    }
}
