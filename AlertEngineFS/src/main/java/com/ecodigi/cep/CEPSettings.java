/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.cep;

import com.ecodigi.util.LoggableContextAwareBean;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.TimeZone;
import jakarta.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CEPSettings extends LoggableContextAwareBean
{
    public static final String TEMP_PATH = "temp";

    @Value( "${cep.enabled}" )
    private Boolean cepEnabled;

    @Value( "${cep.eventsPath}" )
    private String eventsPath;

    @Value( "${cep.eventsSchema}" )
    private String eventsSchema;

    @Value( "${cep.outputPath}" )
    private String outputPath;

    @Value( "${cep.providerName}" )
    private String providerName;

    @Value( "${cep.eventsPackageName}" )
    private String eventsPackageName;

    @Value( "${cep.enableExecutionDebug}" )
    private Boolean enableExecutionDebug;

    @Value( "${cep.enableEventValidation}" )
    private Boolean enableEventValidation;

    @Value( "${cep.eventsHistorySize}" )
    private Integer eventsHistorySize;

    private boolean enabled = false;
    private String tempOutputPath = null;

    private TimeZone cepEngineTimeZone;

    public CEPSettings()
    {
    }

    @PostConstruct
    public void initialize()
    {
        enabled = ( cepEnabled != null ) && cepEnabled;
        if ( enabled )
        {
            Path tempOutPath = Paths.get( outputPath, TEMP_PATH );
            tempOutputPath = tempOutPath.toString();
            try
            {
                Files.createDirectories( tempOutPath );
            }
            catch( Throwable e )
            {
                logger.error( String.format( "Unable to create temporary directory '%s' for output", tempOutputPath ), e );
            }
        }
    }

    @Value( "${cep.engineTimezone}" )
    public void setCEPEngineTimeZoneStr( String cepEngineTimeZone )
    {
        if ( StringUtils.isBlank( cepEngineTimeZone ) )
            this.cepEngineTimeZone = TimeZone.getDefault();
        else
            this.cepEngineTimeZone = TimeZone.getTimeZone( cepEngineTimeZone );
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public String getEventsPath()
    {
        return eventsPath;
    }

    public String getEventsSchema()
    {
        return eventsSchema;
    }

    public String getOutputPath()
    {
        return outputPath;
    }

    public String getTempOutputPath()
    {
        return tempOutputPath;
    }

    public String getProviderName()
    {
        return providerName;
    }

    public String getEventsPackageName()
    {
        return eventsPackageName;
    }

    public boolean isEnabledExecutionDebug()
    {
        return( ( enableExecutionDebug != null ) && enableExecutionDebug );
    }

    public boolean isEnabledEventValidation()
    {
        return( ( enableEventValidation != null ) && enableEventValidation );
    }

    public int getEventsHistorySize()
    {
        if ( eventsHistorySize == null )
            return( 0 );
        else
            return( eventsHistorySize );
    }

    public TimeZone getCEPEngineTimeZone()
    {
        return( cepEngineTimeZone );
    }
}
