/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.cep.accessor;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AlertState
{
    private static final Log logger = LogFactory.getLog( AlertState.class );

    private static final Map<String,Boolean> alertMap = new HashMap<String, Boolean>();
    
    private static String getAlertKey( String alertId, String subId )
    {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append( alertId ).append( '-' ).append( subId );
        return( strBuilder.toString() );
    }
    
    public static boolean markAlertSent( String alertId, String subId )
    {
        if ( logger.isDebugEnabled() )
            logger.debug( "markAlertSent(" + alertId + "," + subId + ")" );
        alertMap.put( getAlertKey( alertId, subId ), Boolean.TRUE );
        return( true );
    }

    public static boolean resetAlertSent( String alertId, String subId )
    {
        if ( logger.isDebugEnabled() )
            logger.debug( "resetAlertSent(" + alertId + "," + subId + ")" );
        alertMap.put( getAlertKey( alertId, subId ), Boolean.FALSE );
        return( true );
    }

    public static boolean wasAlertSent( String alertId, String subId )
    {
        if ( logger.isDebugEnabled() )
            logger.debug( "wasAlertSent(" + alertId + "," + subId + ")" );
        Boolean wasSent = alertMap.get( getAlertKey( alertId, subId ) );
        return( ( wasSent != null ) && wasSent );
    }
}
