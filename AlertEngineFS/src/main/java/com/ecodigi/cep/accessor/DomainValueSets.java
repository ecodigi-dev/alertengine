/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.cep.accessor;

import com.ecodigi.domain.cep.DomainConfiguration;
import com.ecodigi.lang.EcodigiException;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DomainValueSets
{
    private static final Log logger = LogFactory.getLog( DomainValueSets.class );
    
    private static final int MAX_DOMAIN_INDEX = 5;

    private static Map<String,String[]> domain1Map;
    private static final ReentrantReadWriteLock domain1Lock = new ReentrantReadWriteLock();

    private static Map<String,String[]> domain2Map;
    private static final ReentrantReadWriteLock domain2Lock = new ReentrantReadWriteLock();

    private static Map<String,String[]> domain3Map;
    private static final ReentrantReadWriteLock domain3Lock = new ReentrantReadWriteLock();

    private static Map<String,String[]> domain4Map;
    private static final ReentrantReadWriteLock domain4Lock = new ReentrantReadWriteLock();

    private static Map<String,String[]> domain5Map;
    private static final ReentrantReadWriteLock domain5Lock = new ReentrantReadWriteLock();

    private static final String[] emptyArray = new String[] { };
    
    // Max elements + 1, to keep it 1-based
    private static final String[] domainNames = new String[ MAX_DOMAIN_INDEX + 1 ];
    
    private static int nextAvailableDomainIndex = 1;
    
    public static void resetDomainIndex()
    {
        nextAvailableDomainIndex = 1;
    }
    
    public static String assignDomainValues( String domainName, Map<String,String[]> domainMap )
    {
        String methodName = null;
        if ( nextAvailableDomainIndex > MAX_DOMAIN_INDEX )
            throw new EcodigiException( "Max available domain containers reached" );
        
        domainNames[ nextAvailableDomainIndex ] = domainName;
        switch( nextAvailableDomainIndex )
        {
            case 1:
                setDomain1Map( domainMap );
                methodName = "getDomain1Items";
                break;
            case 2:
                setDomain2Map( domainMap );
                methodName = "getDomain2Items";
                break;
            case 3:
                setDomain3Map( domainMap );
                methodName = "getDomain3Items";
                break;
            case 4:
                setDomain4Map( domainMap );
                methodName = "getDomain4Items";
                break;
            case 5:
                setDomain5Map( domainMap );
                methodName = "getDomain5Items";
                break;
        }
        nextAvailableDomainIndex++;

        return( methodName );
    }
    
    public static void updateDomainValues( DomainConfiguration domainConfig )
    {
        int domainIndex = 0;
        for( int index = 1; index < nextAvailableDomainIndex; index++ )
        {
            if ( domainConfig.getName().equals( domainNames[ index ] ) )
                domainIndex = index;
        }

        switch( domainIndex )
        {
            case 1:
                setDomain1Map( domainConfig.getValueMap() );
                break;
            case 2:
                setDomain2Map( domainConfig.getValueMap() );
                break;
            case 3:
                setDomain3Map( domainConfig.getValueMap() );
                break;
            case 4:
                setDomain4Map( domainConfig.getValueMap() );
                break;
            case 5:
                setDomain5Map( domainConfig.getValueMap() );
                break;
        }
    }

    public static void setDomain1Map( Map<String,String[]> domainMap )
    {
        domain1Lock.writeLock().lock();
        try
        {
            DomainValueSets.domain1Map = domainMap;
        }
        finally
        {
            domain1Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,String[]> entry: domainMap.entrySet() )
                logger.debug( String.format( "Domain %s '%s'=%s", domainNames[ 1 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static String[] getDomain1Items( String domainKey )
    {
        if ( StringUtils.isBlank( domainKey ) )
        {
            logger.debug( String.format( "Empty domain %s key provided", domainNames[ 1 ] ) );
            return( emptyArray );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getDomain1Items '" + domainKey + "'" );

        String[] itemValues;
        domain1Lock.readLock().lock();
        try
        {
            itemValues = domain1Map.get( domainKey );
        }
        finally
        {
            domain1Lock.readLock().unlock();
        }

        if ( itemValues == null )
            return( emptyArray );
        else
            return( itemValues );
    }

    public static void setDomain2Map( Map<String,String[]> domainMap )
    {
        domain2Lock.writeLock().lock();
        try
        {
            DomainValueSets.domain2Map = domainMap;
        }
        finally
        {
            domain2Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,String[]> entry: domainMap.entrySet() )
                logger.debug( String.format( "Domain %s '%s'=%s", domainNames[ 2 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static String[] getDomain2Items( String domainKey )
    {
        if ( StringUtils.isBlank( domainKey ) )
        {
            logger.debug( String.format( "Empty domain %s key provided", domainNames[ 2 ] ) );
            return( emptyArray );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getDomain2Items '" + domainKey + "'" );

        String[] itemValues;
        domain2Lock.readLock().lock();
        try
        {
            itemValues = domain2Map.get( domainKey );
        }
        finally
        {
            domain2Lock.readLock().unlock();
        }

        if ( itemValues == null )
            return( emptyArray );
        else
            return( itemValues );
    }

    public static void setDomain3Map( Map<String,String[]> domainMap )
    {
        domain3Lock.writeLock().lock();
        try
        {
            DomainValueSets.domain3Map = domainMap;
        }
        finally
        {
            domain3Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,String[]> entry: domainMap.entrySet() )
                logger.debug( String.format( "Domain %s '%s'=%s", domainNames[ 3 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static String[] getDomain3Items( String domainKey )
    {
        if ( StringUtils.isBlank( domainKey ) )
        {
            logger.debug( String.format( "Empty domain %s key provided", domainNames[ 3 ] ) );
            return( emptyArray );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getDomain3Items '" + domainKey + "'" );

        String[] itemValues;
        domain3Lock.readLock().lock();
        try
        {
            itemValues = domain3Map.get( domainKey );
        }
        finally
        {
            domain3Lock.readLock().unlock();
        }

        if ( itemValues == null )
            return( emptyArray );
        else
            return( itemValues );
    }

    public static void setDomain4Map( Map<String,String[]> domainMap )
    {
        domain4Lock.writeLock().lock();
        try
        {
            DomainValueSets.domain4Map = domainMap;
        }
        finally
        {
            domain4Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,String[]> entry: domainMap.entrySet() )
                logger.debug( String.format( "Domain %s '%s'=%s", domainNames[ 4 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static String[] getDomain4Items( String domainKey )
    {
        if ( StringUtils.isBlank( domainKey ) )
        {
            logger.debug( String.format( "Empty domain %s key provided", domainNames[ 4 ] ) );
            return( emptyArray );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getDomain4Items '" + domainKey + "'" );

        String[] itemValues;
        domain4Lock.readLock().lock();
        try
        {
            itemValues = domain4Map.get( domainKey );
        }
        finally
        {
            domain4Lock.readLock().unlock();
        }

        if ( itemValues == null )
            return( emptyArray );
        else
            return( itemValues );
    }

    public static void setDomain5Map( Map<String,String[]> domainMap )
    {
        domain5Lock.writeLock().lock();
        try
        {
            DomainValueSets.domain5Map = domainMap;
        }
        finally
        {
            domain5Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,String[]> entry: domainMap.entrySet() )
                logger.debug( String.format( "Domain %s '%s'=%s", domainNames[ 5 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static String[] getDomain5Items( String domainKey )
    {
        if ( StringUtils.isBlank( domainKey ) )
        {
            logger.debug( String.format( "Empty domain %s key provided", domainNames[ 5 ] ) );
            return( emptyArray );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getDomain5Items '" + domainKey + "'" );

        String[] itemValues;
        domain5Lock.readLock().lock();
        try
        {
            itemValues = domain5Map.get( domainKey );
        }
        finally
        {
            domain5Lock.readLock().unlock();
        }

        if ( itemValues == null )
            return( emptyArray );
        else
            return( itemValues );
    }
}
