/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.cep.accessor;

import com.ecodigi.domain.cep.NamedValuesConfiguration;
import com.ecodigi.lang.EcodigiException;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NamedValuesSets
{
    private static final Log logger = LogFactory.getLog(NamedValuesSets.class );
    
    private static final int MAX_NAMED_VALUES_INDEX = 5;

    private static Map<String,Long> namedvalues1Map;
    private static final ReentrantReadWriteLock namedvalues1Lock = new ReentrantReadWriteLock();

    private static Map<String,Long> namedvalues2Map;
    private static final ReentrantReadWriteLock namedvalues2Lock = new ReentrantReadWriteLock();

    private static Map<String,Long> namedvalues3Map;
    private static final ReentrantReadWriteLock namedvalues3Lock = new ReentrantReadWriteLock();

    private static Map<String,Long> namedvalues4Map;
    private static final ReentrantReadWriteLock namedvalues4Lock = new ReentrantReadWriteLock();

    private static Map<String,Long> namedvalues5Map;
    private static final ReentrantReadWriteLock namedvalues5Lock = new ReentrantReadWriteLock();

    // Max elements + 1, to keep it 1-based
    private static final String[] namedValuesNames = new String[ MAX_NAMED_VALUES_INDEX + 1 ];

    private static int nextAvailableNamedValuesIndex = 1;
    
    public static void resetNamedValuesIndex()
    {
        nextAvailableNamedValuesIndex = 1;
    }

    public static String assignNamedValuesValues( String namedvaluesName, Map<String,Long> namedValuesMap )
    {
        String methodName = null;
        if ( nextAvailableNamedValuesIndex > MAX_NAMED_VALUES_INDEX )
            throw new EcodigiException( String.format( "Max available namedValues (%d) containers reached: %d", MAX_NAMED_VALUES_INDEX, nextAvailableNamedValuesIndex ) );

        namedValuesNames[ nextAvailableNamedValuesIndex ] = namedvaluesName;
        switch( nextAvailableNamedValuesIndex )
        {
            case 1:
                setNamedValues1Map( namedValuesMap );
                methodName = "getNamedValues1Items";
                break;
            case 2:
                setNamedValues2Map( namedValuesMap );
                methodName = "getNamedValues2Items";
                break;
            case 3:
                setNamedValues3Map( namedValuesMap );
                methodName = "getNamedValues3Items";
                break;
            case 4:
                setNamedValues4Map( namedValuesMap );
                methodName = "getNamedValues4Items";
                break;
            case 5:
                setNamedValues5Map( namedValuesMap );
                methodName = "getNamedValues5Items";
                break;
        }
        nextAvailableNamedValuesIndex++;

        return( methodName );
    }
    
    public static void updateNamedValues( NamedValuesConfiguration namedValuesConfig )
    {
        int namedvaluesIndex = 0;
        for( int index = 1; index < nextAvailableNamedValuesIndex; index++ )
        {
            if ( namedValuesConfig.getName().equals( namedValuesNames[ index ] ) )
                namedvaluesIndex = index;
        }

        switch( namedvaluesIndex )
        {
            case 1:
                setNamedValues1Map( namedValuesConfig.getValueMap() );
                break;
            case 2:
                setNamedValues2Map( namedValuesConfig.getValueMap() );
                break;
            case 3:
                setNamedValues3Map( namedValuesConfig.getValueMap() );
                break;
            case 4:
                setNamedValues4Map( namedValuesConfig.getValueMap() );
                break;
            case 5:
                setNamedValues5Map( namedValuesConfig.getValueMap() );
                break;
        }
    }

    public static void setNamedValues1Map( Map<String,Long> namedvaluesMap )
    {
        namedvalues1Lock.writeLock().lock();
        try
        {
            NamedValuesSets.namedvalues1Map = namedvaluesMap;
        }
        finally
        {
            namedvalues1Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,Long> entry: namedvaluesMap.entrySet() )
                logger.debug( String.format( "NamedValues %s '%s'=%s", namedValuesNames[ 1 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static Long getNamedValues1Items( String namedvaluesKey )
    {
        if ( StringUtils.isBlank( namedvaluesKey ) )
        {
            logger.debug( String.format( "Empty namedvalues %s key provided", namedValuesNames[ 1 ] ) );
            return( null );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getNamedValues1Items '" + namedvaluesKey + "'" );

        Long itemValue;
        namedvalues1Lock.readLock().lock();
        try
        {
            itemValue = namedvalues1Map.get( namedvaluesKey );
        }
        finally
        {
            namedvalues1Lock.readLock().unlock();
        }

        if ( itemValue == null )
            return( null );
        else
            return( itemValue );
    }

    public static void setNamedValues2Map( Map<String,Long> namedvaluesMap )
    {
        namedvalues2Lock.writeLock().lock();
        try
        {
            NamedValuesSets.namedvalues2Map = namedvaluesMap;
        }
        finally
        {
            namedvalues2Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,Long> entry: namedvaluesMap.entrySet() )
                logger.debug( String.format( "NamedValues %s '%s'=%s", namedValuesNames[ 2 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static Long getNamedValues2Items( String namedvaluesKey )
    {
        if ( StringUtils.isBlank( namedvaluesKey ) )
        {
            logger.debug( String.format( "Empty namedvalues %s key provided", namedValuesNames[ 2 ] ) );
            return( null );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getNamedValues2Items '" + namedvaluesKey + "'" );

        Long itemValue;
        namedvalues2Lock.readLock().lock();
        try
        {
            itemValue = namedvalues2Map.get( namedvaluesKey );
        }
        finally
        {
            namedvalues2Lock.readLock().unlock();
        }

        if ( itemValue == null )
            return( null );
        else
            return( itemValue );
    }

    public static void setNamedValues3Map( Map<String,Long> namedvaluesMap )
    {
        namedvalues3Lock.writeLock().lock();
        try
        {
            NamedValuesSets.namedvalues3Map = namedvaluesMap;
        }
        finally
        {
            namedvalues3Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,Long> entry: namedvaluesMap.entrySet() )
                logger.debug( String.format( "NamedValues %s '%s'=%s", namedValuesNames[ 3 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static Long getNamedValues3Items( String namedvaluesKey )
    {
        if ( StringUtils.isBlank( namedvaluesKey ) )
        {
            logger.debug( String.format( "Empty namedvalues %s key provided", namedValuesNames[ 3 ] ) );
            return( null );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getNamedValues3Items '" + namedvaluesKey + "'" );

        Long itemValue;
        namedvalues3Lock.readLock().lock();
        try
        {
            itemValue = namedvalues3Map.get( namedvaluesKey );
        }
        finally
        {
            namedvalues3Lock.readLock().unlock();
        }

        if ( itemValue == null )
            return( null );
        else
            return( itemValue );
    }

    public static void setNamedValues4Map( Map<String,Long> namedvaluesMap )
    {
        namedvalues4Lock.writeLock().lock();
        try
        {
            NamedValuesSets.namedvalues4Map = namedvaluesMap;
        }
        finally
        {
            namedvalues4Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,Long> entry: namedvaluesMap.entrySet() )
                logger.debug( String.format( "NamedValues %s '%s'=%s", namedValuesNames[ 4 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static Long getNamedValues4Items( String namedvaluesKey )
    {
        if ( StringUtils.isBlank( namedvaluesKey ) )
        {
            logger.debug( String.format( "Empty namedvalues %s key provided", namedValuesNames[ 4 ] ) );
            return( null );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getNamedValues4Items '" + namedvaluesKey + "'" );

        Long itemValue;
        namedvalues4Lock.readLock().lock();
        try
        {
            itemValue = namedvalues4Map.get( namedvaluesKey );
        }
        finally
        {
            namedvalues4Lock.readLock().unlock();
        }

        if ( itemValue == null )
            return( null );
        else
            return( itemValue );
    }

    public static void setNamedValues5Map( Map<String,Long> namedvaluesMap )
    {
        namedvalues5Lock.writeLock().lock();
        try
        {
            NamedValuesSets.namedvalues5Map = namedvaluesMap;
        }
        finally
        {
            namedvalues5Lock.writeLock().unlock();
        }

        if ( logger.isDebugEnabled() )
        {
            for( Map.Entry<String,Long> entry: namedvaluesMap.entrySet() )
                logger.debug( String.format( "NamedValues %s '%s'=%s", namedValuesNames[ 5 ], entry.getKey(), StringUtils.join( entry.getValue(), "," ) ) );
        }
    }

    public static Long getNamedValues5Items( String namedvaluesKey )
    {
        if ( StringUtils.isBlank( namedvaluesKey ) )
        {
            logger.debug( String.format( "Empty namedvalues %s key provided", namedValuesNames[ 5 ] ) );
            return( null );
        }

        if ( logger.isDebugEnabled() )
            logger.debug( "getNamedValues5Items '" + namedvaluesKey + "'" );

        Long itemValue;
        namedvalues5Lock.readLock().lock();
        try
        {
            itemValue = namedvalues5Map.get( namedvaluesKey );
        }
        finally
        {
            namedvalues5Lock.readLock().unlock();
        }

        if ( itemValue == null )
            return( null );
        else
            return( itemValue );
    }
}
