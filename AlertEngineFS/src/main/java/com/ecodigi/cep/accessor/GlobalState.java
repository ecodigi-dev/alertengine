/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.cep.accessor;

import com.ecodigi.service.StreamTimeService;
import com.ecodigi.service.TimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GlobalState
{
    private static StreamTimeService streamTimeService;
    private static TimeService timeService;

    @Autowired
    public void setStreamTimeService( StreamTimeService streamTimeService )
    {
        GlobalState.streamTimeService = streamTimeService;
    }

    @Autowired
    public void setTimeService( TimeService timeService )
    {
        GlobalState.timeService = timeService;
    }

    public static Long streamTimestamp()
    {
        return( streamTimeService.getStreamTime() );
    }

    public static long streamBehindMillis()
    {
        return( streamTimeService.getBehindMillis() );
    }

    public static Long systemTimestamp()
    {
        return( timeService.getMicroseconds() );
    }
}
