/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.cep;

import com.ecodigi.domain.AppendableText;
import com.ecodigi.domain.cep.FriendlyEvent;
import com.ecodigi.util.MathUtils;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.event.WrapperEventBean;
import com.espertech.esper.event.bean.BeanEventBean;
import com.espertech.esper.event.map.MapEventBean;
import com.espertech.esper.event.xml.XMLEventBean;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component
public class CEPUnderlyingEventUtils
{
    protected Log logger = LogFactory.getLog( this.getClass() );

    private Set<String> internalEvents = new HashSet<String>();

    public void setInternalEvents( Set<String> internalEvents )
    {
        this.internalEvents = internalEvents;
    }

    private String getEventBeanAsString( Object eventBean )
    {
        if ( eventBean == null )
            return( null );
        else
        {
            if ( eventBean instanceof FriendlyEvent )
                return( ( ( FriendlyEvent )eventBean ).getDisplayStr() );
            else
                return( eventBean.toString() );
        }
    }

    private String getUnderlyingEventContent( BeanEventBean eventBean )
    {
        if ( eventBean == null )
            return( null );

        Object event = eventBean.getUnderlying();
        if ( event == null )
            return( eventBean.toString() );
        else
            return( getEventBeanAsString( event ) );
    }

    private void addEventBeanToContentMap( BeanEventBean eventBean, Map<String,String> contentMap )
    {
        if ( eventBean != null )
        {
            Object event = eventBean.getUnderlying();
            if ( event == null )
                contentMap.put( "Event", eventBean.toString() );
            else
            {
                if ( event instanceof FriendlyEvent )
                    ( ( FriendlyEvent )event ).addAttributes( contentMap );
                else
                    contentMap.put( "Event", getValueAsString( "Event", event ) );
            }
        }
    }

    private String getValueAsString( String field, Object value )
    {
        if ( value instanceof Number )
        {
            Double number = ( ( Number )value ).doubleValue();
            return( MathUtils.getAutoFormatted( number ) );
        }
        return( String.valueOf( value ) );
    }

    private String getUnderlyingEventContent( MapEventBean eventBean )
    {
        if ( eventBean == null )
            return( null );

        AppendableText text = new AppendableText();
        Map<String,Object> evMap = eventBean.getProperties();
        for( String key: evMap.keySet() )
        {
            if ( !internalEvents.contains( key ) )
            {
                Object value = evMap.get( key );
                if ( value instanceof FriendlyEvent )
                {
                    text.clear();
                    text.append( value.toString() );
                    break;
                }
                if ( logger.isDebugEnabled() && ( value != null ) )
                    logger.debug( String.format( "Underlying object %s type %s", key, value.getClass().getName() ) );
                text.append( key + '=' + getValueAsString( key, value ) );
            }
        }
        return( text.toString() );
    }

    private void getUnderlyingEventContentMap( Map<String,String> contentMap, MapEventBean eventBean )
    {
        Map<String,Object> evMap = eventBean.getProperties();
        for( String key: evMap.keySet() )
        {
            Object value = evMap.get( key );
            if ( value instanceof MapEventBean )
                getUnderlyingEventContentMap( contentMap, ( MapEventBean )value );
            else
            {
                if ( value instanceof FriendlyEvent )
                {
                    ( ( FriendlyEvent )value ).addAttributes( contentMap );
                    continue;
                }
                if ( logger.isDebugEnabled() && ( value != null ) )
                    logger.debug( String.format( "Underlying object %s type %s", key, value.getClass().getName() ) );
                contentMap.put( key, getValueAsString( key, value ) );
            }
        }
    }
    
    private Map<String,String> getUnderlyingEventContentMap( MapEventBean eventBean )
    {
        if ( eventBean == null )
            return( null );

        Map<String,String> contentMap = new LinkedHashMap<String, String>();
        getUnderlyingEventContentMap( contentMap, eventBean );
        return( contentMap );
    }

    private String getUnderlyingEventContent( WrapperEventBean eventBean )
    {
        if ( eventBean == null )
            return( null );

        AppendableText text = new AppendableText();
        Map<String,Object> evMap = eventBean.getDecoratingProperties();
        for( String key: evMap.keySet() )
        {
            if ( !internalEvents.contains( key ) )
            {
                Object value = evMap.get( key );
                text.append( key + '=' + getValueAsString( key, value ) );
            }
        }

        EventBean event = eventBean.getUnderlyingEvent();
        if ( ( event != null ) && ( event instanceof BeanEventBean ) )
            text.append( getUnderlyingEventContent( ( BeanEventBean )event ) );

        return( text.toString() );
    }

    private Map<String,String> getUnderlyingEventContentMap( WrapperEventBean eventBean )
    {
        if ( eventBean == null )
            return( null );

        Map<String,String> contentMap = new LinkedHashMap<String, String>();
        Map<String,Object> evMap = eventBean.getDecoratingProperties();
        for( Map.Entry<String,Object> entry: evMap.entrySet() )
        {
            if ( entry.getValue() instanceof FriendlyEvent )
                ( ( FriendlyEvent )entry.getValue() ).addAttributes( contentMap );
            else if ( entry.getValue() instanceof BeanEventBean )
                addEventBeanToContentMap( ( BeanEventBean )entry.getValue(), contentMap );
            else
                contentMap.put( entry.getKey(), getValueAsString( entry.getKey(), entry.getValue() ) );
        }

        EventBean event = eventBean.getUnderlyingEvent();
        if ( ( event != null ) && ( event instanceof BeanEventBean ) )
            addEventBeanToContentMap( ( BeanEventBean )event, contentMap );

        return( contentMap );
    }

    private Map<String,String> getUnderlyingEventContentMap( XMLEventBean eventBean )
    {
        if ( eventBean == null )
            return( null );
        
        Map<String,String> contentMap = new LinkedHashMap<String, String>();
        for( String property: eventBean.getEventType().getPropertyNames() )
        {
            Object value = eventBean.get( property );
            if ( value != null )
                contentMap.put( property, String.valueOf( value ) );
        }
        return( contentMap );
    }

    public String getUnderlyingEventContent( EventBean newEvent )
    {
        if ( newEvent == null )
            return( null );

        if ( newEvent instanceof BeanEventBean )
            return( getUnderlyingEventContent( ( BeanEventBean )newEvent ) );
        else
        {
            if ( newEvent instanceof WrapperEventBean )
                return( getUnderlyingEventContent( ( WrapperEventBean )newEvent ) );
            else
            {
                if ( newEvent instanceof MapEventBean )
                    return( getUnderlyingEventContent( ( MapEventBean )newEvent ) );
                else
                    return( newEvent.toString() );
            }
        }
    }

    public Map<String,String> getUnderlyingEventContentMap( EventBean newEvent )
    {
        if ( newEvent == null )
            return( null );

        if ( newEvent instanceof BeanEventBean )
        {
            Map<String,String> contentMap = new LinkedHashMap<String, String>();
            addEventBeanToContentMap( ( BeanEventBean )newEvent, contentMap );
            return( contentMap );
        }
        else
        {
            if ( newEvent instanceof XMLEventBean )
                return( getUnderlyingEventContentMap( ( XMLEventBean )newEvent ) );
            else
            {
                if ( newEvent instanceof WrapperEventBean )
                    return( getUnderlyingEventContentMap( ( WrapperEventBean )newEvent ) );
                else
                {
                    if ( newEvent instanceof MapEventBean )
                        return( getUnderlyingEventContentMap( ( MapEventBean )newEvent ) );
                    else
                        return( null );
                }
            }
        }
    }

    public Object getUnderlyingEvent( EventBean newEvent )
    {
        if ( newEvent == null )
            return( null );

        if ( newEvent instanceof BeanEventBean )
            return( ( ( BeanEventBean )newEvent ).getUnderlying() );
        else
        {
            if ( newEvent instanceof WrapperEventBean )
            {
                EventBean event = ( ( WrapperEventBean )newEvent ).getUnderlyingEvent();
                if ( event != null )
                    return( event.getUnderlying() );
                else
                    return( newEvent.getUnderlying() );
            }
            else
            {
                if ( newEvent instanceof MapEventBean )
                    return( ( ( MapEventBean )newEvent ).getUnderlying() );
                else
                    return( newEvent.getUnderlying() );
            }
        }
    }
}
