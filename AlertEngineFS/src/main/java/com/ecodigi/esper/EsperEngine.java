/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.esper;

import com.ecodigi.cep.CEPSettings;
import com.ecodigi.cep.accessor.DomainValueSets;
import com.ecodigi.cep.accessor.NamedValuesSets;
import com.ecodigi.domain.AlertEngineConstants;
import com.ecodigi.domain.cep.CEPServiceRequest;
import com.ecodigi.domain.cep.DomainConfiguration;
import com.ecodigi.domain.cep.NamedValuesConfiguration;
import com.ecodigi.lang.EcodigiException;
import com.ecodigi.service.CEPConfigService;
import com.ecodigi.service.EventConsumer;
import com.ecodigi.util.DateUtils;
import com.ecodigi.util.LoggableContextAwareBean;
import com.ecodigi.service.XMLParser;
import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.ConfigurationEventTypeXMLDOM;
import com.espertech.esper.client.ConfigurationOperations;
import com.espertech.esper.client.EPRuntime;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventSender;
import com.espertech.esper.client.EventType;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Component
public class EsperEngine extends LoggableContextAwareBean implements Runnable
{
    private static final long ESPER_SERVICING_INTERVAL_MILLIS  = 100L;
    private static final long ESPER_SERVICING_TIMEOUT_MILLIS   = 60L * 1000L;
    private static final long ESPER_SHUTDOWN_TIMEOUT_MILLIS    = 5L * 1000L;

    @Autowired
    private CEPSettings cepSettings;
    
    @Autowired
    private CEPConfigService cepConfigService;

    @Autowired
    private XMLParser xmlParser;

    private EPServiceProvider  epServiceProvider;
    private EPRuntime          epRuntime;

    private final BlockingQueue<Node> nodeQueue = new LinkedBlockingQueue<>();
    private final AtomicBoolean       running   = new AtomicBoolean( false );
    private final AtomicLong          heartbeat = new AtomicLong( 0 );    
    private final AtomicLong          eventsIn  = new AtomicLong( 0 );    
    private Thread esperThread = null;

    private final Set<String> allowedEvents = new HashSet<>();
    private final Map<String,EventSender> eventSenderMap = new HashMap<>();
    private final ReentrantLock           eventSenderMapLock = new ReentrantLock();

    private final Set<String>    rejectedFilenames     = new HashSet<>();
    private final ReentrantLock  rejectedFilenamesLock = new ReentrantLock();

    private final BlockingQueue<CEPServiceRequest> serviceRequestQueue = new LinkedBlockingQueue<>();

    private void initialize()
    {
        logger.info( String.format( "Will initialize Esper %s", com.espertech.esper.util.Version.VERSION ) );
        if ( cepSettings.isEnabled() )
        {
            heartbeat.set( 0 );
            Configuration config = setupEsper();
            epServiceProvider = EPServiceProviderManager.getProvider( cepSettings.getProviderName(), config );
            epRuntime = epServiceProvider.getEPRuntime();
            initializeEventTypesFromXML();
        }
        else
            epServiceProvider = null;
    }

    private void close()
    {
        if ( epServiceProvider != null )
        {
            epServiceProvider.destroy();
            epRuntime = null;
        }
        dropEventSenders();
        allowedEvents.clear();
        heartbeat.set( 0 );
        logger.info( "Esper was shut down" );
    }

    protected Configuration setupEsper()
    {
        Configuration config = new Configuration();
        if ( cepSettings.isEnabledExecutionDebug() )
            config.getEngineDefaults().getLogging().setEnableExecutionDebug( true );
        config.addVariable( "alertSent", Boolean.class, "false" );

        // Add external plugins and configurations
        setDomainFunctionsConfiguration( config );

        // Add internal plugins
        addLocalPlugins( config );
        addTimeFunctions( config );

        return( config );
    }

    private void setDomainFunctionsConfiguration( Configuration config )
    {
        logger.debug( "Will setup domain functions" );
        DomainValueSets.resetDomainIndex();
        for( DomainConfiguration domainConfig: cepConfigService.getAllDomains() )
        {
            String methodName = DomainValueSets.assignDomainValues( domainConfig.getName(), domainConfig.getValueMap() );
            config.addPlugInSingleRowFunction( domainConfig.getName(), "com.ecodigi.cep.accessor.DomainValueSets", methodName );
            logger.info( String.format( "Domain config %s: method %s", domainConfig.getName(), methodName ) );
        }
        
        NamedValuesSets.resetNamedValuesIndex();
        for( NamedValuesConfiguration namedValuesConfig: cepConfigService.getAllNamedValues() )
        {
            String methodName = NamedValuesSets.assignNamedValuesValues( namedValuesConfig.getName(), namedValuesConfig.getValueMap() );
            config.addPlugInSingleRowFunction( namedValuesConfig.getName(), "com.ecodigi.cep.accessor.NamedValuesSets", methodName );
            logger.info( String.format( "Named values config %s: method %s", namedValuesConfig.getName(), methodName ) );
        }
    }

    private void addLocalPlugins( Configuration config )
    {
        config.addPlugInSingleRowFunction( "markAlertSent",  "com.ecodigi.cep.accessor.AlertState", "markAlertSent" );
        config.addPlugInSingleRowFunction( "resetAlertSent", "com.ecodigi.cep.accessor.AlertState", "resetAlertSent" );
        config.addPlugInSingleRowFunction( "wasAlertSent",   "com.ecodigi.cep.accessor.AlertState", "wasAlertSent" );
    }

    private void addTimeFunctions( Configuration config )
    {
        config.addPlugInSingleRowFunction( "streamTimestamp",    "com.ecodigi.cep.accessor.GlobalState",   "streamTimestamp" );
        config.addPlugInSingleRowFunction( "streamBehindMillis", "com.ecodigi.cep.accessor.GlobalState",   "streamBehindMillis" );
        config.addPlugInSingleRowFunction( "systemTimestamp",    "com.ecodigi.cep.accessor.GlobalState",   "systemTimestamp" );
    }

    private ConfigurationEventTypeXMLDOM getConfigurationEventTypeXMLDOM( File schemaFile, String rootNode )
    {
        ConfigurationEventTypeXMLDOM xmlConfig = new ConfigurationEventTypeXMLDOM();
        try
        {
            URL schemaURL = schemaFile.toURI().toURL();
            if ( schemaURL == null )
                throw new EcodigiException( String.format( "Unable to load Events Schema from '%s'", cepSettings.getEventsSchema() ) );

            xmlConfig.setRootElementName( rootNode );
            xmlConfig.setSchemaResource( schemaURL.toString() );
            xmlConfig.setEventSenderValidatesRoot( false );
        }
        catch( MalformedURLException e )
        {
            throw new EcodigiException( String.format( "Unable to load Events Schema from '%s'", cepSettings.getEventsSchema() ), e );
        }
        return( xmlConfig );
    }

    private void initializeEventTypesFromXML()
    {
        logger.info( String.format( "Will load Events Schema from '%s'", cepSettings.getEventsSchema() ) );
        File schemaFile = new File( cepSettings.getEventsSchema() );
        if ( !schemaFile.exists() )
            throw new EcodigiException( String.format( "Unable to load Events Schema from '%s'. File not found", cepSettings.getEventsSchema() ) );

        ConfigurationOperations configOps = epServiceProvider.getEPAdministrator().getConfiguration();
        configOps.addEventType( AlertEngineConstants.EVENTS_FILE_ROOT_NODE, getConfigurationEventTypeXMLDOM( schemaFile, AlertEngineConstants.EVENTS_FILE_ROOT_NODE ) );

        try
        {
            Document doc = xmlParser.parse( cepSettings.getEventsSchema() );
            // Find "events" root node
            Node eventsNode = null;
            NodeList rootNodes = doc.getElementsByTagName( XMLParser.XSD_ELEMENT );
            for( int i = 0; i < rootNodes.getLength(); i++ )
            {
                Node childNode = rootNodes.item( i );
                if ( AlertEngineConstants.EVENTS_FILE_ROOT_NODE.equals( childNode.getAttributes().getNamedItem( "name" ).getNodeValue() ) )
                {
                    eventsNode = childNode;
                    break;
                }
            }
            if ( eventsNode == null )
                throw new EcodigiException( String.format( "No root element %s found. Invalid schema %s ", AlertEngineConstants.EVENTS_FILE_ROOT_NODE, cepSettings.getEventsSchema() ) );

            // Retrieve list with all types of events expected
            String type = eventsNode.getAttributes().getNamedItem( "type" ).getNodeValue();
            NodeList complexTypes = doc.getElementsByTagName( XMLParser.XSD_COMPLEX_TYPE );
            Node elType = null;
            for ( int i = 0; i < complexTypes.getLength(); i++ )
            {
                Node node = complexTypes.item( i );
                if ( type.equals( node.getAttributes().getNamedItem( "name" ).getNodeValue() ) )
                {
                    elType = complexTypes.item( i );
                    break;
                }
            }

            if ( elType != null )
            {
                // Configure all event types
                allowedEvents.clear();
                NodeList childNodes = elType.getChildNodes().item( 1 ).getChildNodes();
                for ( int i = 0; i < childNodes.getLength(); i++ )
                {
                    if ( XMLParser.XSD_ELEMENT.equals( childNodes.item( i ).getNodeName() ) )
                    {
                        Node entryNode = childNodes.item( i );
                        String value = entryNode.getAttributes().getNamedItem( "type" ).getNodeValue();
                        String rootElement = StringUtils.uncapitalize( value );
                        logger.info( String.format( "Adding %s to xmlConfig @ %s", value, rootElement ) );
                        configOps.addEventType( value, getConfigurationEventTypeXMLDOM( schemaFile, rootElement ) );
                        allowedEvents.add( value );
                    }
                }

                // Print events properties
                EventType[] eventTypes = configOps.getEventTypes();
                for( EventType evType: eventTypes )
                {
                    logger.info( String.format( "***** Event %s *****", evType.getName() ) ); 
                    for( String propName: evType.getPropertyNames() )
                        logger.info( String.format( "Property %s Type %s", propName, evType.getPropertyType( propName ) ) );
                }
            }
            else
                throw new EcodigiException( "No element type found. Invalid schema: " + cepSettings.getEventsSchema() );
        }
        catch ( Exception e )
        {
            throw new EcodigiException( String.format( "Unable to parse Events Schema from '%s'", cepSettings.getEventsSchema() ), e );
        }
    }

    public boolean isAllowedEvent( String eventName )
    {
        return( allowedEvents.contains( eventName ) );
    }

    public EPStatement createEPStatement( String epl )
    {
        return( epServiceProvider.getEPAdministrator().createEPL( epl ) );
    }

    private void holdDemand( int queueSize )
    {
        long sleepTimeMillis;
        if ( queueSize < 10000 )
            return;
        if ( queueSize < 50000 )
            sleepTimeMillis = 5;
        else if ( queueSize < 100000 )
            sleepTimeMillis = 50;
        else if ( queueSize < 1000000 )
            sleepTimeMillis = 500;
        else
            sleepTimeMillis = 1000;

        try
        {
            Thread.sleep( sleepTimeMillis );
        }
        catch( InterruptedException e )
        {
        }
    }

    public void pushEvent( Node node )
    {
        holdDemand( nodeQueue.size() );
        nodeQueue.offer( node );
        eventsIn.incrementAndGet();
    }

    public int getInputQueueSize()
    {
        return( nodeQueue.size() );
    }

    public int getRequestQueueSize()
    {
        return( serviceRequestQueue.size() );
    }

    private EventSender getEventSender( String eventName )
    {
        EventSender sender = null;
        eventSenderMapLock.lock();
        try
        {
            sender = eventSenderMap.get( eventName );
            if ( sender == null )
            {
                sender = epRuntime.getEventSender( eventName );
                eventSenderMap.put( eventName, sender );
                logger.info( String.format( "Creating new EventSender for '%s'", eventName ) );
            }
        }
        finally
        {
            eventSenderMapLock.unlock();
        }
        return( sender );
    }
    
    private void dropEventSenders()
    {
        eventSenderMapLock.lock();
        try
        {
            eventSenderMap.clear();
        }
        finally
        {
            eventSenderMapLock.unlock();
        }
    }

    public void requestService( CEPServiceRequest serviceRequest )
    {
        serviceRequestQueue.offer( serviceRequest );
    }

    public boolean processRequests()
    {
        boolean actionDone = false;
        while ( running.get() )
        {
            CEPServiceRequest serviceRequest = serviceRequestQueue.poll();
            if ( serviceRequest != null )
            {
                serviceRequest.execute( this );
                actionDone = true;
            }
            else
                break;
        }
        return( actionDone );
    }

    public boolean processEvents()
    {
        boolean actionDone = false;
        while ( running.get() )
        {
            Node node = nodeQueue.poll();
            if ( node != null )
            {
                if ( StringUtils.isBlank( node.getNodeName() ) )
                {
                    logger.warn( String.format( "Invalid node name '%s' at node '%s'", node.getNodeName(), xmlParser.getNodeAsString( node ) ) );
                    rejectFileFromNode( node );
                    continue;
                }

                try
                {
                    getEventSender( node.getNodeName() ).sendEvent( node );
                    actionDone = true;
                }
                catch( Throwable e )
                {
                    rejectFileFromNode( node );
                    logger.error( "Unable to process node: " + xmlParser.getNodeAsString( node ), e );
                    break;
                }
            }
            else
                break;
        }
        return( actionDone );
    }
    
    private void rejectFileFromNode( Node node )
    {
        String filename = ( String )node.getUserData( EventConsumer.KEY_XML_FILE_NAME );
        logger.warn( String.format( "Rejected file '%s'", filename ) );
        rejectedFilenamesLock.lock();
        try
        {
            rejectedFilenames.add( filename );
        }
        finally
        {
            rejectedFilenamesLock.unlock();
        }
    }
    
    public List<String> getRejectedFiles()
    {
        List<String> rejectedFiles = null;
        rejectedFilenamesLock.lock();
        try
        {
            if ( !rejectedFilenames.isEmpty() )
            {
                rejectedFiles = new ArrayList<String>();
                rejectedFiles.addAll( rejectedFilenames );
                rejectedFilenames.clear();
            }
        }
        finally
        {
            rejectedFilenamesLock.unlock();
        }
        return( rejectedFiles );
    }

    private void serviceSleep()
    {
        try
        {
            Thread.sleep( ESPER_SERVICING_INTERVAL_MILLIS );
        }
        catch( InterruptedException e )
        {
        }
    }

    public void start()
    {
        running.set( false );
        esperThread = new Thread( this, "Esper Thread" );
        esperThread.start();
        logger.debug( "Waiting for Esper startup" );
        while( !running.get() )
            serviceSleep();
        logger.debug( "Esper initialization completed" );
    }

    public void stop()
    {
        if ( esperThread != null )
        {
            running.set( false );
            esperThread.interrupt();
            try
            {
                esperThread.join( ESPER_SHUTDOWN_TIMEOUT_MILLIS );
            }
            catch( InterruptedException e )
            {
            }
            esperThread = null;
        }
    }

    public void checkThreadHealth()
    {
        long elapsed = System.currentTimeMillis() - heartbeat.get();
        if ( elapsed > ESPER_SERVICING_TIMEOUT_MILLIS )
            logger.error( String.format( "Esper service thread may be unavailable or unstable. Last heartbeat was %s ago", DateUtils.getDurationStr( elapsed ) ) );
    }

    public long getHeartBeat()
    {
        return( heartbeat.get() );
    }

    public long getEventsIn()
    {
        return( eventsIn.get() );
    }

    @Override
    public void run()
    {
        initialize();
        running.set( true );
        while ( running.get() )
        {
            heartbeat.set( System.currentTimeMillis() );
            try
            {
                boolean actionDone = processRequests();
                actionDone |= processEvents();
                if ( !actionDone )
                    serviceSleep();
            }
            catch( Throwable e )
            {
                logger.error( "Exception at service thread", e );
                serviceSleep();
            }
        }
        close();
    }
}
