/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.esper;

import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.UpdateListener;
import java.util.LinkedHashSet;
import java.util.Set;

public class StatementBean
{
    private final String epl;
    private EPStatement epStatement;
    private final Set<UpdateListener> listeners = new LinkedHashSet<UpdateListener>();

    public StatementBean(String epl)
    {
        this.epl = epl;
    }

    public String getEPL()
    {
        return epl;
    }

    public void setListeners(UpdateListener... listeners)
    {
        for (UpdateListener listener : listeners)
        {
            addListener(listener);
        }
    }
    public void addListener(UpdateListener listener)
    {
        listeners.add(listener);
        if (epStatement != null)
        {
            epStatement.addListener(listener);
        }
    }

    void setEPStatement(EPStatement epStatement)
    {
        this.epStatement = epStatement;
        for (UpdateListener listener : listeners)
        {
            epStatement.addListener(listener);
        }
    }
}
