/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.cep.CEPSettings;
import com.ecodigi.domain.AlertEngineConstants;
import com.ecodigi.domain.cep.AlertConfiguration;
import com.ecodigi.domain.cep.DomainConfiguration;
import com.ecodigi.domain.cep.EPLFeedConfiguration;
import com.ecodigi.domain.cep.EventSourceConfiguration;
import com.ecodigi.domain.cep.HealthReport;
import com.ecodigi.domain.cep.NamedValuesConfiguration;
import com.ecodigi.domain.cep.OutputEvent;
import com.ecodigi.domain.cep.OutputEventEntry;
import com.ecodigi.domain.cep.OutputEventType;
import com.ecodigi.util.DateUtils;
import com.ecodigi.util.LoggableContextAwareBean;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.security.ArrayTypePermission;
import com.thoughtworks.xstream.security.NoTypePermission;
import com.thoughtworks.xstream.security.NullPermission;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XStreamEngine extends LoggableContextAwareBean
{
    private static final String ISO8601_DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

    @Autowired
    private CEPSettings cepSettings;

    private final XStream xstream = new XStream();
    private final AtomicLong eventCounter = new AtomicLong( 0 );
    private final AtomicLong reportSequenceFactory = new AtomicLong( 0 );

    @PostConstruct
    public void initialize()
    {
        // clear out existing permissions and set own ones
        xstream.addPermission( NoTypePermission.NONE );

        // allow some basics
        xstream.addPermission( NullPermission.NULL );
        xstream.addPermission( PrimitiveTypePermission.PRIMITIVES );
        xstream.addPermission( ArrayTypePermission.ARRAYS );
        xstream.allowTypeHierarchy( Collection.class );
        xstream.allowTypesByWildcard( new String[] { "java.lang.*", "java.util.**" } );

        // Allow only CEP Classes
        Class<?>[] classes = new Class<?>[] { OutputEvent.class, OutputEventEntry.class, OutputEventType.class,
                                        EventSourceConfiguration.class, AlertConfiguration.class, EPLFeedConfiguration.class,
                                        DomainConfiguration.class, NamedValuesConfiguration.class, HealthReport.class };
        xstream.allowTypes( classes );
        xstream.processAnnotations( classes );
        xstream.registerConverter( new DateConverter( ISO8601_DATEFORMAT, null ) );
    }

    @SuppressWarnings("unchecked")
    public <T extends Object> T read( Class<T> type, Reader reader )
    {
        try
        {
            Object obj = xstream.fromXML( reader );
            return( ( T )obj );
        }
        catch( Throwable e )
        {
            logger.error( "Unable to read XML into class " + type.getName(), e );
        }
        return( null );
    }

    public boolean write( Object object, String prefix, String suffix )
    {
        String filename = String.format( "%s-%s-%d%s", prefix, DateUtils.getIsoTimestampString( System.currentTimeMillis() ), eventCounter.incrementAndGet(), suffix );
        return( write( object, filename, false ) );
    }

    public boolean write( HealthReport healthReport )
    {
        String filename = String.format( "%s-%d%s", AlertEngineConstants.HEALTH_REPORT_FILENAME, reportSequenceFactory.incrementAndGet(), AlertEngineConstants.HEALTH_REPORT_EXTENSION );
        return( write( healthReport, filename, true ) );
    }

    public boolean write( Object object, String filename, boolean replaceExisting )
    {
        boolean success = false;
        Path tempPath = Paths.get( cepSettings.getTempOutputPath(), filename );
        Path filePath = Paths.get( cepSettings.getOutputPath(), filename );
        if ( logger.isDebugEnabled() )
            logger.debug( String.format( "Will write to '%s'", tempPath.toString() ) );
        try
        {
            Writer out = new BufferedWriter( new FileWriter( tempPath.toFile() ) );
            xstream.toXML( object, out );
            out.close();
            if ( replaceExisting )
                Files.move( tempPath, filePath, StandardCopyOption.REPLACE_EXISTING );
            else
                Files.move( tempPath, filePath );
            success = true;
        }
        catch( Throwable e )
        {
            logger.error( String.format( "Unable to write XML file '%s'", tempPath.toString() ), e );
        }
        return( success );
    }
}
