/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.domain.AlertEngineConstants;
import com.ecodigi.domain.utils.EventSeverity;
import com.ecodigi.domain.cep.OutputEvent;
import com.ecodigi.domain.cep.OutputEventType;
import com.ecodigi.runlevel.RunLevelHandler;
import com.ecodigi.runlevel.ServiceRunLevelType;
import com.ecodigi.util.LoggableContextAwareBean;
import java.util.concurrent.LinkedBlockingQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class NotificationPublisher extends LoggableContextAwareBean
{
    private static final long NOTIFICATION_DELIVERY_INTERVAL_MILLIS = 100;

    private final LinkedBlockingQueue<OutputEvent> outputQueue = new LinkedBlockingQueue<OutputEvent>();

    @Autowired
    private XStreamEngine xstreamEngine;

    @Autowired
    private RunLevelHandler runLevelHandler;

    @Autowired
    private StreamTimeService streamTimeService;
    
    public void sendNotification( EventSeverity eventSeverity, String message )
    {
        sendNotification( OutputEventType.Notification, eventSeverity, message, null, null );
    }

    public void sendNotification( OutputEventType eventType, EventSeverity eventSeverity, String message, Long sourceId, String sourceName )
    {
        OutputEvent outputEvent = new OutputEvent( eventType, getCurrentTime() );
        if ( sourceId != null )
        {
            if ( logger.isDebugEnabled() )
                logger.debug( String.format( "%s %s: %s [%d]", eventType.name(), eventSeverity.name(), message, sourceId ) );
            outputEvent.setSourceId( sourceId );
        }
        if ( sourceName != null )
        {
            if ( logger.isDebugEnabled() )
                logger.debug( String.format( "%s %s: %s [%s]", eventType.name(), eventSeverity.name(), message, sourceName ) );
            outputEvent.setSourceName( sourceName );
        }
        outputEvent.setEventSeverity( eventSeverity );
        outputEvent.setMessage( message );
        outputQueue.offer( outputEvent );
    }

    private long getCurrentTime()
    {
        if ( streamTimeService == null )
            return( TimeService.coldGetMicroseconds() );
        else
            return( streamTimeService.getCurrentTime() );
    }

    @Scheduled( initialDelay = ServiceRunLevelType.DEFAULT_BASIC_LEVEL_INITIAL_WAIT, fixedDelay = NOTIFICATION_DELIVERY_INTERVAL_MILLIS )
    protected void deliverNotifications()
    {
        while ( runLevelHandler.handleServiceState( "Notification Publisher" ) )
        {
            OutputEvent outEvent = outputQueue.poll();
            if ( outEvent != null )
            {
                if ( !publish( outEvent ) )
                    break;
            }
            else
                break;
        }
    }

    private boolean publish( OutputEvent outEvent )
    {
        return( xstreamEngine.write( outEvent, "msg", AlertEngineConstants.MESSAGE_FILE_EXTENSION ) );
    }
}
