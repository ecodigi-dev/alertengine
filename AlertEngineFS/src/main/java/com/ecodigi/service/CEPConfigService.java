/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.cep.CEPManager;
import com.ecodigi.cep.accessor.DomainValueSets;
import com.ecodigi.cep.accessor.NamedValuesSets;
import com.ecodigi.domain.cep.AlertConfiguration;
import com.ecodigi.domain.AlertEngineConstants;
import com.ecodigi.domain.cep.DomainConfiguration;
import com.ecodigi.domain.cep.EPLFeedConfiguration;
import com.ecodigi.domain.utils.EventSeverity;
import com.ecodigi.domain.cep.EventSourceConfiguration;
import com.ecodigi.domain.cep.NamedValuesConfiguration;
import com.ecodigi.domain.cep.OutputEventType;
import com.ecodigi.io.FileSuffixFilter;
import com.ecodigi.lang.EcodigiException;
import com.ecodigi.runlevel.RunLevelHandler;
import com.ecodigi.runlevel.ServiceRunLevelType;
import com.ecodigi.stream.FileListenerOperation;
import com.ecodigi.stream.PoolingDirectoryFileListener;
import com.ecodigi.util.LoggableContextAwareBean;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CEPConfigService extends LoggableContextAwareBean
{
    public static final boolean ADD_EVENT                      = false;
    public static final boolean DELETE_EVENT                   = true;

    @Value( "${cep.expressionsPath}" )
    private String expressionsPath;

    @Autowired
    private CEPManager cepManager;

    @Autowired
    private NotificationPublisher notificationPublisher;

    @Autowired
    private XStreamEngine xStream;

    @Autowired
    private RunLevelHandler runLevelHandler;

    private final Map<Long,EventSourceConfiguration> sources = new HashMap<>();
    private final ReentrantLock sourcesLock = new ReentrantLock();
    private final Map<Long,AlertConfiguration> alerts = new HashMap<>();
    private final ReentrantLock alertsLock = new ReentrantLock();
    private final Map<String,EPLFeedConfiguration> feeds = new HashMap<>();
    private final ReentrantLock feedsLock = new ReentrantLock();
    private final Map<Long,DomainConfiguration> domains = new HashMap<>();
    private final ReentrantLock domainsLock = new ReentrantLock();
    private final Map<Long,NamedValuesConfiguration> namedValues = new HashMap<>();
    private final ReentrantLock namedValuesLock = new ReentrantLock();

    private PoolingDirectoryFileListener fileListener;

    private final Pattern eventSourceConfigPattern = Pattern.compile( AlertEngineConstants.EVENT_SOURCE_CONFIG_REGEX );
    private final Pattern alertConfigPattern       = Pattern.compile( AlertEngineConstants.ALERT_CONFIG_REGEX );
    private final Pattern feedConfigPattern        = Pattern.compile( AlertEngineConstants.FEED_CONFIG_REGEX );
    private final Pattern validationConfigPattern  = Pattern.compile( AlertEngineConstants.VALIDATION_CONFIG_REGEX );

    @Scheduled( initialDelay = ServiceRunLevelType.DEFAULT_APPLICATION_LEVEL_INITIAL_WAIT, fixedDelay = PoolingDirectoryFileListener.FILE_PROCESS_POOLING_INTERVAL )
    private void processEntries()
    {
        if ( fileListener == null )
            return;

        if ( runLevelHandler.handleServiceState( "CEP Config File Listener" ) )
        {
            while ( fileListener.processEntries() )
            {
            }
        }
    }

    public void initializeDomains()
    {
        loadDomains();
        loadNamedValues();
    }

    public void initializeExpressions()
    {
        loadEventSources();
        loadAlerts();
        loadFeeds();
    }

    public void initializeFileListener() throws IOException
    {
        String[] extensions = new String[] { AlertEngineConstants.EVENT_SOURCE_CONFIG_EXTENSION, AlertEngineConstants.ALERT_CONFIG_EXTENSION,
                                             AlertEngineConstants.FEED_CONFIG_EXTENSION, AlertEngineConstants.DOMAIN_CONFIG_EXTENSION,
                                             AlertEngineConstants.NAMED_VALUES_CONFIG_EXTENSION };
        fileListener = new PoolingDirectoryFileListener( expressionsPath, extensions, PoolingDirectoryFileListener.MANUAL_DELETE, PoolingDirectoryFileListener.IGNORE_FILES_AT_STARTUP,
                                                         PoolingDirectoryFileListener.DEFAULT_HISTORY_FILE_COUNT, PoolingDirectoryFileListener.DEFAULT_POOLING_INTERVAL,
                                                         PoolingDirectoryFileListener.ALL_OPERATIONS )
        {
            // Overriden for the internal thread not to be created and file processing thread be delegated to Spring
            @Override
            protected void startProcessThread()
            {
            }

            @Override
            public boolean process( File file, FileListenerOperation operation )
            {
                boolean newItem = FileListenerOperation.Created.equals( operation ) || FileListenerOperation.Modified.equals( operation ) ;
                boolean delItem = ( FileListenerOperation.Deleted.equals( operation ) );

                if ( ( !newItem && !delItem ) || ( newItem && delItem ) )
                {
                    logger.info( "Unrecognized event" );
                    return false;
                }

                updateConfig( file.toString(), delItem );
                return true;
            }
        };
        fileListener.start();
    }

    public void close()
    {
        fileListener.stop();
        dropDomains();
        dropFeeds();
        dropEventSources();
        dropAlerts();
        dropNamedValues();
    }

    private void updateConfig( String filename, boolean deleteItem )
    {
        if ( StringUtils.isBlank( filename ) )
            return;
        try
        {
            if ( logger.isDebugEnabled() )
                logger.debug( String.format( "Will process conf file '%s' to %s", filename, ( deleteItem ? "delete" : "add" ) ) );
            if ( filename.endsWith( AlertEngineConstants.ALERT_CONFIG_EXTENSION ) )
                loadAlert( filename, deleteItem );
            else if ( filename.endsWith( AlertEngineConstants.EVENT_SOURCE_CONFIG_EXTENSION ) )
                loadEventSource( filename, deleteItem );
            else if ( filename.endsWith( AlertEngineConstants.FEED_CONFIG_EXTENSION ) )
                loadFeed( filename, deleteItem );
            else if ( filename.endsWith( AlertEngineConstants.DOMAIN_CONFIG_EXTENSION ) )
                loadDomain( filename, deleteItem );
            else if ( filename.endsWith( AlertEngineConstants.NAMED_VALUES_CONFIG_EXTENSION ) )
                loadNamedValues( filename, deleteItem );
            else
                logger.error( String.format( "Unrecognized configuration file '%s' to %s", filename, ( deleteItem ? "delete" : "add" ) ) );
        }
        catch( IOException e )
        {
            logger.error( String.format( "Unable to process conf file '%s' to %s", filename, ( deleteItem ? "delete" : "add" ) ), e );
        }
    }

    private void loadEventSources()
    {
        logger.debug( "Will scan Event Sources under: " + expressionsPath + "'" );
        File baseDir = new File( expressionsPath );
        FileSuffixFilter fileFilter = new FileSuffixFilter( AlertEngineConstants.EVENT_SOURCE_CONFIG_EXTENSION );
        if ( baseDir.exists() )
        {
            File files[] = baseDir.listFiles( fileFilter );
            Arrays.sort( files );
            for( File file: files )
            {
                try
                {
                    loadEventSource( file.getPath(), ADD_EVENT );
                }
                catch( IOException e )
                {
                    logger.error( String.format( "Unable to load Event Source from '%s'", file.getPath() ), e );
                }
            }
        }
    }

    private void loadAlerts()
    {
        logger.debug( "Will scan Alerts under: " + expressionsPath + "'" );
        File baseDir = new File( expressionsPath );
        FileSuffixFilter fileFilter = new FileSuffixFilter( AlertEngineConstants.ALERT_CONFIG_EXTENSION );
        if ( baseDir.exists() )
        {
            File files[] = baseDir.listFiles( fileFilter );
            Arrays.sort( files );
            for( File file: files )
            {
                try
                {
                    loadAlert( file.getPath(), ADD_EVENT );
                }
                catch( IOException e )
                {
                    logger.error( String.format( "Unable to load Alert from '%s'", file.getPath() ), e );
                }
            }
        }
    }

    private void loadFeeds()
    {
        logger.debug( "Will scan EPL Feeds under: " + expressionsPath + "'" );
        File baseDir = new File( expressionsPath );
        FileSuffixFilter fileFilter = new FileSuffixFilter( AlertEngineConstants.FEED_CONFIG_EXTENSION );
        if ( baseDir.exists() )
        {
            File files[] = baseDir.listFiles( fileFilter );
            Arrays.sort( files );
            for( File file: files )
            {
                try
                {
                    loadFeed( file.getPath(), ADD_EVENT );
                }
                catch( IOException e )
                {
                    logger.error( String.format( "Unable to load EPL Feed from '%s'", file.getPath() ), e );
                }
            }
        }
    }

    private void loadDomains()
    {
        logger.debug( "Will scan Domains under: " + expressionsPath + "'" );
        File baseDir = new File( expressionsPath );
        FileSuffixFilter fileFilter = new FileSuffixFilter( AlertEngineConstants.DOMAIN_CONFIG_EXTENSION );
        if ( baseDir.exists() )
        {
            File files[] = baseDir.listFiles( fileFilter );
            Arrays.sort( files );
            for( File file: files )
            {
                try
                {
                    loadDomain( file.getPath(), ADD_EVENT );
                }
                catch( IOException e )
                {
                    logger.error( String.format( "Unable to load Domain from '%s'", file.getPath() ), e );
                }
            }
        }
    }

    private void loadNamedValues()
    {
        logger.debug( "Will scan Named Values under: " + expressionsPath + "'" );
        File baseDir = new File( expressionsPath );
        FileSuffixFilter fileFilter = new FileSuffixFilter( AlertEngineConstants.NAMED_VALUES_CONFIG_EXTENSION );
        if ( baseDir.exists() )
        {
            File files[] = baseDir.listFiles( fileFilter );
            Arrays.sort( files );
            for( File file: files )
            {
                try
                {
                    loadNamedValues( file.getPath(), ADD_EVENT );
                }
                catch( IOException e )
                {
                    logger.error( String.format( "Unable to load Named Values from '%s'", file.getPath() ), e );
                }
            }
        }
    }

    private void dropEventSources()
    {
        sourcesLock.lock();
        try
        {
            sources.clear();
        }
        finally
        {
            sourcesLock.unlock();
        }
    }

    private void dropAlerts()
    {
        alertsLock.lock();
        try
        {
            alerts.clear();
        }
        finally
        {
            alertsLock.unlock();
        }
    }

    private void dropFeeds()
    {
        feedsLock.lock();
        try
        {
            feeds.clear();
        }
        finally
        {
            feedsLock.unlock();
        }
    }

    private void dropDomains()
    {
        domainsLock.lock();
        try
        {
            domains.clear();
        }
        finally
        {
            domainsLock.unlock();
        }
    }

    private void dropNamedValues()
    {
        namedValuesLock.lock();
        try
        {
            namedValues.clear();
        }
        finally
        {
            namedValuesLock.unlock();
        }
    }

    private Long getEventSourceId( String filename )
    {
        Long id = null;
        Matcher matcher = eventSourceConfigPattern.matcher( FilenameUtils.getName( filename ) );
        if ( matcher.matches() )
            id = Long.parseLong( matcher.group( AlertEngineConstants.CONFIG_REGEX_ID_GROUP_NAME ) );
        return( id );
    }

    private Long getAlertId( String filename )
    {
        Long id = null;
        Matcher matcher = alertConfigPattern.matcher( FilenameUtils.getName( filename ) );
        if ( matcher.matches() )
            id = Long.parseLong( matcher.group( AlertEngineConstants.CONFIG_REGEX_ID_GROUP_NAME ) );
        return( id );
    }

    private String getFeedName( String filename )
    {
        String name = null;
        Matcher matcher = feedConfigPattern.matcher( FilenameUtils.getName( filename ) );
        if ( matcher.matches() )
            name = matcher.group( AlertEngineConstants.CONFIG_REGEX_ID_GROUP_NAME );
        return( name );
    }

    private boolean isValidation( String filename )
    {
        Matcher matcher = validationConfigPattern.matcher( FilenameUtils.getName( filename ) );
        return( matcher.matches() );
    }

    private void loadEventSource( String filename, boolean deleteItem ) throws IOException
    {
        if ( deleteItem )
        {
            Long eventSourceId = getEventSourceId( filename );
            if ( eventSourceId == null )
            {
                logger.error( "Unable to delete event source " + filename );
                return;
            }
            sourcesLock.lock();
            try
            {
                sources.remove( eventSourceId );
            }
            finally
            {
                sourcesLock.unlock();
            }

            cepManager.acquire();
            try
            {
                cepManager.removeSourceFeed( eventSourceId );
            }
            finally
            {
                cepManager.release();
            }
        }
        else
        {
            BufferedReader in = new BufferedReader( new FileReader( filename ) );
            EventSourceConfiguration eventSourceConfig = xStream.read( EventSourceConfiguration.class, in );
            in.close();

            if ( eventSourceConfig != null )
                updateSource( eventSourceConfig );
            else
            {
                String message = "Unable to read event source file " + filename;
                logger.error( message );
                Long eventSourceId = getEventSourceId( filename );
                notificationPublisher.sendNotification( OutputEventType.EventSourceValidation, EventSeverity.Error, message, eventSourceId, null );
            }
        }
    }

    private void loadAlert( String filename, boolean deleteItem ) throws IOException
    {
        if ( deleteItem )
        {
            Long alertId = getAlertId( filename );
            if ( alertId == null )
            {
                logger.error( "Unable to delete alert " + filename );
                return;
            }
            alertsLock.lock();
            try
            {
                alerts.remove( alertId );
            }
            finally
            {
                alertsLock.unlock();
            }

            cepManager.acquire();
            try
            {
                cepManager.removeAlert( alertId );
            }
            finally
            {
                cepManager.release();
            }
        }
        else
        {
            BufferedReader in = new BufferedReader( new FileReader( filename ) );
            AlertConfiguration alertConfig = xStream.read( AlertConfiguration.class, in );
            in.close();

            if ( alertConfig != null )
                updateAlert( alertConfig );
            else
            {
                String message = "Unable to read alert file " + filename;
                logger.error( message );
                Long alertId = getAlertId( filename );
                notificationPublisher.sendNotification( OutputEventType.AlertValidation, EventSeverity.Error, message, alertId, null );
            }
        }
    }

    private void loadFeed( String filename, boolean deleteItem ) throws IOException
    {
        boolean validation = isValidation( filename );

        if ( deleteItem )
        {
            if ( validation )
            {
                logger.debug( String.format( "Validation file %s ignored", filename ) );
                return;
            }
            String feedName = getFeedName( filename );
            if ( feedName == null )
            {
                logger.error( "Unable to delete EPL feed " + filename );
                return;
            }
            feedsLock.lock();
            try
            {
                feeds.remove( feedName );
            }
            finally
            {
                feedsLock.unlock();
            }

            cepManager.acquire();
            try
            {
                cepManager.removeEPLOutputFeed( feedName );
            }
            finally
            {
                cepManager.release();
            }
        }
        else
        {
            BufferedReader in = new BufferedReader( new FileReader( filename ) );
            EPLFeedConfiguration feedConfig = xStream.read( EPLFeedConfiguration.class, in );
            in.close();

            if ( feedConfig != null )
            {
                if ( StringUtils.isBlank( feedConfig.getName() ) )
                    logger.error( "Invalid feed config " + feedConfig.toString() );
                else
                {
                    if ( validation )
                    {
                        validateExpression( feedConfig.getName(), feedConfig.getExpression() );

                        // Remove temporary file
                        File validationFile = new File( filename );
                        validationFile.delete();
                    }
                    else
                        updateFeed( feedConfig );
                }
            }
            else
            {
                String message = "Unable to read feed config file " + filename;
                logger.error( message );
                String feedName = getFeedName( filename );
                notificationPublisher.sendNotification( OutputEventType.FeedValidation, EventSeverity.Error, message, null, feedName );
            }
        }
    }

    private void loadDomain( String filename, boolean deleteItem ) throws IOException
    {
        BufferedReader in = new BufferedReader( new FileReader( filename ) );
        DomainConfiguration domainConfig = xStream.read( DomainConfiguration.class, in );
        in.close();

        if ( domainConfig != null )
            updateDomain( domainConfig, deleteItem );
    }

    private void loadNamedValues( String filename, boolean deleteItem ) throws IOException
    {
        BufferedReader in = new BufferedReader( new FileReader( filename ) );
        NamedValuesConfiguration namedValuesConfig = xStream.read( NamedValuesConfiguration.class, in );
        in.close();

        if ( namedValuesConfig != null )
            updateNamedValues( namedValuesConfig, deleteItem );
    }

    public void updateSource( EventSourceConfiguration eventSourceConfig )
    {
        sourcesLock.lock();
        try
        {
            EventSourceConfiguration currentSourceConfig = sources.get( eventSourceConfig.getId() );
            if ( ( currentSourceConfig == null ) ||  !currentSourceConfig.equals( eventSourceConfig ) )
            {
                sources.remove( eventSourceConfig.getId() );

                // Start Event Source
                boolean success = false;
                String message = "";
                try
                {
                    cepManager.acquire();
                    try
                    {
                        if ( eventSourceConfig.isEnabled() )
                        {
                            success = cepManager.addSourceFeed( eventSourceConfig );
                            sources.put( eventSourceConfig.getId(), eventSourceConfig );
                        }
                        else
                            cepManager.removeSourceFeed( eventSourceConfig.getId() );
                    }
                    finally
                    {
                        cepManager.release();
                    }
                }
                catch( EcodigiException e )
                {
                    message = ( e.getCause() == null ? e.getMessage() : e.getCause().getMessage() );
                    logger.error( message, e );
                }
                catch( Throwable e )
                {
                    message = String.format( "Exception when updating Event Source %s: %s", eventSourceConfig.getName(), e.toString() );
                    logger.error( message, e );
                }

                logger.debug( "Will report about source " + eventSourceConfig.getName() );
                if ( success )
                    notificationPublisher.sendNotification( OutputEventType.EventSourceValidation, EventSeverity.Info, AlertEngineConstants.CEP_ACTION_SUCCESS, eventSourceConfig.getId(), null );
                else
                {
                    eventSourceConfig.setEnabled( false );
                    notificationPublisher.sendNotification( OutputEventType.EventSourceValidation, EventSeverity.Error, message, eventSourceConfig.getId(), null );
                }
            }
            else
            {
                // Nothing to be done
                notificationPublisher.sendNotification( OutputEventType.EventSourceValidation, EventSeverity.Info, AlertEngineConstants.CEP_ACTION_SUCCESS, eventSourceConfig.getId(), null );
            }
        }
        finally
        {
            sourcesLock.unlock();
        }
    }

    public void updateAlert( AlertConfiguration alertConfig )
    {
        alertsLock.lock();
        try
        {
            AlertConfiguration currentAlertConfig = alerts.get( alertConfig.getId() );
            if ( ( currentAlertConfig == null ) ||  !currentAlertConfig.equals( alertConfig ) )
            {
                alerts.remove( alertConfig.getId() );

                // Start alert
                boolean success = false;
                String message = "";
                try
                {
                    cepManager.acquire();
                    try
                    {
                        if ( alertConfig.isEnabled() )
                        {
                            success = cepManager.addAlert( alertConfig );
                            alerts.put( alertConfig.getId(), alertConfig );
                        }
                        else
                            cepManager.removeAlert( alertConfig.getId() );
                    }
                    finally
                    {
                        cepManager.release();
                    }
                }
                catch( EcodigiException e )
                {
                    message = ( e.getCause() == null ? e.getMessage() : e.getCause().getMessage() );
                    logger.error( message, e );
                }
                catch( Throwable e )
                {
                    message = String.format( "Exception when updating Alert %s: %s", alertConfig.getName(), e.toString() );
                    logger.error( message, e );
                }

                if ( success )
                    notificationPublisher.sendNotification( OutputEventType.AlertValidation, EventSeverity.Info, AlertEngineConstants.CEP_ACTION_SUCCESS, alertConfig.getId(), null );
                else
                {
                    alertConfig.setEnabled( false );
                    notificationPublisher.sendNotification( OutputEventType.AlertValidation, EventSeverity.Error, message, alertConfig.getId(), null );
                }
            }
            else
            {
                // Nothing to be done
                notificationPublisher.sendNotification( OutputEventType.AlertValidation, EventSeverity.Info, AlertEngineConstants.CEP_ACTION_SUCCESS, alertConfig.getId(), null );
            }
        }
        finally
        {
            alertsLock.unlock();
        }
    }

    public void updateFeed( EPLFeedConfiguration feedConfig )
    {
        feedsLock.lock();
        try
        {
            EPLFeedConfiguration currentFeedConfig = feeds.get( feedConfig.getName() );
            if ( ( currentFeedConfig == null ) ||  !currentFeedConfig.equals( feedConfig ) )
            {
                feeds.remove( feedConfig.getName() );

                // Start EPL Feed
                boolean success = false;
                String message = "";
                try
                {
                    cepManager.acquire();
                    try
                    {
                        success = cepManager.addEPLOutputFeed( feedConfig.getName(), feedConfig.getExpression() );
                        feeds.put( feedConfig.getName(), feedConfig );
                    }
                    finally
                    {
                        cepManager.release();
                    }
                    if ( !success )
                    {
                        message = String.format( "Unable to add EPL Feed %s", feedConfig.getName() );
                        logger.error( message );
                    }
                }
                catch( EcodigiException e )
                {
                    message = ( e.getCause() == null ? e.getMessage() : e.getCause().getMessage() );
                    logger.error( message, e );
                }
                catch( Throwable e )
                {
                    message = String.format( "Exception when adding EPL Feed %s: %s", feedConfig.getName(), e.toString() );
                    logger.error( message, e );
                }

                if ( success )
                    notificationPublisher.sendNotification( OutputEventType.FeedValidation, EventSeverity.Info, AlertEngineConstants.CEP_ACTION_SUCCESS, null, feedConfig.getName() );
                else
                {
                    feedConfig.setEnabled( false );
                    notificationPublisher.sendNotification( OutputEventType.FeedValidation, EventSeverity.Error, message, null, feedConfig.getName() );
                }
            }
            else
            {
                // Nothing to be done
                notificationPublisher.sendNotification( OutputEventType.FeedValidation, EventSeverity.Info, AlertEngineConstants.CEP_ACTION_SUCCESS, null, feedConfig.getName() );
            }
        }
        finally
        {
            feedsLock.unlock();
        }
    }

    private void validateExpression( String name, String expression )
    {
        boolean success = false;
        String message = "";
        try
        {
            success = cepManager.validateExpression( expression );
            if ( !success )
            {
                message = String.format( "Unable to validate EPL expression '%s'", expression );
                logger.error( message );
            }
        }
        catch( EcodigiException e )
        {
            message = ( e.getCause() == null ? e.getMessage() : e.getCause().getMessage() );
            logger.error( message, e );
        }
        catch( Throwable e )
        {
            message = e.getMessage();
            logger.error( message, e );
        }

        if ( success )
            notificationPublisher.sendNotification( OutputEventType.ExpressionValidation, EventSeverity.Info, AlertEngineConstants.CEP_ACTION_SUCCESS, null, name );
        else
            notificationPublisher.sendNotification( OutputEventType.ExpressionValidation, EventSeverity.Error, message, null, name );
    }

    public void updateDomain( DomainConfiguration domainConfig, boolean deleteItem )
    {
        domainsLock.lock();
        try
        {
            if ( deleteItem )
            {
                domains.remove( domainConfig.getId() );
            }
            else
            {
                DomainConfiguration currentDomainConfig = domains.get( domainConfig.getId() );
                if ( ( currentDomainConfig == null ) ||  !currentDomainConfig.equals( domainConfig ) )
                {
                    domains.put( domainConfig.getId(), domainConfig );
                    DomainValueSets.updateDomainValues( domainConfig );
                    logger.info( String.format( "Domain %s content updated", domainConfig.getName() ) );
                }
            }
        }
        finally
        {
            domainsLock.unlock();
        }
    }

    public void updateNamedValues( NamedValuesConfiguration namedValuesConfig, boolean deleteItem )
    {
        namedValuesLock.lock();
        try
        {
            if ( deleteItem )
            {
                namedValues.remove( namedValuesConfig.getId() );
            }
            else
            {
                NamedValuesConfiguration currentNamedValuesConfig = namedValues.get( namedValuesConfig.getId() );
                if ( ( currentNamedValuesConfig == null ) ||  !currentNamedValuesConfig.equals( namedValuesConfig ) )
                {
                    namedValues.put( namedValuesConfig.getId(), namedValuesConfig );
                    NamedValuesSets.updateNamedValues( namedValuesConfig );
                    logger.info( String.format( "NamedValues %s content updated", namedValuesConfig.getName() ) );
                }
            }
        }
        finally
        {
            namedValuesLock.unlock();
        }
    }

    public List<AlertConfiguration> getAllAlerts()
    {
        List<AlertConfiguration> list = new ArrayList<>();
        alertsLock.lock();
        try
        {
            list.addAll( alerts.values() );
        }
        finally
        {
            alertsLock.unlock();
        }
        return( list );
    }

    public AlertConfiguration getAlertConfig( Long id )
    {
        alertsLock.lock();
        try
        {
            return( alerts.get( id ) );
        }
        finally
        {
            alertsLock.unlock();
        }
    }

    public List<EventSourceConfiguration> getAllEventSources()
    {
        List<EventSourceConfiguration> list = new ArrayList<>();
        sourcesLock.lock();
        try
        {
            list.addAll( sources.values() );
        }
        finally
        {
            sourcesLock.unlock();
        }
        return( list );
    }

    public EventSourceConfiguration getEventSourceConfig( Long id )
    {
        sourcesLock.lock();
        try
        {
            return( sources.get( id ) );
        }
        finally
        {
            sourcesLock.unlock();
        }
    }

    public List<EPLFeedConfiguration> getAllFeeds()
    {
        List<EPLFeedConfiguration> list = new ArrayList<>();
        feedsLock.lock();
        try
        {
            list.addAll( feeds.values() );
        }
        finally
        {
            feedsLock.unlock();
        }
        return( list );
    }

    public EPLFeedConfiguration getFeedConfig( String name )
    {
        feedsLock.lock();
        try
        {
            return( feeds.get( name ) );
        }
        finally
        {
            feedsLock.unlock();
        }
    }

    public List<DomainConfiguration> getAllDomains()
    {
        List<DomainConfiguration> list = new ArrayList<>();
        domainsLock.lock();
        try
        {
            list.addAll( domains.values() );
        }
        finally
        {
            domainsLock.unlock();
        }
        return( list );
    }

    public List<NamedValuesConfiguration> getAllNamedValues()
    {
        List<NamedValuesConfiguration> list = new ArrayList<>();
        namedValuesLock.lock();
        try
        {
            list.addAll( namedValues.values() );
        }
        finally
        {
            namedValuesLock.unlock();
        }
        return( list );
    }
}
