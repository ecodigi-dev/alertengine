/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.domain.utils.LongHistoryRecord;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

@Service
public class StreamTimeService
{
    public static final long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;

    private static final int  STREAM_TIME_MESSAGES_DEPTH   = 100;
    private static final long STREAM_TIME_BEHIND_THRESHOLD = 60000;

    private static final TimeZone GMTTimeZone = TimeZone.getTimeZone( "GMT" );

    @Autowired
    private TimeService             timeService;

    private LongHistoryRecord streamTimeHistory;

    private final DateFormat timeFormat = new SimpleDateFormat( "HH:mm:ss" );

    @PostConstruct
    public void init()
    {
        streamTimeHistory = new LongHistoryRecord( STREAM_TIME_MESSAGES_DEPTH );
        timeFormat.setTimeZone( GMTTimeZone );
    }

    public Long getStreamTime()
    {
        return( streamTimeHistory.getMax() );
    }

    public long getCurrentTime()
    {
        Long streamTime = streamTimeHistory.getMax();
        if ( streamTime == null )
            return( timeService.getMicroseconds() );
        else
            return( streamTime );
    }

    private long shiftMicrosFromUTC( long whenMicros, TimeZone targetTimeZone )
    {
        return( whenMicros + targetTimeZone.getOffset( whenMicros / 1000L ) * 1000L );
    }

    public Long getStreamTime( TimeZone timeZone )
    {
        Long streamTime = streamTimeHistory.getMax();
        if ( streamTime == null )
            return( null );

        return( shiftMicrosFromUTC( streamTime, timeZone ) );
    }

    public void putStreamTime( Long streamTime )
    {
        if ( streamTime == null )
            return;

        if ( !streamTimeHistory.isFilled() || ( streamTime > streamTimeHistory.getMin() ) )
            streamTimeHistory.put( streamTime );
    }

    public long getBehindMillis()
    {
        Long streamTime = getStreamTime();
        if ( streamTime == null )
            return( 0 );

        long behindMillis = ( timeService.getMicroseconds() - streamTime ) / 1000;

        return( behindMillis > 0 ? behindMillis : 0 );
    }

    public boolean isBehind()
    {
        long behindMillis = getBehindMillis();
        return( behindMillis > STREAM_TIME_BEHIND_THRESHOLD );
    }

    public String getBehindString( MessageSourceAccessor messageSourceAccessor )
    {
        long behindMillis = getBehindMillis();
        if ( behindMillis > STREAM_TIME_BEHIND_THRESHOLD )
        {
            StringBuilder behind = new StringBuilder( messageSourceAccessor.getMessage( "streamTime.behind" ) ).append( ' ' );
            long days = behindMillis / MILLIS_IN_A_DAY;
            if ( days > 0 )
            {
                behind.append( days );
                if ( days > 1 )
                    behind.append( ' ' ).append( messageSourceAccessor.getMessage( "streamTime.days" ) ).append( ' ' );
                else
                    behind.append( ' ' ).append( messageSourceAccessor.getMessage( "streamTime.day" ) ).append( ' ' );
                behindMillis = behindMillis % MILLIS_IN_A_DAY;
            }
            behind.append( timeFormat.format( new Date( behindMillis ) ) );
            return( behind.toString() );
        }
        else
            return( "" );
    }
}
