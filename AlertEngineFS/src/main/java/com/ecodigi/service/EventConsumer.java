/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.cep.CEPSettings;
import com.ecodigi.domain.AlertEngineConstants;
import com.ecodigi.domain.OnEventNode;
import com.ecodigi.esper.EsperEngine;
import com.ecodigi.runlevel.RunLevelHandler;
import com.ecodigi.runlevel.ServiceRunLevelType;
import com.ecodigi.stream.FileListenerOperation;
import com.ecodigi.stream.PoolingDirectoryFileListener;
import com.ecodigi.util.LoggableContextAwareBean;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Service
public class EventConsumer extends LoggableContextAwareBean
{
    public static final long FILE_REJECTION_INTERVAL_MILLIS = 1000L;
    public static final String KEY_XML_FILE_NAME = "xmlfile";

    private PoolingDirectoryFileListener fileListener;

    @Autowired
    private CEPSettings cepSettings;

    @Autowired
    private EsperEngine esperEngine;

    @Autowired
    private RunLevelHandler runLevelHandler;

    @Autowired
    private StreamTimeService streamTimeService;

    @Autowired
    private XMLParser xmlParser;

    @Scheduled( initialDelay = ServiceRunLevelType.DEFAULT_APPLICATION_LEVEL_INITIAL_WAIT, fixedDelay = PoolingDirectoryFileListener.FILE_PROCESS_POOLING_INTERVAL )
    private void processEntries()
    {
        if ( fileListener == null )
            return;

        if ( runLevelHandler.handleServiceState( "Event Consumer File Listener" ) )
        {
            while ( fileListener.processEntries() )
            {
            }
        }
    }

    public void initialize() throws IOException
    {
        fileListener = new PoolingDirectoryFileListener( cepSettings.getEventsPath(), AlertEngineConstants.EVENTS_FILENAME_EXTENSION, cepSettings.getEventsHistorySize() )
        {
            // Overriden for the internal thread not to be created and file processing thread be delegated to Spring
            @Override
            protected void startProcessThread()
            {
            }

            @Override
            public boolean process(  File file, FileListenerOperation operation )
            {
                //return( parseDocumentAsWhole( file ) );
                return( parseWithSAX( file ) );
            }
        };
        fileListener.start();
    }

    public void close()
    {
        fileListener.stop();
    }

    private boolean parseDocumentAsWhole( File file )
    {
        Node node = xmlParser.parseAsNode( file.toPath(), AlertEngineConstants.EVENTS_FILE_ROOT_NODE );
        if ( node != null )
        {
            NodeList children = node.getChildNodes();
            for( int i = 0; i < children.getLength(); i++ )
            {
                Node child = children.item( i );
                if ( logger.isDebugEnabled() )
                    logger.debug( String.format( "Name '%s' value '%s' Type %d", child.getNodeName(), child.getNodeValue(), child.getNodeType() ) );
                if ( child.getNodeType() == Node.ELEMENT_NODE )
                {
                    if ( logger.isDebugEnabled() )
                        logger.debug( String.format( "Posting '%s'", child.getNodeName() ) );

                    // Include originating XML file name
                    child.setUserData( KEY_XML_FILE_NAME, file.getName(), null );
                    esperEngine.pushEvent( child );

                    // Update Streamtime for the specified event.
                    // Todo: externalize these constants
                    if ( AlertEngineConstants.EVENT_NAME_TIMESTAMP.equals( child.getNodeName() ) )
                        processStreamtime( child );
                }
            }
            return( true );
        }
        else
            return( false );
    }

    private boolean parseWithSAX( final File file )
    {
        xmlParser.parse( file, AlertEngineConstants.EVENTS_FILE_ROOT_NODE,
            new OnEventNode()
            {
                @Override
                public boolean onEvent( Node node )
                {
                    node.setUserData( KEY_XML_FILE_NAME, file.getName(), null );
                    esperEngine.pushEvent( node );
                    if ( AlertEngineConstants.EVENT_NAME_TIMESTAMP.equals( node.getNodeName() ) )
                        processStreamtime( node );
                    return( true );
                }
            });
        return( true );
    }

    private void processStreamtime( Node node )
    {
        Node timeStampNode = node.getAttributes().getNamedItem( AlertEngineConstants.EVENT_TIMESTAMP_ATTRIBUTE );
        if ( timeStampNode != null )
        {
            if ( logger.isDebugEnabled() )
                logger.debug( String.format( "Name '%s' Timestamp %s", node.getNodeName(), timeStampNode.getNodeValue() ) );
            try
            {
                streamTimeService.putStreamTime( Long.parseLong( timeStampNode.getNodeValue() ) );
            }
            catch( NumberFormatException e )
            {
                logger.error( String.format( "Invalid %s attribute value '%s'", AlertEngineConstants.EVENT_TIMESTAMP_ATTRIBUTE, timeStampNode.getNodeValue() ), e );
            }
        }
    }

    @Scheduled( initialDelay = ServiceRunLevelType.DEFAULT_APPLICATION_LEVEL_INITIAL_WAIT, fixedDelay = FILE_REJECTION_INTERVAL_MILLIS )
    public void moveRejectedFiles()
    {
        if ( fileListener == null )
            return;

        if ( runLevelHandler.handleServiceState( "Event File Rejector" ) )
        {
            List<String> rejectedFilenames = esperEngine.getRejectedFiles();
            if ( rejectedFilenames != null )
            {
                for( String filename: rejectedFilenames )
                    fileListener.rejectFile( filename );
            }
        }
    }
}
