/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.cep.CEPSettings;
import com.ecodigi.domain.OnEventNode;
import com.ecodigi.domain.SAXParserHandler;
import com.ecodigi.util.LoggableContextAwareBean;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Path;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

@Service
public class XMLParser extends LoggableContextAwareBean
{
    private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
    public static final String XSD_ELEMENT = "xs:element";
    public static final String XSD_COMPLEX_TYPE = "xs:complexType";
    
    @Autowired
    private CEPSettings cepSettings;
    
    private DocumentBuilder documentBuilder = null;
    private SAXParser saxParser = null;

    private final MyErrorHandler myErrorHandler = new MyErrorHandler();

    private class MyErrorHandler implements ErrorHandler
    {

        private String getParseExceptionInfo( SAXParseException spe )
        {
            String systemId = spe.getSystemId();
            if (systemId == null)
            {
                systemId = "null";
            }

            String info = "URI=" + systemId + " Line=" + spe.getLineNumber() +
                          ": " + spe.getMessage();
            return info;
        }

        @Override
        public void warning( SAXParseException spe ) throws SAXException
        {
            logger.warn( "Warning: " + getParseExceptionInfo(spe) );
        }

        @Override
        public void error( SAXParseException spe ) throws SAXException
        {
            String message = "Error: " + getParseExceptionInfo(spe);
            logger.error(message);
            throw new SAXException( message );
        }

        @Override
        public void fatalError( SAXParseException spe ) throws SAXException
        {
            String message = "Fatal Error: " + getParseExceptionInfo(spe);
            logger.error(message);
            throw new SAXException( message );
        }
    }
    
    public void initialize()
    {
        initialize( cepSettings.isEnabledEventValidation() );
    }
    
    public void initialize( boolean validate )
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware( true );
        if ( validate )
        {
            dbf.setAttribute( JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA );
            dbf.setValidating( true );
        }
        try
        {
            documentBuilder = dbf.newDocumentBuilder();
            documentBuilder.setErrorHandler( myErrorHandler );
        }
        catch ( ParserConfigurationException e )
        {
            logger.error( e );
        }

        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        parserFactory.setValidating( validate );
        parserFactory.setNamespaceAware( true );
        try
        {
            saxParser = parserFactory.newSAXParser();
        }
        catch ( ParserConfigurationException e )
        {
            logger.error( e );
        }
        catch( SAXException e )
        {
            logger.error( e );
        }
    }

    public void close()
    {
        documentBuilder = null;
        saxParser       = null;
    }

    public Document parse( String fileName ) throws ParserConfigurationException, SAXException, IOException
    {
        return( documentBuilder.parse( new File(fileName) ) );
    }

    public Node parseAsNode( Path file, String rootNodeName )
    {
        try
        {
            Node node = parse( file.toString() );
            Node rootNode = null;
            if ( StringUtils.isNotBlank( rootNodeName ) )
            {
                rootNode = node.getChildNodes().item( 0 );
                if ( ( rootNode == null ) || !rootNodeName.equals( rootNode.getNodeName() ) )
                    throw new SAXException( String.format( "Root node does not have the expected name. Was expecting '%s' and found '%s'", rootNodeName, ( rootNode == null ? "null" : rootNode.getNodeName() ) ) );
            }
            return ( rootNode );
        }
        catch ( ParserConfigurationException ex )
        {
            logger.error( ex );
        }
        catch ( SAXException ex )
        {
            logger.error( ex );
        }
        catch ( IOException ex )
        {
            logger.error( ex );
        }
        catch( Throwable e )
        {
            logger.error( e);
        }
        return( null );
    }

    public void parse( File file, String rootNodeName, OnEventNode onEventNode )
    {
        SAXParserHandler handler = new SAXParserHandler( documentBuilder );
        handler.parse( saxParser, file, rootNodeName, onEventNode );
    }

//    public void printNode( Node node, int level )
//    {
//        logger.info( String.format( "***** Node %s - %s", node.getNodeName(), node.getNodeValue() ) );
//        NodeList list = node.getChildNodes();
//        for( int index = 0; index < list.getLength(); index++ )
//        {
//            Node child = list.item( index );
//            logger.info( String.format( "Node at level %d element %d/%d %s - %s", level, index + 1, list.getLength(), child.getNodeName(), child.getNodeValue() ) );
//            if ( child.hasChildNodes() )
//                printNode( child, level + 1 );
//        }
//    }
    
    public String getNodeAsString( Node node )
    {
        try
        {
            Transformer tf = TransformerFactory.newInstance().newTransformer();
            tf.setOutputProperty( OutputKeys.ENCODING, "UTF-8" );
            tf.setOutputProperty( OutputKeys.INDENT, "yes" );
            Writer out = new StringWriter();
            tf.transform( new DOMSource( node ), new StreamResult( out ) );
            return( out.toString() );
        }
        catch( TransformerException e )
        {
            logger.error( e );
        }
        return( null );
    }

    public void printNode( Node node )
    {
        String str = getNodeAsString( node );
        if  ( node != null )
            logger.info( str );
    }
}
