/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.util.LoggableContextAwareBean;
import java.util.Date;
import org.springframework.stereotype.Service;

@Service
public class TimeService extends LoggableContextAwareBean
{
    public long getMillis()
    {
        return( System.currentTimeMillis() );
    }

    public long getMicroseconds()
    {
        return( getMillis() * 1000L );
    }

    public Date getDate()
    {
        return( new Date( getMillis() ) );
    }
    
    public static long coldGetMicroseconds()
    {
        return( System.currentTimeMillis() * 1000L );
    }

//    public Date getDate( TimeZone timeZone )
//    {
//        return( new Date( TimeZoneUtils.shiftFromUTC( getMillis(), timeZone ) ) );
//    }
}
