/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.cep.CEPUnderlyingEventUtils;
import com.ecodigi.domain.AlertEngineConstants;
import com.ecodigi.domain.cep.OutputEvent;
import com.ecodigi.domain.cep.OutputEventType;
import com.ecodigi.runlevel.RunLevelHandler;
import com.ecodigi.runlevel.ServiceRunLevelType;
import com.ecodigi.util.LoggableContextAwareBean;
import com.espertech.esper.client.EventBean;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EventOutputPublisher extends LoggableContextAwareBean
{
    private static final long OUTPUT_DELIVERY_INTERVAL_MILLIS = 100;

    private final LinkedBlockingQueue<OutputEvent> outputQueue = new LinkedBlockingQueue<OutputEvent>();

    @Autowired
    private XStreamEngine xstreamEngine;

    @Autowired
    private RunLevelHandler runLevelHandler;

    @Autowired
    private CEPUnderlyingEventUtils underlyingEventUtils;
    
    @Autowired
    private StreamTimeService streamTimeService;

    private final AtomicLong alertsOut = new AtomicLong( 0 );
    private final AtomicLong feedsOut  = new AtomicLong( 0 );
    
    public void onEvent( Long sourceId, EventBean newEvent )
    {
        OutputEvent outEvent = new OutputEvent( OutputEventType.Alert, sourceId, streamTimeService.getStreamTime() );
        addEventAttributes( outEvent, newEvent );
        outputQueue.offer( outEvent );
        alertsOut.incrementAndGet();
    }

    public void onEvent( String name, EventBean newEvent )
    {
        OutputEvent outEvent = new OutputEvent( OutputEventType.Feed, name, streamTimeService.getStreamTime() );
        addEventAttributes( outEvent, newEvent );
        outputQueue.offer( outEvent );        
        feedsOut.incrementAndGet();
    }
    
    public int getQueueSize()
    {
        return( outputQueue.size() );
    }

    private void addEventAttributes( OutputEvent outEvent, EventBean newEvent )
    {
        Map<String,String> contentMap = underlyingEventUtils.getUnderlyingEventContentMap( newEvent );
        if ( contentMap == null )
        {
            logger.error( "Unable to create Content Map from " + newEvent.getClass().getName() );
            return;
        }
        for( Map.Entry<String,String> entry: contentMap.entrySet() )
            outEvent.addEntry( entry.getKey(), entry.getValue() );
    }

    @Scheduled( initialDelay = ServiceRunLevelType.DEFAULT_BASIC_LEVEL_INITIAL_WAIT, fixedDelay = OUTPUT_DELIVERY_INTERVAL_MILLIS )
    protected void deliverOutputEvents()
    {
        while ( runLevelHandler.handleServiceState( "Event Output Publisher" ) )
        {
            OutputEvent outEvent = outputQueue.poll();
            if ( outEvent != null )
            {
                if ( !publish( outEvent ) )
                    break;
            }
            else
                break;
        }
    }

    private boolean publish( OutputEvent outEvent )
    {
        return( xstreamEngine.write( outEvent, "cep", AlertEngineConstants.ALERT_FILE_EXTENSION ) );
    }

    public long getAlertsOut()
    {
        return( alertsOut.get() );
    }

    public long getFeedsOut()
    {
        return( feedsOut.get() );
    }
}
