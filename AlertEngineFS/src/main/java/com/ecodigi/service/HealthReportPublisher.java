/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.service;

import com.ecodigi.cep.CEPManager;
import com.ecodigi.cep.CEPSettings;
import com.ecodigi.domain.AlertEngineConstants;
import com.ecodigi.domain.cep.HealthReport;
import com.ecodigi.esper.EsperEngine;
import com.ecodigi.io.FileSuffixFilter;
import com.ecodigi.runlevel.AlertEngineRunLevel;
import com.ecodigi.runlevel.RunLevelHandler;
import com.ecodigi.runlevel.ServiceRunLevelType;
import com.ecodigi.util.LoggableContextAwareBean;
import java.io.File;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class HealthReportPublisher extends LoggableContextAwareBean
{
    private static final long HEALTH_REPORT_INTERVAL_MILLIS = 5000;

    @Autowired
    private CEPSettings cepSettings;

    @Autowired
    private XStreamEngine xstreamEngine;

    @Autowired
    private RunLevelHandler runLevelHandler;
    
    @Autowired
    private TimeService timeService;
    
    @Autowired
    private StreamTimeService streamTimeService;

    @Autowired
    private EventOutputPublisher eventOutputPublisher;
    
    @Autowired
    private EsperEngine esperEngine;

    @Autowired
    private CEPManager cepManager;

    private HealthReport getHealthReport()
    {
        File eventsDir = new File( cepSettings.getEventsPath() );
        File streamFiles[] = eventsDir.listFiles( new FileSuffixFilter( AlertEngineConstants.EVENTS_FILENAME_EXTENSION ) );
        long totalSize = 0;
        for( File streamFile: streamFiles )
            totalSize += streamFile.length();
        HealthReport report = new HealthReport( AlertEngineRunLevel.ALERT_ENGINE_VERSION_STR, getUpTime(), timeService.getMillis(),
                                                streamTimeService.getCurrentTime() / 1000L, esperEngine.getHeartBeat(),
                                                esperEngine.getEventsIn(), eventOutputPublisher.getAlertsOut(), eventOutputPublisher.getFeedsOut(),
                                                streamFiles.length, totalSize,
                                                esperEngine.getRequestQueueSize(), esperEngine.getInputQueueSize(),
                                                eventOutputPublisher.getQueueSize() );
        report.setExpressionCount( cepManager.getActiveSourceFeedCount(), cepManager.getActiveAlertCount(), cepManager.getActiveEPLOutputFeedCount() );
        return( report );
    }

    private long getUpTime()
    {
        Date upTime = runLevelHandler.getUpTime();
        return( upTime == null ? 0 : upTime.getTime() );
    }

    @Scheduled( initialDelay = ServiceRunLevelType.DEFAULT_APPLICATION_LEVEL_INITIAL_WAIT, fixedDelay = HEALTH_REPORT_INTERVAL_MILLIS )
    protected void postReport()
    {
        if ( runLevelHandler.handleServiceState( "Health Report Publisher" ) )
        {
            HealthReport report = getHealthReport();
            xstreamEngine.write( report );
        }
    }
}
