/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain;

public class AppendableText
{
    private StringBuilder text = null;
    private final String  delimiter;

    public AppendableText()
    {
        delimiter = ", ";
    }

    public AppendableText( String delimiter )
    {
        this.delimiter = delimiter;
    }

    public AppendableText append( String textFragment )
    {
        return( append( textFragment, true ) );
    }
    
    public AppendableText append( String textFragment, boolean includeDelimiter )
    {
        if ( text == null )
            text = new StringBuilder();

        if ( includeDelimiter && ( text.length() > 0 ) )
            text.append( delimiter );

        text.append( textFragment );

        return( this );
    }

    public boolean isEmpty()
    {
        return( ( text == null ) || ( text.length() == 0 ) );
    }
    
    public void clear()
    {
        text = null;
    }

    @Override
    public String toString()
    {
        if ( text == null )
            return( null );
        else
            return( text.toString() );
    }
}
