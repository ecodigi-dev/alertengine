/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.ecodigi.domain;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.SAXParser;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXParserHandler extends DefaultHandler
{
    protected Log logger = LogFactory.getLog( getClass() );

    private final DocumentBuilder documentBuilder;

    private String rootNodeName;
    private OnEventNode onEventNode;

    private SAXParserState state = SAXParserState.None;
    private StringWriter writer = new StringWriter();

    private Document doc;
    private Element rootNode;
    private Element eventNode;
    private Element attributeNode;

    public SAXParserHandler( DocumentBuilder documentBuilder )
    {
        this.documentBuilder = documentBuilder;
    }

    public void parse( SAXParser saxParser, File file, String rootNodeName, OnEventNode onEventNode )
    {
        this.rootNodeName = rootNodeName;
        this.onEventNode  = onEventNode;
        try
        {
            saxParser.parse( file, this );
        }
        catch ( IOException ex )
        {
            logger.error( ex );
        }
        catch( SAXException e )
        {
            logger.error( e );
        }
        catch( Throwable e )
        {
            logger.error( e);
        }
    }

    @Override
    public void startElement( String uri, String localName, String qName, Attributes atts ) throws SAXException
    {
        if ( logger.isDebugEnabled() )
            logger.debug( String.format( "startElement uri %s localName %s qName %s State %s", uri, localName, qName, state.name() ) );

        switch( state )
        {
            case None:
                state = SAXParserState.Root;
                if ( !rootNodeName.equals( qName ) )
                    logger.warn( String.format( "Expected root node '%s' but received '%s'", rootNodeName, qName ) );
                break;
            case Root:
                state = SAXParserState.Event;

                // Create the root element node
                doc = documentBuilder.newDocument();
                doc.setStrictErrorChecking( false );
                rootNode = doc.createElement( rootNodeName );
                doc.appendChild( rootNode );

                // Create event element
                eventNode = doc.createElement( qName );
                rootNode.appendChild( eventNode );
                for( int i = 0; i < atts.getLength(); i++ )
                {
                    String attrName = atts.getQName( i );
                    String attrValue = atts.getValue( i );
                    eventNode.setAttribute( attrName, attrValue );
                }
                break;
            case Event:
                state = SAXParserState.Attributes;
                attributeNode = doc.createElement( qName );
                eventNode.appendChild( attributeNode );
                writer = new StringWriter();
                break;
            case Attributes:
                logger.error( "startElement: invalid nesting level for this application" );
                break;
        }
    }

    @Override
    public void endElement( String uri, String localName, String qName ) throws SAXException
    {
        if ( logger.isDebugEnabled() )
            logger.debug( String.format( "endElement uri %s localName %s qName %s State %s Content %s", uri, localName, qName, state.name(), writer.toString().trim() ) );

        switch( state )
        {
            case None:
                logger.error( "endElement: invalid nesting level for this application" );
                break;
            case Root:
                state = SAXParserState.None;
                logger.debug( "End of doc" );
                break;
            case Event:
                state = SAXParserState.Root;
                onEventNode.onEvent( eventNode );
                rootNode = null;
                doc = null;
                eventNode = null;
                break;
            case Attributes:
                state = SAXParserState.Event;
                attributeNode.setTextContent( writer.toString().trim() );
                attributeNode = null;
                break;
        }
    }

    @Override
    public void characters( char[] ch, int start, int length ) throws SAXException
    {
        writer.write( ch, start, length );
    }
}
