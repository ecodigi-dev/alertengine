/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.cep;

import com.ecodigi.esper.EsperEngine;
import com.ecodigi.lang.EcodigiException;
import com.espertech.esper.client.EPStatement;
import org.apache.commons.lang3.StringUtils;

public class CEPServiceRequest
{
    private CEPServiceRequestCompleted completionHandler;
    private String epl;
    private EPStatement statement;
    private AbstractStatementBean statementBean;
    private Throwable exception;

    public CEPServiceRequest( String epl )
    {
        this.epl = epl;
    }

    public CEPServiceRequest( AbstractStatementBean statementBean )
    {
        this.statementBean = statementBean;
    }

    public void setCompletionHandler( CEPServiceRequestCompleted completionHandler )
    {
        this.completionHandler = completionHandler;
    }
    
    public EPStatement getStatement()
    {
        return statement;
    }

    public void setException( Throwable exception )
    {
        this.exception = exception;
    }

    public Throwable getException()
    {
        return exception;
    }
    
    public void execute( EsperEngine esperEngine )
    {
        exception = null;

        if ( StringUtils.isNotBlank( epl ) )
        {
            try
            {
                statement = esperEngine.createEPStatement( epl );
            }
            catch( Throwable e )
            {
                exception = e;
            }
        }
        else if ( statementBean != null )
        {
            try
            {
                statementBean.destroy();
            }
            catch( Throwable e )
            {
                exception = e;
            }
        }
        else
            exception = new EcodigiException( "Invalid CEPServiceRequest" );
        
        completionHandler.completed( this );
    }

    @Override
    public String toString()
    {
        if ( StringUtils.isNotBlank( epl ) )
            return( "Create '" + epl + "'" );
        else if ( statementBean != null )
            return( "Destroy " + statementBean.toString() );
        else
            return( "Unkown request" );
    }
}
