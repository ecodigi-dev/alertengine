/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.cep;

import com.ecodigi.service.EventOutputPublisher;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EcodigiEPLFeed extends AbstractStatementBean implements UpdateListener
{
    private static final Log logger = LogFactory.getLog( EcodigiEPLFeed.class );

    private final EventOutputPublisher outputPublisher;
    private final String               name;

    public EcodigiEPLFeed( EventOutputPublisher outputPublisher, String name )
    {
        this.outputPublisher = outputPublisher;
        this.name             = name;
    }

    public String getName()
    {
        return name;
    }

    public void setListener()
    {
        setListener( this );
    }

    @Override
    public void update( EventBean[] newEvents, EventBean[] oldEvents )
    {
        if ( logger.isDebugEnabled() )
            logger.debug( String.format( "Event new %d old %d",
                                         ( newEvents == null ? -1 : newEvents.length ),
                                         ( oldEvents == null ? -1 : oldEvents.length ) ) );
        if ( newEvents != null )
        {
            for( EventBean eventBean: newEvents )
                outputPublisher.onEvent( name, eventBean );
        }
    }

    @Override
    public String toString()
    {
        return( "EcodigiEPLFeed " + getName() );
    }
}
