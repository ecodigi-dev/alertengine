/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.cep;

import com.ecodigi.lang.EcodigiException;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.UpdateListener;

public abstract class AbstractStatementBean
{
    private EPStatement epStatement;

    public void destroy()
    {
        epStatement.removeAllListeners();
        epStatement.stop();
        epStatement.destroy();
        epStatement = null;
    }

    public void setEpStatement( EPStatement epStatement )
    {
        this.epStatement = epStatement;
    }

    public EPStatement getEpStatement()
    {
        return epStatement;
    }

    public void setListener( UpdateListener listener )
    {
        if ( epStatement == null )
            throw new EcodigiException( "EP Statement must be created before setting listener" );
        epStatement.addListener( listener );
    }    
}
