/* 
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.domain.cep;

import com.ecodigi.service.EventOutputPublisher;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EcodigiAlert extends AbstractStatementBean implements UpdateListener
{
    private static final Log logger = LogFactory.getLog( EcodigiAlert.class );

    private final EventOutputPublisher outputPublisher;
    private final AlertConfiguration   alertConfig;

    public EcodigiAlert( EventOutputPublisher outputPublisher, AlertConfiguration alertConfig )
    {
        this.outputPublisher  = outputPublisher;
        this.alertConfig      = alertConfig;
    }

    public AlertConfiguration getAlertConfig()
    {
        return alertConfig;
    }

    public void setListener()
    {
        setListener( this );
    }

    @Override
    public void update( EventBean[] newEvents, EventBean[] oldEvents )
    {
        if ( logger.isDebugEnabled() )
            logger.debug( String.format( "Event %s new %d old %d", alertConfig.getName(),
                                        ( newEvents == null ? -1 : newEvents.length ),
                                        ( oldEvents == null ? -1 : oldEvents.length ) ) );
        if ( newEvents != null )
        {
            for( EventBean eventBean: newEvents )
                outputPublisher.onEvent( alertConfig.getId(), eventBean );
        }
    }

    @Override
    public String toString()
    {
        return( "EcodigiAlert " + getAlertConfig().getName() );
    }
}
