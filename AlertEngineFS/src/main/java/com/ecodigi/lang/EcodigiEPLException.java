/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.lang;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class EcodigiEPLException extends Exception
{
    private static final long serialVersionUID = 100L;

    protected Log logger = LogFactory.getLog( this.getClass() );

    public EcodigiEPLException( String message )
    {
        super( message );
        if( logger.isTraceEnabled() )
        {
            for (StackTraceElement stackTraceElement : this.getStackTrace()) {
                logger.trace(this.toString());
                logger.trace(stackTraceElement.toString());
            }
        }
    }

    public EcodigiEPLException( Throwable t )
    {
        super(t);
        if( logger.isTraceEnabled() )
        {
            logger.trace(this.toString());
            for (StackTraceElement stackTraceElement : this.getStackTrace()) {
                logger.trace(stackTraceElement.toString());
            }
            logger.trace("RootCause: " + t.toString());
            for (StackTraceElement stackTraceElement : t.getStackTrace()) {
                logger.trace(stackTraceElement.toString());
            }
        }
    }

    public EcodigiEPLException( String message, Throwable t )
    {
        super( message, t );
        if( logger.isTraceEnabled() )
        {
            logger.trace(this.toString() + " - " + message);
            for (StackTraceElement stackTraceElement : this.getStackTrace()) {
                logger.trace(stackTraceElement.toString());
            }
            logger.trace("RootCause: " + t.toString());
            for (StackTraceElement stackTraceElement : t.getStackTrace()) {
                logger.trace(stackTraceElement.toString());
            }
        }
    }
}
