/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.ecodigi.util;

import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;

@SuppressWarnings( "unused" )
public class MathUtils
{
    public static final int DEFAULT_PERCENT_PRECISION = 2;
    public static final boolean USE_THOUSAND_SEPARATOR = true;
    public static final boolean NO_THOUSAND_SEPARATOR  = false;

    private static final DecimalFormat  nf    = new DecimalFormat( "#######################0" );
    private static final DecimalFormat  nf9   = new DecimalFormat( "#######################0.000000000" );
    private static final DecimalFormat  nfts  = new DecimalFormat( "###,###,###,###,###,###,###,##0" );
    private static final DecimalFormat  nf2ts = new DecimalFormat( "###,###,###,###,###,###,###,##0.00" );
    private static final DecimalFormat  nf9ts = new DecimalFormat( "###,###,###,###,###,###,###,##0.000000000" );

    private static final double INTEGER_THRESHOLD = Integer.MAX_VALUE / 10;

//    public static int intValue( Double value )
//    {
//        if ( value == null )
//            return( 0 );
//        return( value.intValue() );
//    }
//
//    public static double safeAdd( Double value1, Double value2 )
//    {
//        return( ( value1 == null ? 0.0 : value1 ) +
//                ( value2 == null ? 0.0 : value2 ) );
//    }
//
//    public static long getRoundPercent( long dividend, long divisor )
//    {
//        return( Math.round( ( double )dividend * 100.0 / ( double )divisor ) );
//    }
//
//    public static int getRoundPercent( int dividend, int divisor )
//    {
//        return( ( int )Math.round( ( double )dividend * 100.0 / ( double )divisor ) );
//    }
//
//    public static double getRoundPercent( double dividend, double divisor )
//    {
//        return( Math.round( dividend * 100.0 / divisor ) );
//    }
//
//    public static double getRoundPercent( double dividend, double divisor, int precision )
//    {
//        double precisionFactor = Math.pow( 10, precision );
//        return( Math.round( dividend * 100.0 * precisionFactor / divisor ) / precisionFactor );
//    }
//
//    public static long ceilToNextMultiple( long value, long multiplier )
//    {
//        long ceilValue = ( value / multiplier ) * multiplier;
//        if ( ceilValue < value )
//            ceilValue += multiplier;
//        return( ceilValue );
//    }
//
//    public static String getFormatted( Integer value )
//    {
//        if ( value == null )
//            return( "null" );
//        else
//        {
//            synchronized( nfts )
//            {
//                return( nfts.format( value ) );
//            }
//        }
//    }
//
//    public static String getFormatted( Long value )
//    {
//        if ( value == null )
//            return( "null" );
//        else
//        {
//            synchronized( nfts )
//            {
//                return( nfts.format( value ) );
//            }
//        }
//    }
//
//    public static String getFormatted( Double value )
//    {
//        if ( value == null )
//            return( "null" );
//        else
//        {
//            synchronized( nf2ts )
//            {
//                return( nf2ts.format( value ) );
//            }
//        }
//    }
//
//    public static Double parseAutoFormatted( String value ) throws ParseException
//    {
//        if ( "null".equals(value) )
//            return null;
//        if ( StringUtils.isNotBlank(value) )
//        {
//            if ( value.indexOf('.') > 0 )
//                synchronized( nf9ts )
//                {
//                    return( nf9ts.parse(value).doubleValue() );
//                }
//            else
//                synchronized( nfts )
//                {
//                    return( nfts.parse(value).doubleValue() );
//                }
//        }
//        else
//            return 0.0;
//    }

    public static String getAutoFormatted( Double value )
    {
        return( getAutoFormatted( value, INTEGER_THRESHOLD, USE_THOUSAND_SEPARATOR ) );
    }

//    public static String getAutoFormatted( Double value, boolean thousandSeparator )
//    {
//        return( getAutoFormatted( value, INTEGER_THRESHOLD, thousandSeparator ) );
//    }

    public static String getAutoFormatted( Double value, double intThreshold, boolean thousandSeparator )
    {
        if ( value == null )
            return( "null" );
        else
        {
            if ( value > intThreshold )
            {
                if ( thousandSeparator )
                {
                    synchronized( nfts )
                    {
                        return( nfts.format( value ) );
                    }
                }
                else
                {
                    synchronized( nf )
                    {
                        return( nf.format( value ) );
                    }

                }
            }
            else
            {
                double frac = value - value.longValue();
                if ( frac > 0 )
                {
                    if ( thousandSeparator )
                    {
                        synchronized( nf9ts )
                        {
                            String strValue = nf9ts.format( value );
                            return( StringUtils.stripEnd( strValue, "0" ) );
                        }
                    }
                    else
                    {
                        synchronized( nf9 )
                        {
                            String strValue = nf9.format( value );
                            return( StringUtils.stripEnd( strValue, "0" ) );
                        }
                    }
                }
                else
                {
                    if ( thousandSeparator )
                    {
                        synchronized( nfts )
                        {
                            return( nfts.format( value ) );
                        }
                    }
                    else
                    {
                        synchronized( nf )
                        {
                            return( nf.format( value ) );
                        }
                    }
                }
            }
        }
    }

    public static String getLeftPaddedString( long number, int size )
    {
        String numberStr = String.valueOf( number );
        return( StringUtils.leftPad( numberStr, size, '0' ) );
    }
}
