/*
 * Copyright (C) 2018 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.util;

import java.util.TimeZone;

public class MicrosecUtils
{
    public static final long MILLISECONDS_IN_A_SECOND   = 1000;
    public static final long MICROSECONDS_IN_MILISECOND = 1000;
    public static final long MICROSECONDS_IN_A_SECOND   = 1000 * 1000;
    public static final long MICROSECONDS_IN_A_MINUTE   = MICROSECONDS_IN_A_SECOND * 60;
    public static final long MICROSECONDS_IN_AN_HOUR    = MICROSECONDS_IN_A_MINUTE * 60;
    public static final long MICROSECONDS_IN_A_DAY      = MICROSECONDS_IN_AN_HOUR * 24;
    public static final long MICROSECONDS_IN_A_WEEK     = MICROSECONDS_IN_A_DAY * 7;

    public static final String MICROSECONDS_SYMBOL      = "&#956;s";
    public static final String MILLISECONDS_SYMBOL      = "ms";

//    public static String getHMSString( long timestamp )
//    {
//        // Add 500ms to round value to seconds
//        timestamp += 500000;
//
//        long hours = timestamp / MICROSECONDS_IN_AN_HOUR;
//        long remaining = timestamp - hours * MICROSECONDS_IN_AN_HOUR;
//        long minutes = remaining / MICROSECONDS_IN_A_MINUTE;
//        remaining -= minutes * MICROSECONDS_IN_A_MINUTE;
//        long seconds = remaining / MICROSECONDS_IN_A_SECOND;
//        return( String.format( "%02d:%02d:%02d", hours, minutes, seconds ) );
//    }
//
    public static long getMicrosecondsPart( long timestamp )
    {
        return( timestamp % MICROSECONDS_IN_A_SECOND );
    }

//    public static int getFragment( long timestamp, int fragment )
//    {
//        if ( timestamp == 0 )
//            return( 0 );
//
//        switch( fragment )
//        {
//            case Calendar.SECOND:
//                return( ( int )( ( truncateSeconds( timestamp ) - truncateMinutes( timestamp ) ) / MICROSECONDS_IN_A_SECOND ) );
//            case Calendar.MINUTE:
//                return( ( int )( ( truncateMinutes( timestamp ) - truncateHours( timestamp ) ) / MICROSECONDS_IN_A_MINUTE ) );
//            case Calendar.HOUR:
//            case Calendar.HOUR_OF_DAY:
//                return( ( int )( ( truncateHours( timestamp ) - truncateDays( timestamp ) ) / MICROSECONDS_IN_AN_HOUR ) );
//        }
//
//        throw new EcodigiException( "Invalid time fragment " + fragment );
//    }
//
//    public static long getMicrosecondsTimestamp( long seconds, long microseconds )
//    {
//        return( seconds * MICROSECONDS_IN_A_SECOND + microseconds );
//    }
//
//    public static long getMillisecondsTimestamp( long seconds, long microseconds )
//    {
//        return( ( seconds * MILLISECONDS_IN_A_SECOND ) + ( ( microseconds + 500L ) / MICROSECONDS_IN_MILISECOND ) );
//    }
//
//    public static long truncateMilliseconds( long timestamp )
//    {
//        return( timestamp - ( timestamp % MILLISECONDS_IN_A_SECOND ) );
//    }
//
    public static long truncateSeconds( long timestamp )
    {
        return( timestamp - ( timestamp % MICROSECONDS_IN_A_SECOND ) );
    }

//    public static long truncateMinutes( long timestamp )
//    {
//        return( timestamp - ( timestamp % MICROSECONDS_IN_A_MINUTE ) );
//    }
//
//    public static long truncateHours( long timestamp )
//    {
//        return( timestamp - ( timestamp % MICROSECONDS_IN_AN_HOUR ) );
//    }
//
//    public static long truncateDays( long timestamp )
//    {
//        return( timestamp - ( timestamp % MICROSECONDS_IN_A_DAY ) );
//    }
//
//    public static long truncateSliceWidth( long timestamp, long sliceWidth )
//    {
//        return( timestamp - ( timestamp % sliceWidth ) );
//    }
//
//    public static boolean inSameDay( long first, long second )
//    {
//        return( truncateDays( first ) == truncateDays( second ) );
//    }
//
//    public static long addDays( long timestamp, long days )
//    {
//        return( timestamp + ( MICROSECONDS_IN_A_DAY * days ) );
//    }
//
//    public static long nextDay( long timestamp )
//    {
//        return( addDays( truncateDays( timestamp ), 1 ) );
//    }
//
//    public static long nextDay( long timestamp, TimeZone targetTimeZone )
//    {
//        return( addDays( truncateDays( MicrosecUtils.shiftFromUTC( timestamp, targetTimeZone ) ), 1 ) );
//    }
//
//    public static long addHours( long timestamp, long hours )
//    {
//        return( timestamp + ( MICROSECONDS_IN_AN_HOUR * hours ) );
//    }
//
//    public static long addMinutes( long timestamp, long minutes )
//    {
//        return( timestamp + ( MICROSECONDS_IN_A_MINUTE * minutes ) );
//    }
//
//    public static long nextMinute( long timestamp )
//    {
//        return( addMinutes( truncateMinutes( timestamp ), 1 ) );
//    }
//
//    public static long addSeconds( long timestamp, long seconds )
//    {
//        return( timestamp + ( MICROSECONDS_IN_A_SECOND * seconds ) );
//    }
//
//    public static long getSeconds( long microsecondsTimestamp )
//    {
//        return( microsecondsTimestamp / MICROSECONDS_IN_A_SECOND );
//    }
//
    public static long getMillis( long microsecondsTimestamp )
    {
        return( microsecondsTimestamp / MICROSECONDS_IN_MILISECOND );
    }

    public static long getMicros( long millis )
    {
        return( millis * MICROSECONDS_IN_MILISECOND );
    }

    public static long getOffset( long microsecondsTimestamp, TimeZone sourceTimeZone )
    {
        return( getMicros( sourceTimeZone.getOffset( getMillis( microsecondsTimestamp ) ) ) );
    }

//    public static long shiftTimeZone( long microsecondsTimestamp, TimeZone sourceTimeZone, TimeZone targetTimeZone )
//    {
//        return( microsecondsTimestamp - getOffset( microsecondsTimestamp, sourceTimeZone ) + getOffset( microsecondsTimestamp, targetTimeZone ) );
//    }
//
//    public static long shiftToUTC( long microsecondsTimestamp, TimeZone sourceTimeZone )
//    {
//        return( microsecondsTimestamp - getOffset( microsecondsTimestamp, sourceTimeZone ) );
//    }
//
    public static long shiftFromUTC( long microsecondsTimestamp, TimeZone targetTimeZone )
    {
        return( microsecondsTimestamp + getOffset( microsecondsTimestamp, targetTimeZone ) );
    }

//    public static Date getDate( long microsecondsTimestamp )
//    {
//        return( new Date( getMillis( microsecondsTimestamp ) ) );
//    }
//
//    public static long getTimeInMicroseconds( Date when )
//    {
//        return( ( when.getTime() * 1000L ) % MICROSECONDS_IN_A_DAY );
//    }
//
    public static String getISOTimestampString( long microsecondsTimestamp )
    {
        StringBuilder str = new StringBuilder( DateUtils.getIsoHRDateTimeString( truncateSeconds( microsecondsTimestamp ) / 1000 ) );
        str.append( '.' );
        str.append( MathUtils.getLeftPaddedString( getMicrosecondsPart( microsecondsTimestamp ), 6 ) );
        return( str.toString() );
    }

//    public static String getTimestampString( long microsecondsTimestamp, DateFormat dateFormat )
//    {
//        StringBuilder str = new StringBuilder( dateFormat.format( truncateSeconds( microsecondsTimestamp ) / 1000 ) );
//        str.append( '.' );
//        str.append( MathUtils.getLeftPaddedString( getMicrosecondsPart( microsecondsTimestamp ), 6 ) );
//        return( str.toString() );
//    }
//
//    public static String getTimestampStringWithTimeZone( long microsecondsTimestamp, DateFormat dateFormat )
//    {
//        return( String.format( "%s %s", getTimestampString( microsecondsTimestamp, dateFormat ), dateFormat.getTimeZone().getDisplayName( false, TimeZone.SHORT ) ) );
//    }
//
//    public static String getDateTimeZoneString( long microsecondsTimestamp, TimeZone timeZone )
//    {
//        DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
//        df.setTimeZone( timeZone );
//        return( String.format( "%s %s", df.format( getDate( microsecondsTimestamp ) ), timeZone.getDisplayName( false, TimeZone.SHORT ) ) );
//    }
//
//    public static String getDurationShortStr( long durationMicros )
//    {
//        boolean negative = ( durationMicros < 0 );
//        if ( negative )
//            durationMicros = - durationMicros;
//
//        if ( durationMicros < 1500 )
//            return( String.format( "%s%d �s", ( negative ? "-" : "" ), durationMicros ) );
//        else
//        {
//            if ( durationMicros < ( 1500 * MICROSECONDS_IN_MILISECOND ) )
//                return( String.format( "%s%d ms", ( negative ? "-" : "" ), durationMicros / MICROSECONDS_IN_MILISECOND ) );
//            else
//                return( String.format( "%s%d s", ( negative ? "-" : "" ), ( ( durationMicros + 500 ) / MICROSECONDS_IN_A_SECOND ) ) );
//        }
//    }
//
//    public static String getDurationStr( long durationMicros )
//    {
//        boolean negative = ( durationMicros < 0 );
//        if ( negative )
//            durationMicros = - durationMicros;
//
//        if ( durationMicros < 1500 )
//            return( String.format( "%s%d �s", ( negative ? "-" : "" ), durationMicros ) );
//        else
//        {
//            if ( durationMicros < ( 1500 * MICROSECONDS_IN_MILISECOND ) )
//                return( String.format( "%s%d ms", ( negative ? "-" : "" ), durationMicros / MICROSECONDS_IN_MILISECOND ) );
//            else
//            {
//                if ( durationMicros < MICROSECONDS_IN_A_DAY )
//                    return( ( negative ? "-" : "" ) + DateUtils.getIsoHRTimeString( ( ( durationMicros + 500 ) / MICROSECONDS_IN_MILISECOND ) ) );
//                else
//                {
//                    long days = durationMicros / MICROSECONDS_IN_A_DAY;
//                    return( String.format( "%s%d days %s", ( negative ? "-" : "" ), days, DateUtils.getIsoHRTimeString( ( durationMicros % MICROSECONDS_IN_A_DAY ) / 1000 ) ) );
//                }
//            }
//        }
//    }
}