/*
 * Copyright (C) 2022 Ecodigi Tecnologia e Servi�os Ltda
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.ecodigi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 *
 * @author flaviogago
 */
@Configuration
@ComponentScan( basePackages = "com.ecodigi" )
@PropertySource( value={ "classpath:/com/ecodigi/config/alertEngine.default.properties", "file:${config.path}" }, ignoreResourceNotFound = true )
@EnableScheduling
public class AlertEngineConfig implements SchedulingConfigurer
{    
    private static final int SCHEDULER_POOL_SIZE = 20;

    @Bean
    public ThreadPoolTaskScheduler getThreadPoolTaskScheduler()
    {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize( SCHEDULER_POOL_SIZE );
        taskScheduler.initialize();
        return taskScheduler;
    }

    @Override
    public void configureTasks( ScheduledTaskRegistrar taskRegistrar )
    {
        taskRegistrar.setTaskScheduler( getThreadPoolTaskScheduler() );
    }
}
